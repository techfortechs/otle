# OTLE
### Over The Line Early
This project aims to automate detection of boats which cross the starting line
before a race has started. This is done through immediate visual feedback to
a referee using a web interface.

## Table of Contents

1. [Getting Started](#getting-started)
2. [Prerequisites](#prerequisites)
3. [Installation](#installation)
4. [Usage](#usage)
5. [Running Tests](#running-tests)

## Getting Started

These instructions will guide you through setting up your local environment
for development and testing purposes. See the [Deployment](#deployment) section
for notes on how to deploy the project on a live system.

### Prerequisites

List the software, libraries, and tools required to install and run the project. Provide the version numbers if necessary. For example:

- Git
- PostgreSQL
- PostGIS
- GDAL Libraries
- Python 3 (>=3.9)
- virtualenv
- MQTT

#### JavaScript Bundling Prerequisites
While production javascript assets are provided in the repo. Developing further
requires some additional installation:

- npm / yarn

### Installation
#### Manual Install
For manual installation follow these steps:

1. Clone and cd into the repository:
    1. Copy `ahoythere/settings/local.py.template` to `ahoythere/settings/local.py`.
    2. Populate `DATABASES` and `MQTT_*` fields with expected values.

2. Create a virtual environment.
	- ```console
		$ virtualenv pythonenv
	```
3. Install python dependencies
	- ```console
		$ ./pythonenv/bin/pip install -r requirements.txt
	```
4. Initialize a database instance, perform these instructions on the machine
hosting the database.
	- Install postgresql and postgis libraries
	- Start database with a command such as
		```console
		$ pg_ctl start -l logfile
		```
	- Connect to postgresql instance
   		```console
		$ sudo su postgres
		```
	- Run commands in psql shell:
		```postgresql
		CREATE DATABASE ahoythere;
		\c ahoythere
		CREATE EXTENSION postgis;
		CREATE USER skipper WITH ENCRYPTED PASSWORD {{ PASSWORD }};
		ALTER ROLE skipper SET client_encoding TO 'utf8';
		ALTER ROLE skipper SET default_transaction_isolation TO 'read committed';
		ALTER ROLE skipper SET timezone TO 'UTC';
		GRANT ALL PRIVILEGES ON DATABASE ahoythere TO skipper;
		```
5. Migrate the database to latest migrations
	```console
	$ . ./pythonenv/bin/activate/
    $ ./manage.py migrate
	```

6. Add a super user account and follow instructions
	```console
	$ ./manage.py createsuperuser
	```

7. Run bring the server up.
	```console
	$ ./manage.py runserver 0.0.0.0:8888
	```

8. You can access the webserver at [machine-ip]:8888/

9. Install MQTT on a machine (can be the same machine as Django). Follow MQTT
instructions on http://www.steves-internet-guide.com/install-mosquitto-broker/


##### JavaScript Development Installation
Manually creating the javascript environment requires some steps. For the sake
of simplicity, the examples are using `npm` invocations.

1. Install requirements
    ```console
    $ cd frontend/
    $ npm install
    ```

2. Run webpack build
    ```console
    $ webpack build
    ```

#### Docker Install
To run the project using docker, install Docker and docker-compose.

1. Then you may run
	```console
	$ docker-compose build
	$ docker-compose run otle python manage.py migrate
	$ docker-compose run otle python manage.py createsuperuser
	```

2. Start the docker network with
    ```console
    $ docker-compose up
    ```

### Usage
Once the server is online, you may access the Django web interace by accessing
[django-machine-ip]:8888/.

#### Device IDs
This project assumes that all GPS devices have unique hardware identifiers. The
Device model contains the field ``hardware_id`` which should be set to the
hexidecimal hardware id set on the device. This field **is case sensitive**.


### Running Tests
You may run tests with ```python manage.py test```

You may simulate a regatta with
    ```console
    $ python manage.py simulate_regatta test-regatta-binary simulation/Boat\ Positions-\ Valid\ Boat\ Positions.csv
    ```
