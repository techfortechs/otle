# syntax=docker/dockerfile:1
FROM python:3.10-bullseye
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /app
COPY requirements.txt /app/

RUN apt-get update -y && apt-get install -y binutils \
    libproj-dev gdal-bin postgresql-client git
RUN pip install -r requirements.txt && \
    pip install loguru paho-mqtt && \
    pip install git+https://github.com/filips123/MagneticFieldCalculator.git

COPY . /app
