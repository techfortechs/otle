import argparse
import json
import multiprocessing as mp
import random
import struct
from dataclasses import asdict, dataclass, is_dataclass
from datetime import datetime, timezone
from time import sleep

import numpy as np
from loguru import logger
from paho.mqtt import client as mqtt_client

BOAT_POSITION_CYCLE = [
    (42.358525, -71.087324),
    (42.358493, -71.085135),
    (42.357058, -71.084137),
    (42.354489, -71.084040),  # Possibly hitting a few paddle boats
    (42.354085, -71.087076),
    (42.354965, -71.088181),
    (42.356312, -71.088921),
    (42.357588, -71.088438),
]
N_POS = len(BOAT_POSITION_CYCLE)


class EnhancedJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if is_dataclass(o):
            return asdict(o)
        if isinstance(o, datetime):
            timestamp = o.replace(tzinfo=timezone.utc).timestamp() * 1000
            return int(timestamp)
        return super().default(o)


@dataclass
class PositionPacket:
    nonce: int
    device_id: str
    latitude: float
    longitude: float
    time: datetime

    fmt = "pILdd"

    def pack(self):
        timestamp = self.time.replace(tzinfo=timezone.utc).timestamp() * 1000
        timestamp = int(timestamp)
        return struct.pack(
            self.fmt,
            bytes(self.device_id, "utf-8"),
            self.nonce,
            timestamp,
            self.latitude,
            self.longitude,
        )

    def json(self):
        return json.dumps(self, cls=EnhancedJSONEncoder)


def connect_to_mqtt_broker(device_id, mqtt_host, mqtt_port):
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            logger.success(f"Client {client} connected to MQTT broker")
        else:
            logger.error(f"Client {client} could not connect. RC: {rc}")

    client = mqtt_client.Client(device_id, transport="websockets")
    client.on_connect = on_connect
    client.connect(host=mqtt_host, port=mqtt_port)
    return client


def get_next_pos(counter, jitter_scale=0.001):
    latitude, longitude = BOAT_POSITION_CYCLE[counter % N_POS]
    lat_jitter = random.uniform(-jitter_scale, jitter_scale)
    long_jitter = random.uniform(-jitter_scale, jitter_scale)

    return latitude + lat_jitter, longitude + long_jitter


def simulate_boat(device_id, mqtt_host, mqtt_port):
    client = connect_to_mqtt_broker(device_id, mqtt_host, mqtt_port)
    counter = 0
    steps = 10
    latitude, longitude = get_next_pos(counter)
    while True:
        next_lat, next_long = get_next_pos(counter + 1)

        latitudes = np.linspace(latitude, next_lat, num=steps)[:-1]
        longitudes = np.linspace(longitude, next_long, num=steps)[:-1]

        for lat, long in zip(latitudes, longitudes):
            packet = PositionPacket(
                nonce=counter,
                device_id=device_id,
                latitude=lat,
                longitude=long,
                time=datetime.now(),
            )
            client.publish("position_log", payload=packet.json())
            sleep(1 / steps)

        latitude = next_lat
        longitude = next_long

        counter += 1


def simulate_boats(n_boats, mqtt_host, mqtt_port):
    args = [(f"device_{n}", mqtt_host, mqtt_port) for n in range(n_boats)]
    with mp.Pool(n_boats) as pool:
        pool.starmap(simulate_boat, args)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="Boat Sim")
    parser.add_argument("n_boats", type=int)
    parser.add_argument("mqtt_host", type=str)
    parser.add_argument("mqtt_port", type=int)

    args = parser.parse_args()
    simulate_boats(args.n_boats, args.mqtt_host, args.mqtt_port)
