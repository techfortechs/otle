#!/usr/bin/env python3
# pylint: disable=wrong-import-position

import os
import sys
import json
import glob

from argparse import ArgumentParser

sys.path.insert(0, '.')
sys.path.insert(0, '..')

try:
    import manage # pylint: disable=unused-import
except ImportError as err:
    sys.stderr.write("Could not run script! Is manage.py not in the current"\
        "working directory, or is the environment not configured?:\n"\
        f"{err}\n")
    sys.exit(1)

from races.models import Race, Boat, RaceTick

class Command(object):
    """Sample command script"""
    def __init__(self):
        self.arg_parser = ArgumentParser(description=self.__doc__)
        self.add_arguments(self.arg_parser)
        self.args = self.arg_parser.parse_args(sys.argv[1:])

    def add_arguments(self, pars):
        """Add extra command arguments"""
        pars.add_argument("-n", "--name", type=str, required=True,\
            help='The name of this thing')

    def handle(self):
        # Your script goes here
        print(Race.objects.all())


if __name__ == '__main__':
    Command().handle()

