#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to give a long HW-based ID and get a temporary short boat ID in return. 
#
# Sets up the mqtt client, and requests/listens on 'otle_get_id'
#
# First version written 26 Aug 2022 by Scott Dynes scott@dynes.org
#
# 5 Oct 2022 sdynes run only if the boat_id.txt file has no entry
#

import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import time
import os
import gps
import datetime
import json
import math
import socket # should go away once have bluetooth way to find boat ID
import signal
import subprocess

########
# using fixed boat_ids for now...
exit(1)
#
########


t4t_homedir = "/home/pi/t4t/"
boat_id = ""

MQTT_SERVER = "192.168.50.14" # The MIT MQTT broker or Scott's laptop via tomato on Linksys
                              # Also the repo site for now

CLEAN_SESSION=True

AM_DONE = False



### test to see if this boat has an entery in boat_id.txt. if it does exit.
f = open((t4t_homedir + "boat_id.txt"), "r")
tmp = f.read()
f.close()
if (tmp is not '');
   exit(0)


# get the hw_id:
def get_dev_id():
    dev_id = subprocess.run(["cat /proc/cpuinfo | grep --ignore-case serial |rev| cut -d \" \" -f 1 | rev"], shell=True, executable='/bin/bash', stdout=subprocess.PIPE).stdout.decode('utf-8').splitlines()[0]
    if dev_id is not None:
        return dev_id
    else:
        syslog.syslog(syslog.LOG_ERROR,'No device_id found')

hw_id = get_dev_id()
my_topic =  "otle_get_id" + '/' + str(hw_id)

def extract_ip():
    st = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:       
        st.connect(('10.255.255.255', 1))
        IP = st.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        st.close()
    return IP

def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'ip_of_code_repo Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe("otle_get_id/"+str(hw_id),2)

    # publish the request
    client.publish(my_topic,json.dumps(["otle_get_id/"+str(hw_id), "otle_get_id", str(hw_id)]))
    syslog.syslog(syslog.LOG_INFO,"otle_get_id: published otle_get_id message")

    
def on_message(client, userdata, msg):
    # message should be of form 'current_version', <current hash>, <ip address>, <directory>
    # e.g. current_version, 42, 192.168.50.104, /Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code
    message = str(msg.payload.decode("utf-8","ignore"))
    syslog.syslog(syslog.LOG_INFO,'id generic message received: ' + str( message))


def on_otle_id(client, userdata, msg):
    global boat_id
    global AM_DONE
    # This is registered to the topic otle_get_id/<hw_id>
    message = str(msg.payload.decode("utf-8","ignore"))
    syslog.syslog(syslog.LOG_INFO,'otle_get_id message received: ' + str( message))
    print("In otle_get_id message: received " + message)

    # message should be of form "otle_set_id", <hw_id>, <boat_id>
    m = str(msg.payload.decode("utf-8"))
    # 
    print("message received  ",m)
    message = json.loads(str(msg.payload.decode("utf-8","ignore")))
    syslog.syslog(syslog.LOG_INFO,'otle_get_id message received: ' + str( message))
    # check to see if right message, then separate out the parts
    if ((message[0] == "otle_set_id") & (hw_id == message[1])):
        boat_id = message[2]
        syslog.syslog(syslog.LOG_INFO,"otle_get_id: boat_id is " + str(boat_id))
                      
    # store boat_id
    f = open((t4t_homedir + "boat_id.txt"), "w")
    f.write(str(boat_id))
    f.close()

    AM_DONE = True

id_client = mqtt.Client(hw_id,clean_session=CLEAN_SESSION)
id_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
id_client.on_connect = on_connect
id_client.on_message = on_message
id_client.message_callback_add("otle_get_id/"+str(hw_id), on_otle_id)
id_client.reconnect_delay_set(min_delay=1, max_delay=1200) # need to change if races are longer than 20 minutes
id_client.connect(MQTT_SERVER, keepalive=60)

id_client.loop_start()

while True:
    time.sleep(4.0)
    print(".", end = "")

    if AM_DONE:
        id_client.loop_stop()
        id_client.disconnect()
        exit(0)
        
