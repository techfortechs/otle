# get_up_ublox.py: Used to set up a chip_data.py:
# Helper functions to get paramaters from U-blox chip using the gpsd tools.
# Assumes that the .sh functions referred to are in teh same dir as this file
#
# This script by Scott Dynes scott@dynes.org
#
# First version 28-Apr-2022
#
# This assumes that the gpsd daemon is running, or at least run as root "gpsd  -n -N -s 57600 /dev/ttyAMA1"
#
#
# This also assumes that each chip has been set up to produce/read the appropriate RTCM3 things... (This should be part of this script)

import gps
import os
import datetime
import json
import math
import sys
import time



def get_chip_type(dir):
    
    n = 0
    out = ''
    while ((n < 4) and (out == '')):
        out = os.popen(dir+'get_chip_type.sh').read().rstrip()
        n += 1
           
    if( out==''):
        raise ValueError("Error communicating with chip to get chip type")
        os.popen('logger -i -t ublox "Error communicating with chip to get chip type"' )
    
    print("Chip ype: " + out)
    
    return(out)


def get_chip_protocol(dir):
    n = 0
    out = ''
    while ((n < 4) and (out == '')):
        out = os.popen(dir+'./get_chip_protocol.sh').read().rstrip()
        n += 1
           
    if( out==''):
        raise ValueError("Error communicating with chip to get chip protocol")
        os.popen('logger -i -t ublox "Error communicating with chip to get chip protocol"' )

    print("Chip protocol: " + out)

    return(out)



def get_active_status(dir):
    n = 0
    out = ''
    while ((n < 4) and (out == '')):
        out = os.popen(dir+'./get_active_status.sh').read().rstrip()
        n += 1
           
    if( out==''):
        raise ValueError("Error communicating with chip to get active status")
        os.popen('logger -i -t ublox "Error communicating with chip to get active status"' )

    print("Active status: " + out)

    return(out)


def get_valid_status(dir):
    n = 0
    out = ''
    while ((n < 4) and (out == '')):
        out = os.popen(dir+'./get_valid_status.sh').read().rstrip()
        n += 1
           
    if( out==''):
        raise ValueError("Error communicating with chip to get valid status")
        os.popen('logger -i -t ublox "Error communicating with chip to get valid status"' )
    print("Valid status: " + out)

    return(out)


def get_hp_ll_as_dict(json_data):

    EXIT_CODE = 0
    TPV = None
    
    try:
         while True:
            SENTENCE = json.loads(json_data.pop())
            if 'class' in SENTENCE and SENTENCE['class'] == 'TPV':
                TPV = SENTENCE
            if TPV is not None:
                sys.stdin.close()
                break
        
        
    except (IOError, ValueError):
        # Assume empty data and write msg to stderr.
        EXIT_CODE = 100
        sys.stderr.write("Error reading JSON data from file or stdin."
                         " Creating an empty or partial skyview image.\n")
    
    
    lat = SENTENCE['lat']
    long =  SENTENCE['lon']
    time = SENTENCE['time']
    course_t = SENTENCE['track']
    course_m = SENTENCE['magtrack']
    speed = SENTENCE['speed']
    
    
    boat_dict = {
        "lat": lat,
        "long": long,
        "time": time,
        "course_t": course_t,
        "course_m": course_m,
        "speed": speed,
        "boat_id": 2,
        "skipper": "Alan Hale Jr",
        }
    
    return(boat_dict)
    
    #print(f'{boat_dict["long"]:.9f}')
    
    
