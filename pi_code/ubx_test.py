from ublox_gps import UbloxGps
import serial
# Can also use SPI here - import spidev
# I2C is not supported

port = serial.Serial('/dev/ttyAMA1', baudrate=57600, timeout=1)
gps = UbloxGps(port)


#def survey_in():
#    try:
#        print("Setting time3 to 0")
        
def run():

  try:
    print("Listenting for UBX Messages.")
    while True:
      try:
        coords = gps.hp_geo_coords()
        print(coords.lonHP, coords.latHP)
      except (ValueError, IOError) as err:
        print(err)

  finally:
    port.close()

if __name__ == '__main__':
  run()
