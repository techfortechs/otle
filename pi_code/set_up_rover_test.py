# set_up_rover.py: Used to set up RPi with Sparkfun U blox Z-F9P and Adafruit RFM95W LoRa Radio Transceiver Breakout
#
# This script by Scott Dynes scott@dynes.org
#
# This script based on breadboard setup
# First version 30-Mar-2022
#
#
#

import rfm9x_tools   # expected to be in python path somewhere...
import time
import serial
import serial.threaded

# node mumber of the rovers. This had better be unique in your radio environment...
rover_node = 142

# set up Pi serial port. Make sure that pi is configured so no serial login shell, and serial port is on
ser = serial.Serial(
        port='/dev/ttyAMA1', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        baudrate = 57600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
)
# This assumes that the serial port on the U blox chip (usually UART2) is set similarly.
# This is not the default; use u-center to check and change.

# Set up radio
rfm9x, node = rfm9x_tools.set_up(rover_node)  # the base node number can be anything; the rover nodes need to all be the same.

# Start reading the serial port. If something shows up, send it to the rovers


stop_time = time.time() + 30 # get 60 seconds of data

fname = "/home/pi/t4t/serial_data/rover_test_" + time.strftime("%Y_%j_%H%M%S", time.localtime())

with open(fname, "wb") as binary_file:

    while  (time.time() < stop_time):
        packet = None
            
        # check for packet rx                                                                      
        packet = rfm9x.receive()
        if packet is not None:
            packet_data = bytearray(packet)
            ser.write(packet_data)
            binary_file.write(packet_data)
            

    
        


