# SPDX-FileCopyrightText: 2018 Brent Rubell for Adafruit Industries                            
#                                                                                              
# SPDX-License-Identifier: MIT                                                                 

"""                                                                                            
Example for using the RFM9x Radio with Raspberry Pi.                                           
                                                                                               
Learn Guide: https://learn.adafruit.com/lora-and-lorawan-for-raspberry-pi                      
Author: Brent Rubell for Adafruit Industries                                                   
"""
# Import Python System Libraries                                                               
import time
# Import Blinka Libraries                                                                      
import busio
from digitalio import DigitalInOut, Direction, Pull
import board
import math

# Import RFM9x                                                                                 
import adafruit_rfm9x

def set_up(node):
    # Configure LoRa Radio                                                                         
    CS = DigitalInOut(board.CE1)
    RESET = DigitalInOut(board.D25)
    spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
    rfm9x = adafruit_rfm9x.RFM9x(spi, CS, RESET, 915.0)
    rfm9x.tx_power = 23
    rfm9x.node = node
    prev_packet = None

    node_num = node
    

def listen():
    while True:
        packet = None
        
        # check for packet rx                                                                      
        packet = rfm9x.receive()
        if packet is None:
            print('- Waiting for PKT -')
        else:
            # print packet text and rssi                                                           
            prev_packet = packet
            packet_text = str(prev_packet, "utf-8")
            print("Node 6 receives: " + packet_text)
            time.sleep(1)
        
        
        time.sleep(0.1)


def send(msg, dest_node=None):
    data = bytes(msg,"utf-8")
    rfm9x.send(data, node = node_num, destination = dest_node)
    print("Sent: " + msg + " to node " + to_str(dest_node))



    
    


