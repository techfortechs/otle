#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to hack an autoupdate functionality
#
# Sets up the mqtt client, and listens on 'otle_update' for devices to ping. Responds
# with the ip of the code repo, and the dir from which to copy .py and .sh files
#
# This should be replaced by something more better
#
# First version written 4 Aug 2022 by Scott Dynes scott@dynes.org

import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import time
import os
import gps
import datetime
import json
import math
import socket # should go away once have bluetooth way to find boat ID
import signal



MQTT_SERVER = "192.168.50.14" # The MIT MQTT broker or Scott's laptop via tomato on Linksys
                              # Also the repo site for now


MQTT_UPDATE_IP = "otle_update"
CLEAN_SESSION=True

def extract_ip():
    st = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:       
        st.connect(('10.255.255.255', 1))
        IP = st.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        st.close()
    return IP

def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'ip_of_code_repo Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe(MQTT_UPDATE_IP/#)

    
def on_message(client, userdata, msg):
    # message should be of form 'current_version', <boat_id>, <time>
    # e.g. current_version, 42, 2022-08-04 13:09:44.357520
    m = str(msg.payload.decode("utf-8"))
    # 
    print("message received  ",m)

    # check to see if right message, then separate out the parts
    if (m[0] is 'current_version'):
        boat = m[1]
        msg_time = m[2]

        # create the message.
        #Should be of the form  'current_version', <device_id>, <ip address>, <directory>
        # e.g. current_version, 42, 192.168.50.104, /Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code/
        reply = 'current_version, ' + my_ip + ', ' + repo_dir
        
        # send the response to the boat
        ip_client.publish((MQTT_UPDATE_IP + '/' + boat), msg, qos=2)


# set up connection
ip_client = mqtt.Client(client_name,clean_session=CLEAN_SESSION)
ip_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ip_client.on_connect = on_connect
ip_client.on_message = on_message

ip_client.reconnect_delay_set(min_delay=1, max_delay=1200) # need to change if races are longer than 20 minutes
ip_client.connect(MQTT_SERVER, keepalive=600)


# get my ip
my_ip = extract_ip()

# set the repo dir
repo_dir = '/Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code/'

