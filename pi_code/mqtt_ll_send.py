#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Script to query the u blox chip and send ll and other data to the mqtt broker
#
# Should be transport agnostic
#
# First version written 13 May 2022 by Scott Dynes scott@dynes.org
# 23_feb_2023 updated with AHRS daemon and data, and to use the current ICD
# 28-mar-2023 updating to handle boat/buoy/base distinctions (no AHRS),
#    and to fix using /dev/ttyS0 here as opposed to through gpsd
# 10-Apr-2023 updating to use pyubx2 tools scott@dynes.org
# 22-Aug-2023 greatly updated to use feathers talking to a base feather
# communicationg with central control rpi via USB serial.


import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import time
import os
import subprocess as sp
from datetime import datetime as dtime
import datetime
import json
import math
#import socket # should go away once have bluetooth way to find boat ID
import signal
from threading import Thread, Lock
import serial 


ser=serial.Serial("/dev/ttyACM0",115200)
ser.reset_input_buffer()


mqtt_msg_index = 0 # increment and send with each mqtt msg
mqtt_status_index = 0 # need to not send two status messages so keep track with this

t4t_homedir = "/home/pi/t4t/"

def get_dev_id():
    if not (os.path.isfile(t4t_homedir+"dev_id.txt")):
        os.system("/home/pi/otle/otle/pi_code/set_dev_id.sh")
    f = open(t4t_homedir+"dev_id.txt")
    dev_id = f.readline().strip()
    f.close()
    if dev_id is not None:
        return dev_id
    else:
        syslog.syslog(syslog.LOG_ERROR,'mqtt_ll_send: No device_id found')

dev_id = get_dev_id()



# create file name to store mqtt messages. Good for testing?
# check to see if dir exists; create if it does not
if not os.path.exists(t4t_homedir + "mqtt_data/"):
    os.mkdir(t4t_homedir + "mqtt_data/")
    
fname = t4t_homedir+"mqtt_data/device_" + str(dev_id) + time.strftime("%Y_%j_%H%M%S", time.localtime())
# ... and open it in append mode
mqtt_file =  open(fname, "wb")



#MQTT_SERVER = "192.168.50.14" #  The MIT MQTT broker or Scott's laptop via tomato on Linksys
MQTT_SERVER = "127.0.0.1" #  This local system

# MQTT_GPS_SEND = str(dev_id) + "/position"  #this is the name of the position topic
# MQTT_STATUS_SEND = str(dev_id) + "/device_status"  #this is the name of the status topic
MQTT_OTLE_REC = "over_early_q"
CLEAN_SESSION=True


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'mqtt_ll_send: Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe(MQTT_OTLE_REC)
    
 
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    m="message received  "  ,str(msg.payload.decode("utf-8"))
    q.put(m) #put messages on queue
    print("message received  ",m)

def on_publish (client, userdata, mid):
    global messages
    #m="on publish callback mid "+str(mid)
    #messages.append(m)

def on_log(client, userdata, level, buf):
    print("log: ",buf)

    
# create the ctl-c handler
def handler(signum, frame):
    # close the file and exit
    ser.close()
    mqtt_file.close()
    ll_client.loop_stop()
    ll_client.disconnect()
    exit(0)

signal.signal(signal.SIGINT, handler)

    
client_name = 'dev_' + dev_id

ll_client = mqtt.Client(client_name,clean_session=CLEAN_SESSION)
ll_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ll_client.on_connect = on_connect
ll_client.on_message = on_message
ll_client.on_publish = on_publish        #attach function to callback
ll_client.reconnect_delay_set(min_delay=1, max_delay=1200) # need to change if races are longer than 20 minutes
ll_client.connect(MQTT_SERVER, keepalive=60)


ll_client.loop_start()  #use this line if you want to write any more code here

while True:
    data = ser.readline().decode("utf-8")
    #print(data)
    try:
        data_dict = json.loads(data)
        if data_dict["type"] == "pos":
            MQTT_GPS_SEND = str(data_dict["node_id"]) + "/position"  #this is the name of the position topic
            # create bytearray
            packet = bytearray('otle', encoding='ascii')
            # ...and extend it...
            #print("lat: " + data_dict["lat"].to_bytes(5, byteorder='big', signed = True).hex())
            #print("lon: " + data_dict["lon"].to_bytes(5, byteorder='big', signed = True).hex())
            #print("lat: " + str(data_dict["lat"]) + "  lon: " + str(data_dict["lon"])  )
            #packet.extend(bytearray.fromhex(data_dict["node_id"]))
            packet.extend(data_dict["msg_cnt"].to_bytes(2, byteorder='big', signed = False))
            packet.extend(data_dict["timestamp"].to_bytes(5, byteorder='big', signed = False))
            packet.extend(int(data_dict["lat"]).to_bytes(5, byteorder='big', signed = True))
            packet.extend(int(data_dict["lon"]).to_bytes(5, byteorder='big', signed = True))
            packet.extend(data_dict["err_2d"].to_bytes(1, byteorder='big', signed = False))
            packet.extend(data_dict["heading_m"].to_bytes(1, byteorder='big', signed = False))
            packet.extend(data_dict["heading_t"].to_bytes(1, byteorder='big', signed = False))
            #print("out_packet: " + packet.hex()  + " len(packet): " + str(len(packet)))
            #print("publishing to ll_client")
            
            ret = ll_client.publish(MQTT_GPS_SEND, packet )
            mqtt_file.write(packet)
            #print("sending MQTT msg of size " + str(len(packet)) +  " to " + MQTT_GPS_SEND + "\n")
            if (ret[0] == mqtt.MQTT_ERR_NO_CONN):
                print('no connection to broker')
                break
    

        elif  data_dict["type"] == "info":            
            print("Info msg: " + data_dict["text"])
            
 
        elif  data_dict["type"] == "stat":            
            # print("creating status packet")
            MQTT_STATUS_SEND = str(data_dict["node_id"]) + "/device_status"  #this is the name of the status topic
            sys_packet = bytearray()
            sys_packet.extend(bytearray.fromhex(data_dict["node_id"]))
            sys_packet.extend(data_dict["timestamp"].to_bytes(5, byteorder='big', signed = False))
            sys_packet.extend(data_dict["rtk_status"].to_bytes(1,byteorder='big', signed = False))
            sys_packet.extend(bytearray.fromhex(data_dict["gen_status"]))                       
            sys_packet.extend(data_dict["RXQ_RSSI"].to_bytes(1,byteorder='big', signed = False)) # rssi is assumed to be negative; value is abs(rssi)
            sys_packet.extend(int(data_dict["packets"]).to_bytes(4,byteorder='big', signed = False))
            
            #print("publishing status packet to ll_client")
            ret = ll_client.publish(MQTT_STATUS_SEND, sys_packet )

            mqtt_file.write(sys_packet)

        else:
            print("unknown type: " + data_dict["type"])
        
    except json.JSONDecodeError as e:
        print(e.msg, e)
        print(repr(data))
