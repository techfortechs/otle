# set_up_ublox.py: Used to set up a  U-blox chip using the gpsd tools.
#
# This script by Scott Dynes scott@dynes.org
#
# First version 15-Apr-2022
# 10-Apr-2023 updated to use pyubx toolset scott@dynes.org
#
# The gpsd daemon should not be running
#
#
# This also assumes that each chip has been set up to produce/read the appropriate RTCM3 things... (This should be part of this script)

import time
import serial
import os
import sys
import syslog
from threading import Thread, Lock
from serial import Serial
import pyubx2_tools as pt
from pyubx2 import (
    UBXMessage,
    UBXReader,
    UBX_MSGIDS,
    SET,
    POLL,
    UBX_PROTOCOL
)


# create the serial lock
serial_lock = Lock()
# set the serial lock in the pyubx2_tools:

pt.set_serial_lock(serial_lock)

# set debug to True to get lots of print statements, and False to turn off
debug_mode = False
pt.set_debug(debug_mode)  # pyubx2_tools will emit debugging info if True

# make sure /dev/ttyS0 and /dev/ttyAMA1 are set up
os.popen('stty -F /dev/ttyAMA1 57600' )
os.popen('stty -F /dev/ttyS0 57600' )


# sleep to let things get settled after a cold boot and service start
if not debug_mode:
    time.sleep(120)

t4t_homedir = '/home/pi/t4t/'

# add homedir to the PYTHONPATH
sys.path.append(t4t_homedir)


# Try to set the outputs on the chip to ubx and RTCM only. Complain if we cannot
try:
    # set only ubx and RTMC3 messages on the chip
    pt.set_ubx_rtcm_only()
except Exception as e:
    print (f"set_up_ublox: can't set ubx and RTCM; {e} ")
    sys.exit()

    
# First, see what kind of chip and protocol we have.
[ChipType, ChipProtocol] = pt.get_chip_info()
if debug_mode:
    print(f"ChipType: {ChipType}, protocol: {ChipProtocol} ")

# Next, see what kind of device this is (base for outputting RTCM3 msgs, rovers for receiving RTCM3 msgs and outputting ID, lat, long
#
# This should follow from some file in some directory

if( os.path.exists(t4t_homedir+'/me.rover')):
    DeviceType = 'rover'

if( os.path.exists(t4t_homedir+'/me.base')):
    DeviceType = 'base'
if debug_mode:
    print(f"Device type: {DeviceType}")


# set up the base
if( DeviceType == 'base'):

    print("running base script")
    syslog.syslog(syslog.LOG_INFO,"set_up_ublox: running base script" )

    # see that we have a ZED-F9P. If not, throw error
    if(ChipType != 'ZED-F9P'):
        raise ValueError("Base devices must have a chip capable of generating RTCM3 data, such as the ZED-F9P")
        syslog.syslog(syslog.LOG_ERR,"set_up_ublox: Base devices must have a chip capable of generating RTCM3 data, such as the ZED-F9P" )
    # set up RTCM3 outputs thru UART2
    pt.set_up_RTCM3_msgs()


    # Start the survey-in. To do this disable the survey-in, and then start it.
    pt.start_survey_in()

    # see if we are in survey-in mode
    [mode, dur, meanAcc]  = pt.get_survey_status()
    if( mode == 0):
        syslog.syslog(syslog.LOG_ERR,"set_up_ublox: Error: GPS chip failed to commence survey-in")
        raise ValueError("set_up_ublox: Error: GPS chip failed to commence survey-in")

    print("Surveying in at " + str(time.time()))
    syslog.syslog(syslog.LOG_INFO,'set_up_ublox: Surveying in')
    
    # check every so often to see if the survey has completed. Raise an error after 6 minutes
    end_time = dur + 6 * 60
    accLimit = str(pt.get_accLimit())
    print(f"AccLimit is {accLimit}")
    
    # loop to see if we complete survey-in in less than 5 minutes
    [mode, dur, meanAcc]  = pt.get_survey_status()
    while((dur < end_time) and (mode != 1)):

        time.sleep(21)
        
        # check valid status
        [mode, dur, meanAcc] = pt.get_survey_status()
        syslog.syslog(syslog.LOG_INFO,"set_up_ublox: At dur " + str(dur) + " mode is " + str(mode) + " and meanAcc is " + str(meanAcc)  )
        print("At time " + str(time.time()) + "set_up_ublox: At dur " + str(dur) + "mode is " + str(mode) + " and meanAcc is " + str(meanAcc))

    # 6 minutes have passed. check to see if we have a valid survey-in status
    [mode, dur, meanAcc]  = pt.get_survey_status() 
    if(mode != 1):
        raise ValueError("Error: GPS chip failed to end survey-in after 6 minutes. Check antenna connection and sky visibility.")
        syslog.syslog(syslog.LOG_ERR,f"set_up_ublox: GPS chip failed to end survey-in after 6 minutes. dur: {dur} meanAcc: {meanAcc} accLimit: {accLimit} ")
        
    # We are in survey mode. Activate the radio to broadcast RTCM3 messages
    print("Chip should be surveyed in and emitting RTCM3 data")
    syslog.syslog(syslog.LOG_INFO,"set_up_ublox: Chip should be surveyed in and emitting RTCM3 data")
    
    print("Starting base radio to transmit RTCM3 data...")
    syslog.syslog(syslog.LOG_INFO,"set_up_ublox: Starting mqtt_ll_send and  base radio to transmit RTCM3 data...")
 
    # start mqtt_ll_send
    os.popen('sudo systemctl start otle_systemd_mqtt_ll_send' )
    
    import set_up_base

    # We should be done. 


# set up the rover
if( DeviceType == 'rover'):

    print("running rover script")
    syslog.syslog(syslog.LOG_INFO, "set_up_ublox: running rover script")

    # set only ubx and RTMC3 messages on the chip
    pt.set_ubx_rtcm_only()
    
    # set chip to attempt to resolve phase ambiguities
    pt.set_ubx_rtk_fix()

    # Disable survey-in
    pt.disable_survey_in()
    print("Disabled survey-in")
    
    # set the update speed to 5x/sec
    pt.set_nav_soln_rate(200)
    print("Updated position sample rate to 5x/sec")
    syslog.syslog(syslog.LOG_INFO, "set_up_ublox: Updated position sample rate to 5x/sec" )
    
    # Start the rover radio
    print("Starting rover radio to recceive RTCM3 data...")
    syslog.syslog(syslog.LOG_INFO, "set_up_ublox: Starting rover radio to receive RTCM3 data..." )

    # start mqtt_ll_send
    os.popen('sudo systemctl start otle_systemd_mqtt_ll_send' )

    import set_up_rover




