#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Script to accept data from the mqtt broker, decode the byte string,
# print the data to the command line
#
#
# This version written 8 Mar 2023 by Scott Dynes scott@dynes.org
# 22 Mar 2023  incorporating status data sbcd
#

import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import os
import numpy as np
import pandas as pd
import time
import signal
from datetime import datetime as dtime, timedelta

file_dir = os.path.dirname(sys.argv[0])

os.system('clear')  # clear the screen

# set the status variables to hae some value
carrSoln = uptime = rssi_lora = rssi_wifi = meas_time = temp = 0

# set the datetime epoch:
epoch = dtime.strptime('Q2020-01-01T00:00:00.000Z', "Q%Y-%m-%dT%H:%M:%S.%f%z")

here_i_am_q = Queue()

MQTT_SERVER = '192.168.4.1' #  MIT t4t broker or Scott's laptop via tomato on Linksys
#MQTT_SERVER = "192.168.83.171"  # MQTT broker for a test on scott's hotspot network
MQTT_GPS_SEND = "+/position" # this is the name of the position topic
MQTT_OTLE_REC = "over_early_q"

# define pandas array for mqtt data
in_data = pd.DataFrame({'dev_id': pd.Series(dtype='str'),
                   'time': pd.Series(dtype='str'),
                        'lat': pd.Series(dtype='str'),
                        'lon': pd.Series(dtype='str'),
                   'err_2d': pd.Series(dtype='str'),
                   'heading_m': pd.Series(dtype='str'),
                        'course_t': pd.Series(dtype='str'),
                        'rtk_status': pd.Series(dtype='str'),
                        'cpu_temp': pd.Series(dtype='str'),
                        'uptime': pd.Series(dtype='str'),
                        'lora_rssi': pd.Series(dtype='str'),
                        'wifi_rssi': pd.Series(dtype='str'),})

def on_message(mosq, obj, msg):
    # This callback will be called for messages that we receive that do not
    # match any patterns defined in topic specific callbacks, i.e. in this case
    # those messages that do not have topics $SYS/broker/messages/# nor
    # $SYS/broker/bytes/#
    #print("m", end = "")
    #print("topic: " + msg.topic + " qos: " + str(msg.qos) + " payload: " + str(msg.payload))
    my_a = 1

def on_here_i_am(mosq, obj, msg):

    global in_data #, carrSoln, uptime, rssi_lora, rssi_wifi, meas_time, temp

   
    message = msg
    pl = message.payload  # no decoding; it's a bytestring
    dev_id = message.topic[0:4] # the dev_id should be the first 4 characters
    
    # check to see we have a valid payload
    if (pl[0:4] == b'otle'):
        mqtt_msg_index = int.from_bytes(pl[4:6], byteorder='big', signed = False)
        meas_time = epoch + timedelta(seconds = (int.from_bytes(pl[6:11], byteorder='big', signed = False)/10))
        llat = int.from_bytes(pl[11:16], byteorder='big', signed = True)* 1e-9
        llong = int.from_bytes(pl[16:21], byteorder='big', signed = True)* 1e-9
        err_2d = int.from_bytes(pl[21:22], byteorder='big', signed = False)/200
        course_m = int.from_bytes(pl[22:23], byteorder='big', signed = False)*360/256
        course_t = int.from_bytes(pl[23:24], byteorder='big', signed = False)*360/256
    
        # create a new dataframe  with this data
        new_df = pd.DataFrame({'dev_id': [str(dev_id)], 'time': [str(meas_time)], 'lat': [str(llat)],\
                               'lon': [str(llong)], 'err_2d':  [str(err_2d)], 'heading_m': [str(course_m)], 'course_t': [str(course_t)]})

        #print(new_df)
        # enter data into numpy array. see if we have this dev_id; otherwise create a new row
        # print("dev_id present? " + str(str(dev_id) in in_data["dev_id"].values))

        if ((not in_data.empty) and (str(dev_id) in in_data["dev_id"].values)):
            n = in_data[in_data['dev_id']==str(dev_id)].index.values[0]
            in_data.loc[n,['dev_id', 'time', 'lat', 'lon', 'err_2d', 'heading_m', 'course_t']] = [str(dev_id),meas_time.strftime("%m/%d/%Y, %H:%M:%S"),f'{llat:.7f}', \
                                                                        f'{llong:.7f}',str(err_2d),str(course_m),str(course_t)]
        else:
            # add a new row
            in_data = pd.concat([in_data, new_df], ignore_index = True)
            #... and then sort by dev_id
            in_data.sort_values(by=['dev_id'])


        
        #n = len(in_data)
        #astr = "\x1b[" + str(n+3) + "F"
        #bstr = bytearray(astr, encoding='ascii')
        #os.write(1, bstr)
        ##print("bstr is" + str(bstr))
        #print(f"ni is: {n}")
#        os.system('clear')
#        print(in_data) 


#    time.sleep(1)
        
def status_msg(mosq, obj, msg):

    global in_data #, carrSoln, uptime, rssi_lora, rssi_wifi, meas_time, temp

    message = msg
    pl = message.payload  # no decoding; it's a bytestring
    dev_id = message.topic[0:4] # the dev_id should be the first 4 characters

    #print(f"in status message; dev_id is {dev_id}")
    
    # no check to see we have a valid payload
    status_msg_index = int.from_bytes(pl[0:2], byteorder='big', signed = False)
    meas_time = epoch + timedelta(seconds = (int.from_bytes(pl[2:7], byteorder='big', signed = False)/10))
    carrSoln = int.from_bytes(pl[7:8], byteorder='big', signed = False)
    temp = int.from_bytes(pl[8:9], byteorder='big', signed = False)
    uptime = int.from_bytes(pl[9:12], byteorder='big', signed = False)
    rssi_lora = int.from_bytes(pl[12:13], byteorder='big', signed = True)
    rssi_wifi = int.from_bytes(pl[13:14], byteorder='big', signed = True)

    # create a new dataframe with this data
    new_df = pd.DataFrame({'dev_id': [str(dev_id)], 'time': [str(meas_time)], 'rtk_status': [str(carrSoln)],\
                'cpu_temp': [str(temp)], 'uptime':  [str(uptime)], 'lora_rssi': [str(rssi_lora)], 'wifi_rssi': [str(rssi_wifi)]})
    #print("new_df")
    #print(new_df)
    #print("pre_in_data")
    #print(in_data)
    # enter data into numpy array. see if we have this dev_id; otherwise create a new row
#    print("dev_id present? " + (str(dev_id) in in_data["dev_id"].values))
    if ((not in_data.empty) and (str(dev_id) in in_data["dev_id"].values)):
        n = in_data[in_data['dev_id']==str(dev_id)].index.values[0]
        #print("n is: " + str(n))
        in_data.loc[n, ['dev_id','time','rtk_status','cpu_temp','uptime','lora_rssi','wifi_rssi']] = [str(dev_id),str(meas_time), \
                                                            str(carrSoln),str(temp),str(uptime),str(rssi_lora),str(rssi_wifi)]
    #print("post_in_data")
    #print(in_data)
        
    # if a row for this ID doesn't exist let the 'on_here_i_am' handler add it - doesn't make sense to have status without position data
#    else:
#        # add a new row
#        in_data = in_data.append({'dev_id': [str(dev_id)], 'time': [str(meas_time)], 'rtk_status': [str(carrSoln)],\
#                'cpu_temp': [str(temp)], 'uptime':  [str(uptime)], 'lora_rssi': [str(rssi_lora)], 'wifi_rssi': [str(rssi_wifi)]}, ignore_index = True)
#        #... and then sort by dev_id
#        in_data.sort_values(by=['dev_id'])
 
    # and now print it after going up the number of lines in in_data
#    os.system('clear')
#    print(in_data) 
        
#    time.sleep(1)
    
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe("here_i_am")
    

# create the ctl-c handler
def handler(signum, frame):
    # close the connection and exit 
    ll_client.loop_stop()
    ll_client.disconnect()
    exit(0)

signal.signal(signal.SIGINT, handler)

# Set up the broker
client_name = 'otle_processor'

ll_client = mqtt.Client(client_name,transport='websockets')
ll_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ll_client.on_message = on_message
ll_client.message_callback_add("+/status", on_here_i_am)
ll_client.message_callback_add("+/device_status", status_msg)
#ll_client.on_publish = on_publish        #attach function to callback
ll_client.reconnect_delay_set(min_delay=1, max_delay=1200) # need to change if races are longer than 20 minutes
ll_client.connect(MQTT_SERVER, port=9001, keepalive=60)

ll_client.subscribe("#",0)  # subscribe to everything

ll_client.loop_start()  #use this line if you want to write any more code here
while True:
    n = len(in_data)
    astr = "\x1b[" + str(n+1) + "F"
    bstr = bytearray(astr, encoding='ascii')
    os.write(1, bstr)
    #print("bstr is" + str(bstr))
    #print(f"ni is: {n}")
    # os.system('clear')
    print(in_data) 

    time.sleep(1)
