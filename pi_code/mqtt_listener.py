#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to listen for devices joining, and plot the positions of the devices sending out the
# hash of the current main branch.
#
# More messy than that; just looks in the code dir for the hash from the last commit.
# At some point need to be more structured and maybe just pull from git
#
# First version written 19 Aug 2022 by Scott Dynes scott@dynes.org
#
# 25 Aug 2022 sbcd expanded to be a catch-all for many mqtt listeners
#
# This should be run on 'server' before any device is turned on.
#
# 23-Sept-2022  by Scott Dynes scott@dynes.org
# For brass rat defining boats and marks based on hw id
# updated 30 sept because Matt has the original r/c
# hw_id                boat/mark     id_num
# 100000005a8ff519      pin          128+2
# 10000000f7a7022e      base          128+0
# 100000009c7a8867      RC            128+1
# with matt 30 sept 1000000098221281      RC            128+1
# This should be handled by associating django device objects with
# boat objects in the near future
# 5 Oct for now moving to static device/boat ids - so a) need all HW_ids associated with a boat_id,
# and b) need get_boat_id to ask for one only if there is no boat_id on the device. Every device
# should have an id when it starts up.
#

import time
from datetime import datetime
import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import os
import threading
import socket
import json
import signal
import numpy as np

# for printing out dots
timer_count = 0

# define queues
id_q = Queue()
update_q = Queue()

# boat_id_dic holds device HW id:given device ID pairs
boat_id_dict = {'100000005a8ff519':'130', '10000000f7a7022e':'128', '100000009c7a8867':'129', '1000000098221281':'1', '1000000070072aaa':'0'  } # the real one 
#boat_id_dict = {                           '10000000f7a7022e':'129', '100000009c7a8867':'130', '1000000098221281':'1' }  # the testing one

def extract_ip():
    IP = os.popen("ip a s wlan0 | egrep -o 'inet [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | cut -d' ' -f2").read().rstrip()
    return IP


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # *** Be sure to subscribe to all topics handled here
    ret = client.subscribe([('otle_update',2),('otle_get_id',2)])
    

# The callback for when a PUBLISH message is received from the server.
# All the data munging and recording work is done here.

def on_update_message(client, userdata, msg):
    # the sender expects a response of
    # the form 'current_version', <current hash>, <ip address>, <directory>
    print("In update msg handler. Topic: " + str(msg.topic) + ", message: " + str(msg.payload.decode("utf-8")))
    update_q.put(msg)


def on_get_my_id(client, userdata, msg):
    # the sender expects a response of
    # the form 'current_version', <current hash>, <ip address>, <directory>
    print("In get_id msg handler. Topic: " + str(msg.topic) + ", message: " + str(msg.payload.decode("utf-8")))
    id_q.put(msg)


def on_message(mosq, obj, msg):
    # This callback will be called for messages that we receive that do not
    # match any patterns defined in topic specific callbacks, i.e. in this case
    # those messages that do not have topics $SYS/broker/messages/# nor
    # $SYS/broker/bytes/#
    print("m", end = "")
    #print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


# create the ctl-c handler
def handler(signum, frame):
    # close the connection and exit 
    otle_client.loop_stop()
    otle_client.disconnect()
    exit(0)

signal.signal(signal.SIGINT, handler)

    
# set up vars
my_ip = extract_ip() # my ip address
code_dir = os.path.dirname(os.path.abspath(__file__)) + "/cur_ver_pi_code"   # for now
current_hash = open("./cur_ver_pi_code/current_version.txt", "r").readline().strip()

    
# Set up the broker

MQTT_SERVER = '192.168.50.14' #  MIT or Scott's laptop via tomato on Linksys

client_name = 'otle_listener'

otle_client = mqtt.Client(client_name)
otle_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
otle_client.on_message = on_message
otle_client.message_callback_add("otle_update/#", on_update_message)
otle_client.message_callback_add("otle_get_id/#", on_get_my_id)
otle_client.reconnect_delay_set(min_delay=1, max_delay=3600) # need to change if races are longer than 20 minutes
otle_client.connect(MQTT_SERVER, keepalive=60)

otle_client.subscribe("#", 0)  # subscribe to everything

otle_client.loop_start()  #use this line if you want to write any more code here
while True:

    if not id_q.empty():
        message = id_q.get()
        msg = json.loads(str(message.payload.decode("utf-8","ignore")))
        print("In get_id queue handler. Topic: " + str(message.topic))
        print("msg[0]: " + str(msg[0]))
        print("str(msg[1]) == 'otle_get_id': " + str(str(msg[1]) == "otle_get_id"))
        print("msg[1]: " + str(msg[1]))
        print("msg[2]: " + str(msg[2]))

        # message should be of form ["otle_get_id", "otle_get_id", <hw_id>]
        if(str(msg[1]) == "otle_get_id"):
            # get the HW_id
            hw_id = str(msg[2])
            print("otle_get_id: msg[1] == otle_get_id")
            # see if ID in dict already, and create the dict entry and boat_id
            if hw_id in boat_id_dict:
                # return the boat_id
                boat_id = boat_id_dict.get(hw_id)
                print("otle_get_id: dict already has key")
            else:
                # create the entry with the next boat_id
                # find the next boat_id
                if (len([int(x) for x in boat_id_dict.values() if int(x) < 128]) > 0):
                    boat_id = str(int(np.max([int(x) for x in boat_id_dict.values() if int(x) < 128]))+1)
                else:
                    boat_id = int(0) # 0-based indexing for boats. who knew?
                # enter new values into boat_id_dict
                print("hw_id: " + hw_id + " boat_id: " + str(boat_id))
                boat_id_dict.update([(hw_id, str(boat_id))])
                print("otle_get_id: new key; boat_id is " + str(boat_id))

            print(boat_id_dict)
            
            # return value
            otle_client.publish(message.topic,json.dumps(["otle_set_id", hw_id, str(boat_id)]))

            # print new dict
            print(boat_id_dict)

    if not update_q.empty():
        message = update_q.get()
        msg = str(message.payload.decode("utf-8"))
        print("In update msg handler. Topic: " + str(message.topic) + ", message: " + msg)
        if( msg == "send_update_info"):
            otle_client.publish(message.topic,json.dumps(["current_version", current_hash, my_ip, code_dir]))

    
    time.sleep(0.5)
    timer_count += 1
    #if (timer_count%10 == 0):
    #    print(".", end = "", flush=True)


        
