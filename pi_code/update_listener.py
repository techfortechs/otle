#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to listen for devices joining, and plot the positions of the devices sending out the
# hash of the current main branch.
#
# More messy than that; just looks in the code dir for the hash from the last commit.
#
# Listen to 
#
# At some point need to be more structured and maybe just pull from git
#
# First version written 19 Aug 2022 by Scott Dynes scott@dynes.org
#
# 25-Aug_2022 succeeded by mqtt_listener.py which consumes all functionality here.
#


import time
from datetime import datetime
import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import os
import threading
import socket
import json

q = Queue()

def extract_ip():
    st = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:       
        st.connect(('10.255.255.255', 1))
        IP = st.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        st.close()
    return IP

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe(MQTT_UPDATE_IP )
    

# The callback for when a PUBLISH message is received from the server.
# All the data munging and recording work is done here.

def on_update_message(client, userdata, msg):
    # the sender expects a response of
    # the form 'current_version', <current hash>, <ip address>, <directory>
    print("In update msg handler. Topic: " + str(msg.topic) + ", message: " + str(msg.payload.decode("utf-8")))
    if(str(msg.payload.decode("utf-8")) == "send_update_info"):
       client.publish(msg.topic,json.dumps(["current_version", current_hash, my_ip, code_dir]))


def on_message(mosq, obj, msg):
    # This callback will be called for messages that we receive that do not
    # match any patterns defined in topic specific callbacks, i.e. in this case
    # those messages that do not have topics $SYS/broker/messages/# nor
    # $SYS/broker/bytes/#
    print("m", end = "")
    #print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


# set up vars
my_ip = extract_ip() # my ip address
code_dir = '/Users/sdynes/IBI/MIT_t4t/tech4techs/cur_ver_pi_code'   # for now
current_hash = open("/Users/sdynes/IBI/MIT_t4t/tech4techs/cur_ver_pi_code/current_version.txt", "r").readline().strip()

    
# Set up the broker

MQTT_SERVER = '192.168.50.14' #  MIT or Scott's laptop via tomato on Linksys
MQTT_UPDATE_IP = "otle_update"

client_name = 'otle_updater'

ip_client = mqtt.Client(client_name)
ip_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ip_client.on_message = on_message
ip_client.message_callback_add("otle_update/#", on_update_message)
ip_client.reconnect_delay_set(min_delay=1, max_delay=3600) # need to change if races are longer than 20 minutes
ip_client.connect(MQTT_SERVER)

ip_client.subscribe("#", 0)  # subscribe to everything

ip_client.loop_start()  #use this line if you want to write any more code here
while True:
    
    time.sleep(2.0)


        
