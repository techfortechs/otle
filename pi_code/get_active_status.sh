#!/bin/bash                                                                                                                              

# this should return the status of the 'valid' variable of UBX-NAV-SVIN
#
# Used for determining if the chip has completed surveying in (whihc would be 1)
#                                                                                                                                        
# Assumes that the gpsd daemon is running, or at least run as root "gpsd  -n -N -s 57600 /dev/ttyAMA1"                                   
#                                                                                                                                        
# This script by Scott Dynes scott@dynes.org                                                                                             
#                                                                                                                                        
# First version 15-Apr-2022                                                                                                            

# Query the chip                                                                                                                         
mod=`ubxtool -p NAV-SVIN | grep -A 5 'UBX-NAV-SVIN:' | awk '/valid/ {print}' |awk '{print $6}'`
value=${mod%$'\n'}
echo $value
