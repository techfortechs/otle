#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to ask devices listening to topic otle_update/everyone_update to update
#
# Must be called from the pi_code dir so the script can find current_version.txt
#
# First version written 22 Aug 2022 by Scott Dynes scott@dynes.org
#

import time
from datetime import datetime
import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import os
import threading
import socket
import json

def extract_ip():
    IP = os.popen("ip a s eth1 | egrep -o 'inet [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | cut -d' ' -f2").read().rstrip()
    return IP

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe(MQTT_UPDATE_IP )
    

# The callback for when a PUBLISH message is received from the server.
# All the data munging and recording work is done here.


def on_message(mosq, obj, msg):
    # This callback will be called for messages that we receive that do not
    # match any patterns defined in topic specific callbacks, i.e. in this case
    # those messages that do not have topics $SYS/broker/messages/# nor
    # $SYS/broker/bytes/#
    print("in the generic msg handler")
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


# set up vars
my_ip = extract_ip() # my ip address
code_dir = os.path.dirname(os.path.abspath(__file__)) + "/cur_ver_pi_code"   # for now
current_hash = open("./cur_ver_pi_code/current_version.txt", "r").readline().strip()

    
# Set up the broker

MQTT_SERVER = '192.168.50.14' #  MIT or Scott's laptop via tomato on Linksys
MQTT_UPDATE_IP = "otle_update"

client_name = 'otle_updater'

ip_client = mqtt.Client(client_name)
ip_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ip_client.on_message = on_message
ip_client.reconnect_delay_set(min_delay=1, max_delay=3600) # need to change if races are longer than 20 minutes
ip_client.connect(MQTT_SERVER)

ip_client.publish("otle_update/everyone_update",json.dumps(["current_version", current_hash, my_ip, code_dir]))



        
