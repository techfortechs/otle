#!/usr/bin/python3                                                                                                                                                                                    

from threading import Thread

mag_heading = 0

def set_mag_heading():
    import subprocess
    global mag_heading

    popen = subprocess.Popen(["minimu9-ahrs", "--output=euler"], stdout=subprocess.PIPE, universal_newlines=True)
    for stdout_line in iter(popen.stdout.readline, ""):
        vals = stdout_line.split()
        mag_heading = float(vals[0]) + 180.0 * int(abs(float(vals[1])) > 90.0)
        #print('magnetic heading: ' + str(mag_heading))                                                                                                                                              \
                                                                                                                                                                                                      
    popen.stdout.close()

heading_daemon = Thread(target=set_mag_heading, daemon=True, name='MagHeadingDaemon')
heading_daemon.start()

while(True):
    print(str(mag_heading))


