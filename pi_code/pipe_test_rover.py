#!/usr/bin/python3

import os, sys
path = "rtcm3_pipe"
try:
    fifo = open(path, "r")
except Exception as e:
    print (e)
    sys.exit()

while True:
    r = fifo.read(1)
    print ("Received:", r)

fifo.close()
