#!/bin/bash

# set device id, and the hostname
# 19-Feb_2023 scott@dynes.org
#

FILE=/home/pi/t4t/dev_id.txt
dev_id=`cat /sys/class/net/eth0/address | rev | cut -c 1,2,4,5 | rev`

echo $dev_id > $FILE

if test -f "/home/pi/t4t/me.base"; then
    sudo hostnamectl set-hostname "otle_base_$dev_id"
    if [ -n "$(grep 127.0.1.1 /etc/hosts)" ]
    then
        echo "$HOSTNAME Found in your $ETC_HOSTS, Removing now...";
        sudo sed -i '/127.0.1.1.*/d' /etc/hosts
    fi
    sudo -- sh -c -e  "echo '127.0.1.1       `cat /etc/hostname`' >> /etc/hosts"
    logger "in set_dev_id: set hostname to otle_base_$dev_id"
elif test -f "/home/pi/t4t/me.rover"; then
    sudo hostnamectl set-hostname "otle_rover_$dev_id"
    if [ -n "$(grep 127.0.1.1 /etc/hosts)" ]
    then
        echo "$HOSTNAME Found in your $ETC_HOSTS, Removing now...";
        sudo sed -i '/127.0.1.1.*/d' /etc/hosts	
    fi
    sudo -- sh -c -e  "echo '127.0.1.1       `cat /etc/hostname`' >> /etc/hosts"
    logger "in set_dev_id: set hostname to otle_rover_$dev_id"
else 
    sudo hostnamectl set-hostname "otle_cmd_ctl_$dev_id"
    if [ -n "$(grep 127.0.1.1 /etc/hosts)" ]
    then
        echo "$HOSTNAME Found in your $ETC_HOSTS, Removing now...";
        sudo sed -i '/127.0.1.1.*/d' /etc/hosts
    fi
    sudo -- sh -c -e  "echo '127.0.1.1       `cat /etc/hostname`' >> /etc/hosts"
    logger "in set_dev_id: set hostname to otle_other_$dev_id"
fi
    


