#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to accept data from the mqtt broker, and plot the positions of the devices.
#
# At some point this should write to a database
#
# First version written 19 May 2022 by Scott Dynes scott@dynes.org
#

import numpy as np
import pandas as pd
import geopandas as gpd
import contextily as cx
import time
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from shapely.geometry import Point, LineString
from shapely.geometry import asPoint
from numpy import asarray
import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import time
import os
import datetime
import math
import numpy as np
import pandas as pd
import geopandas as gpd
import contextily as cx
import datetime
import matplotlib.pyplot as plt
import json
import threading

gpd.options.display_precision = 9

q = Queue()
proj = None

# create the empty geopandas dataframe
headers = ['geometry','time', 'course_t', 'course_m', 'speed', 'boat_id', 'skipper']
df = gpd.GeoDataFrame(columns=headers, crs="EPSG:4326")


def lonlat2UTM(lonlat):  # lonlat is [long, lat]
    utm = (np.floor((lonlat[0] + 180) / 6) % 60) + 1
    if(lonlat[1] > 0): 
        utm = utm + 32600
    else:
        utm = utm + 32700
    return int(utm)

def rotation(r):
    return np.asarray([np.cos(r), -np.sin(r), np.sin(r), np.cos(r)]).reshape(2, 2)



# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe(MQTT_GPS_SEND )
    

# The callback for when a PUBLISH message is received from the server.
# All the data munging and recording work is done here.
def on_message(client, userdata, msg):
    global q
    # put the data into the gpd dataframe. Convert the long/lat to a gpd Point
    q.put(str(msg.payload.decode("utf-8","ignore")))


# Set up the broker

MQTT_SERVER = '192.168.16.2' #  Scott's laptop via tomato on Linksys
MQTT_GPS_SEND = "here_i_am" # this is the name of topic, like temp
MQTT_OTLE_REC = "over_early_q"

client_name = 'otle_processor'

ll_client = mqtt.Client(client_name)
ll_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ll_client.on_message = on_message
#ll_client.on_publish = on_publish        #attach function to callback
ll_client.reconnect_delay_set(min_delay=1, max_delay=1200) # need to change if races are longer than 20 minutes
ll_client.connect(MQTT_SERVER)

ll_client.subscribe(MQTT_GPS_SEND)  # subscribe to the 'here_i_am' messages


# set up the plotter

fig = plt.figure( figsize=(16.0,8.0),facecolor='#DEDEDE')
ax = fig.add_subplot(1,1,1)
plt.show()

#ani = animation.FuncAnimation(fig, plot_data, interval=200)

#plot_data()


# start the loop
i = 0
ll_client.loop_start()  #use this line if you want to write any more code here

while True:
    while not q.empty():
        message = q.get()
        print("message:" + message )
        msg = json.loads(message)

        df = df.append({"geometry":Point(msg["long"],msg["lat"]), "time": msg["time"], "course_t": msg["course_t"], "course_m": msg["course_m"], "speed": msg["speed"], "boat_id":msg["boat_id"], "skipper": msg["skipper"]}, ignore_index=True).tail(20)
        df.geometry.crs="EPSG:4326"
        #print("after append\n" )

    print("exiting update data; df: \n" + str(df))

    if proj is None:
        #print("proj is: " + str(proj))
        if df.shape[0] > 0:
            utm = lonlat2UTM([msg["long"],msg["lat"]])
            proj = "EPSG:" + str(utm)
            print("set proj to " + str(proj))

    else:
        #print("in else; proj is " + str(proj) + "; df.shape[0] = " + str(df.shape[0]) )
        #df = gpd.read_file("/Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code/fileotle.json", rows=slice(-50,-1))
        new_df = df
        new_df.geometry = new_df.geometry.to_crs(proj)
        print("new_df: \n" + str(new_df))
        
        #ax.clear()
        new_df.plot(column='boat_id', cmap='viridis', ax=ax, legend = True)
    
        # make plot square
        [y_min, y_max] = ax.get_ylim()
        [x_min, x_max] = ax.get_xlim()
        yd=y_max-y_min
        ym=(y_max+y_min)/2
        xd=x_max-x_min
        xm=(x_max+x_min)/2
        if(yd>xd):
           ax.set_xlim([xm-yd/2,xm+yd/2])
        else:
           ax.set_ylim([ym-xd/2,ym+xd/2])
        
        plt.title(str(i))
        i += 1
        plt.pause(0.2)


        

    
        
