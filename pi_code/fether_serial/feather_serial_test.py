#!/usr/bin/python3
# -*- coding: utf-8 -*-


import serial
import time
import signal
import json


ser=serial.Serial("/dev/ttyACM0",115200)
ser.reset_input_buffer()
    
# create the ctl-c handler
def handler(signum, frame):
    # close the file and exit
    ser.close()
    exit(0)

signal.signal(signal.SIGINT, handler)

while True:
    data = ser.readline().decode("utf-8")
    #print(data)
    try:
        dict_json = json.loads(data)
        if "lon" in dict_json.keys():
            print(dict_json["lon"])
    except json.JSONDecodeError as e:
        print(e.msg, e)
        print(repr(data))
