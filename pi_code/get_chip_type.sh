#!/bin/bash

# this should return the version of the u blox chip being used.
#
# Assumes that the gpsd daemon is running, or at least run as root "gpsd  -n -N -s 57600 /dev/ttyAMA1"
#
# This script by Scott Dynes scott@dynes.org
#
# First version 15-Apr-2022

# Query the chip
mod=`ubxtool -p MON-VER | grep MOD | head -1`
value=${mod#*=}
echo $value
