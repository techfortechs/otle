# set_up_base.py: Used to set up RPi with Sparkfun U blox Z-F9P and Adafruit RFM95W LoRa Radio Transceiver Breakout
#
# This script by Scott Dynes scott@dynes.org
#
# This script based on breadboard setup
# First version 30-Mar-2022
#
# 7 Apr 2022 Added a 0.4 sec wait between writes; removed the 'write_with_ack'
# 21 Apr 2022 updated to reflect switching of ttyAMA1 and ttyS0
# 01 Aug 2022 updated following testing of LoRa link

import rfm9x_tools   # expeceted to be in python path somewhere...
import time
from datetime import datetime as dtime
from datetime import timedelta
from serial import Serial
import serial.threaded
import syslog
from pyrtcm import RTCMReader
from threading import Thread, Event, Lock

# Do we want debugging info in the syslog?
debug_mode = True
rfm9x_tools.set_debug(debug_mode)

# base node
base_node = 7

# node mumber of the rovers. This had better be unique in your radio environment...
rover_node = 142

shared_dict = {}  # holds the received and parsed RTCM messages

# open file to write log of RTCM messages
log_file = open("/home/pi/t4t/RTCM_log_file.txt", "a")


# define functions

def read_RTCM_stream(
    shared_dict: dict,
    stream: object,
    rtr: RTCMReader,
    lock: Lock,
    stop: Event,
):

    """
    Read RTCM stream from ZED-F9P and parse into individual RTCM3 messages. 
    Put the indivudal messages into the dict with the message id as the key.
    """
    
    while not stop.is_set():
        if stream.in_waiting:
            try:
                lock.acquire()
                (raw_data, parsed_data) = rtr.read()
                lock.release()
                if parsed_data:
                    shared_dict[str(parsed_data.identity)] =  parsed_data
                    if debug_mode:
                        syslog.syslog(syslog.LOG_INFO,f"set_up_base: parseded RTCM message  {parsed_data.identity} ")
                        print(f"set_up_base: parseded RTCM message  {parsed_data.identity} ")                        
            except Exception as err:
                print(f"\n\nSomething went wrong {err}\n\n")
                syslog.syslog(syslog.LOG_INFO,"set_up_base: Error reading parsing  RTCM stream -  {err} ")
                continue



def send_RTCM_msg(
    shared_dict: object,
    fpointer: object,
    lock: Lock,
    stop: Event,
    rfm9x: object    
):
    debug_mode = True
    the_past = dtime.strptime('Q2020-01-01T00:00:00.000', "Q%Y-%m-%dT%H:%M:%S.%f")
    msg_time_dict = {}  # the dict to store the id and time last sent
    count1230 = 0 # msg id 1230 gets sent one/fifth as ofter as the others
    time_to_sleep = .5
    
    """
    Every second or so send one or more RCTM messages. Loop trhough all the message types in shared_dict. 
    Check the length to make sure we stay within bandwidth constraints by changing sleep time.
    """

    print(f"hello from the thread! debug_mode is {str(debug_mode)}; stop is {str(stop.is_set())}  ")
    
    while not stop.is_set():
        if debug_mode:
            syslog.syslog(syslog.LOG_INFO,f"set_up_base: in send_RTCM_msg. msg_time_dict: {str(msg_time_dict)} ")
            print(f"set_up_base thread: in send_RTCM_msg.  ")        
        # if messages are older than a minute delete them from msg_time_dict. do this ***before*** checking for new messages
        for key in msg_time_dict.keys():
            if (msg_time_dict[key] < (dtime.now() - timedelta(minutes = 1))) & (msg_time_dict[key] !=  the_past):
                # delete the dict entry, keeping entries added with the_past datetime
                msg_time_dict.pop(key)
                if debug_mode:
                    syslog.syslog(syslog.LOG_INFO,f"in send_RTCM_msg: deleted old ID.  msg_time_dict: {str(msg_time_dict)} ")
                    print(f"in send_RTCM_msg thread: deleted old ID. ")
            # see if all the messages in the shared dict are in msg_time_dict. The message IDs are the keys
        for key in shared_dict.keys():
            if key not in  msg_time_dict.keys():
                # put in the key at some time in the past
                msg_time_dict[key] = the_past
                if debug_mode:
                    syslog.syslog(syslog.LOG_INFO,f"in send_RTCM_msg: added new ID.  msg_time_dict: {str(msg_time_dict)} ")
                    print(f"in send_RTCM_msg thread: added new ID.   ")                
        # choose the oldest message, and send that. Note that msg ID 1230 should get sent a fifth as often than the others.
        # find the key for the oldest message

        if not msg_time_dict:
            print(" thread: not msg_time_dict is True, continuing")
            print(str(msg_time_dict))
            time.sleep(.5)
            continue  # dict msg_time_dict is empty
        
        oldest_key = str(list({k: v for k, v in sorted(msg_time_dict.items(), key=lambda item: item[1])})[0])
        # get the length of the payload and write it to a file
        num_bytes = len(shared_dict[oldest_key].payload)+6
        if (num_bytes > 230):
            syslog.syslog(syslog.LOG_INFO,f"*****  in send_RTCM_msg: RTCM msg is longer than 230 for key {oldest_key}. ")
            fpointer.write(f"*****  in send_RTCM_msg: RTCM msg is longer than 230 for key {oldest_key}. ")            
           
        nowtime_str = dtime.now().strftime("%Y-%m-%d %H:%M:%S")
        fpointer.write(f"    {str(oldest_key)}   {str(num_bytes)}   {nowtime_str}  \n ")
        if debug_mode:
            syslog.syslog(syslog.LOG_INFO,f"in send_RTCM_msg: oldest_key is {oldest_key}.  msg_time_dict: {str(msg_time_dict)} ")
            print(f"in send_RTCM_msg thread: oldest_key is {oldest_key}.  ")            
#        if ((oldest_key == '1230') & (count1230 < 5)):
#            count1230 += 1
#            oldest_key = list({k: v for k, v in sorted(msg_time_dict.items(), key=lambda item: item[1])})[1]
#            if debug_mode:
#                syslog.syslog(syslog.LOG_INFO,f"in send_RTCM_msg: 1230 key. count1230: {count1230}  oldest_key: {oldest_key} msg_time_dict: {str(msg_time_dict)} ")
#                print(f"in send_RTCM_msg: 1230 key. count1230: {count1230}  oldest_key: {oldest_key} msg_time_dict: {str(msg_time_dict)} ")                
#            continue
# This was to try to decrease the frequency of sending 1230 msgs; it didn't work b/c of the continue. Don't think this is a problem as of now

        # send the RTCM message. The raw_data should serialize into a complete RTCM 3.2 compliant message
        tmp = shared_dict[oldest_key].serialize()
        if debug_mode:
            print(f" to be sent: {tmp.hex()} which is of type {type(tmp)} ")
        try:
            rfm9x.send(tmp, node = base_node, destination = rover_node)
        except Exception as e:
            print (f"set_up_base: error sending RTCM msg via rfm9x; {e} ")

# the line below will send a kulti-page message - the routine is not debugged, and only the send side is written        
#        rfm9x_tools.send_bytes(rfm9x, shared_dict[oldest_key].serialize(), base_node, rover_node, debug_mode )
        # update the time
        msg_time_dict[oldest_key] = dtime.now()

        # say we can send 625 bytes/sec. Calculate the sleep time based on the size    
        time_to_sleep = int(num_bytes)/625

        time.sleep(time_to_sleep*1.3)  # a little buffer time

        if debug_mode:
            syslog.syslog(syslog.LOG_INFO,f"in send_RTCM_msg: sent {oldest_key}. sleeping for {time_to_sleep}  msg_time_dict: {str(msg_time_dict)} ")
            print(f"in send_RTCM_msg thread: sent {oldest_key}. sleeping for {time_to_sleep}  msg_time_dict: {str(msg_time_dict)} ")            



######

######

# Set up radio
try:
    rfm9x, orig_node = rfm9x_tools.set_up(base_node)  # the base node number can be anything; the rover nodes need to all be the same.
except Exception as err:
    print(f"\n\nSomething went wrong {err}\n\n")
    syslog.syslog(syslog.LOG_INFO,'set_up_rover: somethign went wrong setting up radio - ' + str(err))

rfm9x.destination = rover_node # the default destination, will be used in with_ack


# Start the threads for reading the serial port and sending RTCM data to the rovers

with Serial(port='/dev/ttyAMA1',baudrate = 57600,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS, timeout=3) as serial_stream:

    rtcmreader = RTCMReader(serial_stream)
    serial_lock = Lock()
    stop_event = Event()

    write_thread = Thread(
        target=send_RTCM_msg,
        args=(
            shared_dict,
            log_file,
            serial_lock,
            stop_event,
            rfm9x,
        ),
    )

    # starting write_thread
    write_thread.start()
    if debug_mode:
        print("set_up_base: just after starting write_thread")
    while not stop_event.is_set():
        try:
            if serial_stream.in_waiting:
                try:
                    serial_lock.acquire()
                    (raw_data, parsed_data) = rtcmreader.read()
                    serial_lock.release()
                    if parsed_data:
                        log_file.write(f" {parsed_data.identity}   {len(raw_data)}  {dtime.now().strftime('%Y-%m-%d %H:%M:%S')} \n ") 
                        if debug_mode:
                            syslog.syslog(syslog.LOG_INFO,f"set_up_base: parseded RTCM message  {parsed_data.identity} ")
                            print(f"set_up_base: parseded RTCM message  {parsed_data.identity} ")                                                            
                        shared_dict[str(parsed_data.identity)] =  parsed_data
                except Exception as err:
                    print(f"\n\nSomething went wrong {err}\n\n")
                    syslog.syslog(syslog.LOG_INFO,"set_up_base: Error reading parsing  RTCM stream -  {err} ")
                    continue
                                    
        except KeyboardInterrupt:  # capture Ctrl-C
            print("\n\nTerminated by user.")
            log_file.close()
            stop_event.set()

    print("\nStop signal set. Waiting for threads to complete...")
    write_thread.join()
    print("\nProcessing complete")        

    
    
        


