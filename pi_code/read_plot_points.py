#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to accept data from the mqtt broker, decode the byte string,
# write the data via django to postgres, and maybe plot the positions of the devices.
#
#
# This version written 29 Aug 2022 by Scott Dynes scott@dynes.org
# 
# 23-Sept-2022 by Scott Dynes scott@dynes.org
# For brass rat defining boats and marks based on hw id  
# hw_id                boat/mark     id_num
# 100000005a8ff519      pin          128+2
# 10000000f7a7022e      base           128+0
# 100000009c7a8867      boat_12       12
# 1000000098221281      RC            128+1
# also implemented a dict to store most recent coords and directions for plotting
#
# 29-Sept updating to have the points be colored by their 2d-error
#
# 30-Sept putting in timing and OTLE detection
# Assume 3-minute start, and no over the line within one minute
#   How will the countdown start? Button press on web site sends MQTT message
#   which gets picked up here
#
#   Maybe hard thing? - if over line early then boat needs to round the rc or pin
#   to clear and then start
#

import numpy as np
#import pandas as pd
import geopandas as gpd
#import contextily as cx
import time
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#from shapely.geometry import Point, LineString
#from shapely.geometry import asPoint
from numpy import asarray
import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import os
#import datetime 
import math
import json
import signal
import pdb
from pathlib import Path
from argparse import ArgumentParser

from django.utils.dateparse import parse_datetime
from django.contrib.gis.geos import Point, LineString
from django.conf import settings
from django.utils import timezone

race_time = []  # to hold race time, from -180 thru 0 to whenever seconds
boat_status = {} # dict to hold OTLE/clearing status
cur_data = {} # dir to hold the most recent boat_id: [long, lat, easting, northing, course_t, time] data
cbar = None  # so we can test for its existence later
race_created = 0  # so we know when to start putting data into django

# race timing flags and counters
attn_sound = 0 # attention sound before three-minute start
three_min_sound = 0
one_min_sound = 0
start_time = 0.0  # to hold the time when the count-down is started, e.g. when this script is run
initial_race_time = -195.0   # counts seconds down from 3min + 15 secs.
race_started = False
otle_status = {}  # dict with boat_id, behind_line, between_pins, otl@1min, otl@start, cleared_status, started
# behind_line is True if boat is behind the line and extension, False if in front of the line and extension
# between_pins is True if boat is between the rc and the pin, False if outside pins
# otl flags are False if not over at the time, True if over at the time
# cleared_status is 1 if everything is fine, -1 if boat needs to clear
# started is False if not started, True if started


file_dir =os.path.dirname(sys.argv[0])


# device offset in meters from fiducial point. fixed for here, but should be part of boat setup?
# will differ for different classes of boats
dev_offset = np.array([-0.05, .2])  #x, y in meters with boat axis along y axis
# load boat outline
# here, the Tech
file = open("../boat_outlines/tech/tech_coords_final.csv")
boat_outline = np.loadtxt(file, delimiter=",")
file.close()


sys.path.insert(0, '.')
sys.path.insert(0, '..')

try:
    import manage # pylint: disable=unused-import
except ImportError as err:
    sys.stderr.write("Could not run script! Is manage.py not in the current"\
        "working directory, or is the environment not configured?:\n"\
        f"{err}\n")
    sys.exit(1)


# name based on the date
date_name = time.strftime("%Y_%j_%H%M%S", time.localtime())
from races.models import Race, Boat, RaceTick





gpd.options.display_precision = 9

# set the datetime epoch as not starting at the typical unix epoch but the otle epoch:
epoch = datetime.strptime('Q2020-01-01T00:00:00.000Z', "Q%Y-%m-%dT%H:%M:%S.%f%z")

here_i_am_q = Queue()

MQTT_SERVER = '192.168.50.14' #  MIT t4t broker or Scott's laptop via tomato on Linksys
#MQTT_SERVER = "192.168.83.171"  # MQTT broker for a test on scott's hotspot network
MQTT_GPS_SEND = "here_i_am" # this is the name of topic, like temp
MQTT_OTLE_REC = "over_early_q"


# create file name to store mqtt messages. Good for testing?
# check to see if dir exists; create if it does not

tmp_path = os.path.join(file_dir, "mqtt_data")
if not os.path.exists(tmp_path):
    os.mkdir(tmp_path)
fname = tmp_path + "/server_" + date_name
# ... and open it in append mode
mqtt_file =  open(fname, "wb")

def get_race_time():
    # the start_time variable should have been set when this script was first run
    # the initial_race_time is where the time starts
    # returns the current race time as a float
    return (initial_race_time + (datetime.now() - start_time).total_seconds())
    
    
def lonlat2UTM(lonlat):  # lonlat is [long, lat]
    utm = (np.floor((lonlat[0] + 180) / 6) % 60) + 1
    if(lonlat[1] > 0): 
        utm = utm + 32600
    else:
        utm = utm + 32700
    return int(utm)

def rotation(r):
    return np.asarray([np.cos(r), -np.sin(r), np.sin(r), np.cos(r)]).reshape(2, 2)

def on_message(mosq, obj, msg):
    # This callback will be called for messages that we receive that do not
    # match any patterns defined in topic specific callbacks, i.e. in this case
    # those messages that do not have topics $SYS/broker/messages/# nor
    # $SYS/broker/bytes/#
    print("m", end = "")
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

def on_here_i_am(mosq, obj, msg):
    #print("g", end = "")
    # this call back will handle here_i_am msgs
#    if not here_i_am_q.empty():
#         message = here_i_am_q.get()

    utm = None # will hold the computed EPSG
    
    message = msg
    pl = message.payload  # no decoding; it's a bytestring
    # check to see we have a valid payload
    if (pl[0:4] == b'otle'):
        mqtt_msg_index = int.from_bytes(pl[4:6], byteorder='big', signed = True)
        llat = int.from_bytes(pl[6:11], byteorder='big', signed = True)* 1e-9
        llong = int.from_bytes(pl[11:16], byteorder='big', signed = True)* 1e-9
        err_2d = int.from_bytes(pl[16:17], byteorder='big', signed = False)/200
        meas_time = epoch + timedelta(seconds = (int.from_bytes(pl[17:22], byteorder='big', signed = False)/10))
        course_t = int.from_bytes(pl[22:23], byteorder='big', signed = False)*360/256
        boat_id = int.from_bytes(pl[23:24], byteorder='big', signed = False)

        
        #####
        # hack for brass_rat
        if (boat_id == 0):
            course_t = (-62 + 180)
            
        #####
        
 
        #print(str(boat_id) + ".",  end = "")
        
        # use django gis to transform geometry, and put latest data into cur_data dict
        if (utm is None):
            utm = lonlat2UTM([llong, llat])

        tmp_point = Point(llong, llat, srid=4326)
        trans_point = tmp_point.transform(utm, clone=True)
        
        # the dictionary to hold the most recent boat_id: [long, lat, easting, northing, course_t, err_2d,  time] data
        cur_data[str(boat_id)]=[llong, llat, trans_point[0], trans_point[1], course_t, err_2d, meas_time]
        #print(cur_data)

        # if the race has been started, put the data into postgres using django
        if (race_created == 1):
            #print("in custom handler; race_created = 1")
            boat, _ = race.boats.get_or_create(alt_id=str(boat_id), defaults={
                'name': "boat_%s" % str(boat_id),
            })  # from old_import
            
            boat.ticks.get_or_create(race=race, ctime=meas_time, defaults={
                'coord':Point(llong,llat),
                'course_t': course_t,
                'error_2d': err_2d
            })

        # write the binary string out
        mqtt_file.write(pl)
        
#        if (int(mqtt_msg_index)%600 == 0):
#            tmp_str = "boat_id and msg index: " + str(boat_id) +" " + str(mqtt_msg_index)
#            print(tmp_str)
#            print("llat: " + str(llat))
#            print("llong: " + str(llong))
#            print("err_2d: " + str(err_2d))
#            print("meas_time: " + str(meas_time))
#            print("course_t: " + str(course_t))
#            print("boat_id: " + str(boat_id))
        
    #else:
        #print("here_i_am: unexpected payload: " + str(pl))
        #syslog.syslog(syslog.LOG_INFO,"here_i_am: unexpected payload: " + str(pl))

             
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe("here_i_am")
    

# create the ctl-c handler
def handler(signum, frame):
    # close the connection and exit 
    ll_client.loop_stop()
    ll_client.disconnect()
    exit(0)

signal.signal(signal.SIGINT, handler)



# Set up the broker
client_name = 'otle_processor'

ll_client = mqtt.Client(client_name)
ll_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ll_client.on_message = on_message
ll_client.message_callback_add("here_i_am", on_here_i_am)
#ll_client.on_publish = on_publish        #attach function to callback
ll_client.reconnect_delay_set(min_delay=1, max_delay=1200) # need to change if races are longer than 20 minutes
ll_client.connect(MQTT_SERVER, keepalive=60)

ll_client.subscribe("#",0)  # subscribe to everything

ll_client.loop_start()  #use this line if you want to write any more code here



# set up the plot
fig, ax = plt.subplots(1, figsize = (8,8))
#fig, axs = plt.subplot_mosaic([['boats',    'time'], ['boats',   'words']], width_ratios= [3,1],height_ratios= [1,5],constrained_layout=False)

# create the norm object for the colormap
norm = plt.Normalize(255/200, 0/200) # the complete range of linear values

# Create the colorbar
smap = plt.cm.ScalarMappable(cmap='viridis', norm=norm)
cbar = fig.colorbar(smap, ax=ax, fraction=0.1, shrink = 0.8)
cbar.ax.tick_params(labelsize=11)
cbar.ax.set_ylabel('T', rotation=0, labelpad = 15, fontdict = {"size":14})

while True:

    # handle sounds
    # TBD
    
    # make arrays
    #rc = np.array(cur_data['129'])
    #pi = np.array(cur_data['130'])
    # set rc and pin so normal start will be to positive y/north (rc boat on right end of line)
    if ('129' in cur_data.keys() and '130' in cur_data.keys()):
        line = np.array([cur_data['129'][2:4],cur_data['130'][2:4]])
        l_tmp = np.array(cur_data['129'][2:4]) - np.array(cur_data['130'][2:4])
    else:
        print("129: " + str('129' in cur_data.keys()) + "   130: " + str('130' in cur_data.keys()))


    # clear the axes
    ax.clear()


    # plot the boats
    for count, i in enumerate(cur_data.keys()):
        if (int(i) == 129):  
            ax.scatter(cur_data[i][2],cur_data[i][3], s=[128], c =['g'], marker = '$RC$')
        elif (int(i) == 130):   
            ax.scatter(cur_data[i][2],cur_data[i][3], s=[128], c =['r'], marker = '$pin$')
        else:
            ax.plot(cur_data[i][2], cur_data[i][3], 'bo')
            
        

    ax.set_aspect('equal')
    plt.pause(1)
            
    #    breakpoint()

