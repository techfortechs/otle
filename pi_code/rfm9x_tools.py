# SPDX-FileCopyrightText: 2018 Brent Rubell for Adafruit Industries                            
#                                                                                              
# SPDX-License-Identifier: MIT                                                                 
#
# This script customized by Scott Dynes scott@dynes.org
# First version 30-Mar-2022
#
#  To set up libraries, sudo pip3 install adafruit-circuitpython-rfm9x
#
# 3-Apr-2022 changedsetting speed of spi connection to 1 megabit/sec
# 4-Apr-2022 changed rfm9x params to increase bandwidth (sig_bw, spread, code_rate)
#


"""                                                                                            
Example for using the RFM9x Radio with Raspberry Pi.                                           
                                                                                               
Learn Guide: https://learn.adafruit.com/lora-and-lorawan-for-raspberry-pi                      
Author: Brent Rubell for Adafruit Industries                                                   
"""
# Import Python System Libraries                                                               
import time
# Import Blinka Libraries                                                                      
import busio
from digitalio import DigitalInOut, Direction, Pull
import board
import math
import random
import syslog

# Import RFM9x                                                                                 
import adafruit_rfm9x

# create the debug_mode variable
debug_mode = False

def set_debug(inbound_debug_mode:bool):
    global debug_mode
    debug_mode = inbound_debug_mode
    print(f"rfm9x_tools: debug_mode: {debug_mode}")


def set_up(node):
    # Configure LoRa Radio
    #
    # use:  rfm9x, node = rfm9x_tools.set_up(<node number>)
    CS = DigitalInOut(board.CE1)
    RESET = DigitalInOut(board.D25)
    spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
    rfm9x = adafruit_rfm9x.RFM9x(spi, CS, RESET, 915.0, baudrate=1000000)
    rfm9x.signal_bandwidth = 250000
    rfm9x.coding_rate = 5
    rfm9x.spreading_factor = 7
    # these should yield 5.47 kbps
    rfm9x.enable_crc = True
    rfm9x.tx_power = 13
    rfm9x.high_power = True
    rfm9x.node = node
    prev_packet = None

    node_num = node

    return rfm9x, node
    

def listen(rfm9x):
    while True:
        packet = None
        
        # check for packet rx                                                                      
        packet = rfm9x.receive(timeout=5.0)
        if packet is not None:
            packet_data = bytearray(packet)



def raw_listen(rfm9x):
    while True:
        packet = None
        
        packet = rfm9x.receive(timeout=5.0)
        if packet is not None:
            packet_data = bytearray(packet)



def send(rfm9x, msg = "Hello, world!" , orig_node = None, dest_node=None):
    data = bytes(msg,"utf-8")
    rfm9x.send(data, node = orig_node, destination = dest_node)
    #print("Sent: " + msg + " to node " +str(dest_node))


def raw_send(rfm9x, msg = "Hello, world!" , orig_node = None, dest_node=None):
    rfm9x.send(msg, node = orig_node, destination = dest_node)
    #print("Sent: " + str(len(msg)) + "bytes to node " + str(dest_node))


def send_bytes(rfm9x, msg, orig_node = None, dest_node=None, debug_mode = False):
    # this is the smarter version of send - will break messages into unit to send over multiple packets if needed

    # With the SW we are using (https://docs.circuitpython.org/projects/rfm9x/en/latest/api.html#)
    # lora packet payload can be 252 bytes. We will add a header of three bytes of ID ( 'MIT' ), 4 bits of number
    # of pages in this message, 4 bits of the page number of the current packet, 1 byte of message id,
    # and then up to 240 bytes of payload. This is a total of 246 bytes of radiohead payload.
    #
    # On the other end we will decode, and see if we have the complete page set, reassemble the message, and
    # push it out to the Z-F9P

    # Check the size of the message
    msg_size = len(msg)

    # calculate the number of pages
    num_pages = math.ceil(msg_size/240)

    # get a random number for the msg id:
    msg_id = random.randbytes(1)

    if debug_mode:
        syslog.syslog(syslog.LOG_INFO,f"rfm9x_tools send_bytes: msg id  = {msg_id}, num_pages = {num_pages}, len msg = {msg_size} ")
        print(f"rfm9x_tools send_bytes: msg id  = {msg_id}, num_pages = {num_pages}, len msg = {msg_size} ")        
    
    for i in range(num_pages):
        # create the pages/page byte
        page_byte = bytes.fromhex(hex(num_pages)[2:] +hex(i+1)[2:])
        # get lh nibble with lhn = ord(c) >> 4 & 0x0F, and rh nibble with rhn = ord(c) & 0x0F

        # get the msg chunk
        chunk_start = i * 240
        chunk_end = (i+1) * 240
        if chunk_end > msg_size: chunk_end = msg_size
        msg_chunk = msg[chunk_start:chunk_end]
        
        # create bytearray
        packet = bytearray('otle', encoding='ascii')
        packet.extend(page_byte)
        packet.extend(msg_id)
        packet.extend(msg_chunk)

        if debug_mode:
            syslog.syslog(syslog.LOG_INFO,f"rfm9x_tools: msg id  = {msg_id}, i = {i}, len chunk = {len(msg_chunk)}, len packet is {len(packet)} ")
            print(f"rfm9x_tools: msg id  = {msg_id}, i = {i}, len chunk = {len(msg_chunk)}, len packet is {len(packet)} ")            
            
        raw_send(rfm9x, msg, orig_node, dest_node)

    if debug_mode:
        syslog.syslog(syslog.LOG_INFO,f"rfm9x_tools send_bytes: done sending msg_id {msg_id} ")
        print(f"rfm9x_tools send_bytes: done sending msg_id {msg_id} ")        
