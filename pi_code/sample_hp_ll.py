#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# use: gpspipe -w | sample_hp_ll
# based on code by Niccolo Rigacci


import sys
import time
import os
import gps
import datetime
import json
import math




while True:
     
    indata = os.popen('gpspipe -w --count 5').read().splitlines()

    EXIT_CODE = 0
    TPV = None
    
    try:
         while True:
            SENTENCE = json.loads(indata.pop())
            if 'class' in SENTENCE and SENTENCE['class'] == 'TPV':
                TPV = SENTENCE
            if TPV is not None:
                sys.stdin.close()
                break
        
        
    except (IOError, ValueError):
        # Assume empty data and write msg to stderr.
        EXIT_CODE = 100
        sys.stderr.write("Error reading JSON data from file or stdin."
                         " Creating an empty or partial skyview image.\n")
    
    
    lat = SENTENCE['lat']
    long =  SENTENCE['lon']
    chiptime = SENTENCE['time']
    course_t = SENTENCE['track']
    course_m = SENTENCE['magtrack']
    speed = SENTENCE['speed']
    
    
    boat_dict = {
        "lat": lat,
        "long": long,
        "time": chiptime,
        "course_t": course_t,
        "course_m": course_m,
        "speed": speed,
        "boat_id": 2,
        "skipper": "Alan Hale Jr",
        }
    
    
    print("sample prints: " + str(boat_dict))

    time.sleep(0.1)

    
