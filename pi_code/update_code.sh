#!/bin/bash
#
# Script to rsync code from otle repo to this device, update
# services if needed, and then reboot.
#
# Takes the ip address and directory of the repo as arguments
#
# update_code.sh <ip_addr of repo> <dir of repo>
#

# get args
ip_addr=$1
repo_dir=$2

echo "ip_addr: $ip_addr"
echo "repo_dir: $repo_dir"

# make sue we are in the right dir
cd /home/pi/t4t

# make a backup of the current version
cp current_version.txt current_version_last.txt

# start  ssh-agent and add key
eval $(ssh-agent)
ssh-add ~/.ssh/otle_rsa

# sync the clocks
#sudo date --set="$(ssh otle@192.168.50.14 'date -u')"
# As long as the device can reach the internet the time should be set

# rsync the current_version txt file
rsync -ptvv --delay-updates --modify-window=2 otle@$ip_addr:$repo_dir/current_version.txt /home/pi/t4t

# see if we have the current version. If so, we are done.
if [ `cat current_version.txt`  ==  `cat current_version_last.txt`  ] ;
then
    echo "We have the current version of code"
    reboot=0
    exit 0
else
    echo "Updating code due to newer version"
    reboot=1
fi


# rsync the files into the tmp dir. make sure rsync exits successfully
return_val=0

echo "Return_val: $return_val"
rsync  -ptvv --delay-updates --modify-window=2 otle@$ip_addr:$repo_dir/*.sh /home/pi/t4t
return_val=$((return_val + $?))
echo "Return_val: $return_val"

rsync -ptvv --delay-updates --modify-window=2 otle@$ip_addr:$repo_dir/*.py /home/pi/t4t
return_val=$((return_val + $?))
echo "Return_val: $return_val"

rsync  -ptvv --delay-updates --modify-window=2 otle@$ip_addr:$repo_dir/lib_systemd* /home/pi/t4t
return_val=$((return_val + $?))
echo "Return_val: $return_val"

if [ $return_val -ne "0" ];
then
    # rsync failed; return error
    echo "One of the rsyncs failed...  exiting"
    exit 1
fi


# see if there are new lib_systemd files. If so, move into place and reload
for i in `ls  lib_systemd_system* | grep -v "~$"`
do
    if [  "$i" -nt "./tmp_for_time" ]
    then
	# this file was just downloaded
	# get name of service
        sname=`echo $i | cut -c 20- `	
	# copy to right dir
	sudo cp ./$i /lib/systemd/system/$sname
	# reload daemon
	sudo systemctl daemon-reload
        echo "replaced and reloaded  $sname. Did not restart. "
	reboot=1
    fi

done

# get a timestamp for the next invocation (to see if things have changed since now)
sudo chmod 666 tmp_for_time
touch tmp_for_time

# finally, if changed anything reboot
if [ $reboot  =  "1"  ] ;
then
    echo "Rebooting!"
    sudo reboot
fi




