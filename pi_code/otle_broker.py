#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
# Script to act as the OTLE mqtt broker
#
# Should be transport agnostic
#
# First version written 16 May 2022 by Scott Dynes scott@dynes.org
#
# 23 June 2022 updated to use password authentication
#
# 25 Aug 2022 depricated; file writing is nto done for prototype plotting.
# Succceeded by read_plot.py


import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import time
import os
import datetime
import math
import numpy as np
import pandas as pd
import geopandas as gpd
import contextily as cx
import datetime
import matplotlib.pyplot as plt
import json
import pickle
from shapely.geometry import Point, LineString
from shapely.geometry import asPoint
from numpy import asarray

gpd.options.display_precision = 9

q = Queue()

# create the empty geopandas dataframe
headers = ['geometry','time', 'course_t', 'course_m', 'speed', 'boat_id', 'skipper']
df = gpd.GeoDataFrame(columns=headers, crs="EPSG:4326")


def lonlat2UTM(lonlat):  # lonlat is [long, lat]
    utm = (np.floor((lonlat[0] + 180) / 6) % 60) + 1
    if(lonlat[1] > 0): 
        utm = utm + 32600
    else:
        utm = utm + 32700
    return int(utm)

def rotation(r):
    return np.asarray([np.cos(r), -np.sin(r), np.sin(r), np.cos(r)]).reshape(2, 2)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe(MQTT_GPS_SEND )
    

# The callback for when a PUBLISH message is received from the server.
# All the data munging and recording work is done here.
def on_message(client, userdata, msg):
    # put the data into the gpd dataframe. Convert the long/lat to a gpd Point
    q.put(str(msg.payload.decode("utf-8","ignore")))



# Set up the broker

MQTT_SERVER = '192.168.50.14' #  MIT t4t broker or Scott's laptop via tomato on Linksys
MQTT_GPS_SEND = "here_i_am" # this is the name of topic, like temp
MQTT_OTLE_REC = "over_early_q"

client_name = 'otle_processor'

ll_client = mqtt.Client(client_name)
ll_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ll_client.on_message = on_message
#ll_client.on_publish = on_publish        #attach function to callback
ll_client.reconnect_delay_set(min_delay=1, max_delay=1200) # need to change if races are longer than 20 minutes
ll_client.connect(MQTT_SERVER)

ll_client.subscribe(MQTT_GPS_SEND)  # subscribe to the 'here_i_am' messages

ll_client.loop_start()  #use this line if you want to write any more code here

while True:
    while not q.empty():
        message = q.get()
        #print(message)
        msg = json.loads(message)

        df = df.append({"geometry":Point(msg["long"],msg["lat"]), "time": msg["time"], "course_t": msg["course_t"], "course_m": msg["course_m"], "speed": msg["speed"], "boat_id":msg["boat_id"], "skipper": msg["skipper"]}, ignore_index=True)
        df.geometry.crs="EPSG:4326"
    
        df.to_file("fileotle.json", driver="GeoJSON")


     
