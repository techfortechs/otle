# pyubx2_tools.py: tools and scripts that use the pyubx2 packages
#
# This script by Scott Dynes scott@dynes.org
#
# First version 8-Apr-2023
#
# This assumes that the serial ports are set up
#
#
# This also assumes that each chip has been set up to produce/read the appropriate RTCM3 things... (This should be part of this script)


import time
import serial
import os
import sys
import syslog
from serial import Serial
from pyubx2 import (
    UBXMessage,
    UBXReader,
    UBX_MSGIDS,
    SET,
    POLL,
    UBX_PROTOCOL,
    POLL_LAYER_RAM,
    SET_LAYER_RAM,
)
from threading import Thread, Lock

accLimit = 0  # accuracy limit

SER_PORT = '/dev/ttyS0'
SER_PORT_SPEED = '57600'

# create the serial_lock variable

serial_lock = None

# create the debug_mode variable
debug_mode = False

def set_serial_lock(lock):
    global serial_lock
    serial_lock = lock

    
def set_debug(inbound_debug_mode:bool):
    global debug_mode
    debug_mode = inbound_debug_mode
    print(f"pyubx2_tools: debug_mode: {debug_mode}")

    
def set_up_stream(serial=SER_PORT, speed=SER_PORT_SPEED):
    stream = Serial('/dev/ttyS0', 57600)
    return(stream)


def get_accLimit():
    return(accLimit)


# For UBX messaging
def send_message(message, stream = None):
    our_stream = False
    if stream == None:
        our_stream = True
        serial_lock.acquire()
        stream = set_up_stream()
    stream.write(message.serialize())
    if our_stream == True:
        stream.close()
        serial_lock.release()


def stream_output():
    try:
        serial_lock.acquire()
        stream = set_up_stream()
        ubr = UBXReader(stream)
        while True:
            (raw_data, pd) = ubr.read()
            print("pd: " + str(pd))

    except KeyboardInterrupt:
        serial_lock.release()
        stream.close()
        print("Terminated by user")

    except Exception as e:
       # By this way we can know about the type of error occuring
        serial_lock.release()
        stream.close()
        print("pt.stream_output UART1 error: ",e)

        
def stream_output_UART2():
    try:
        stream = Serial('/dev/ttyAMA1', 57600)
        ubr = UBXReader(stream)
        while True:
            (raw_data, pd) = ubr.read()
            print("pd: " + str(pd))

    except KeyboardInterrupt:
        stream.close()
        print("Terminated by user")

    except Exception as e:
       # By this way we can know about the type of error occuring
        stream.close()
        print("pt.stream_output_UART2 error: ",e)

def set_ubx_rtcm_only():
    # sets UART1 to only accept and emit ubx messages, and same for UART2 and RTCM3
    transaction = 0
    layers = SET_LAYER_RAM  # *** NB: SET and DEL messages use different memory layer values to POLL ***
    cfgData = [("CFG_UART1INPROT_UBX", 1), ("CFG_UART1INPROT_NMEA", 0), ("CFG_UART1INPROT_RTCM3X", 0)]
    msg = UBXMessage.config_set(layers, transaction, cfgData)
    send_message(msg)
    cfgData = [("CFG_UART1OUTPROT_UBX", 1), ("CFG_UART1OUTPROT_NMEA", 0), ("CFG_UART1OUTPROT_RTCM3X", 0)]
    msg = UBXMessage.config_set(layers, transaction, cfgData)
    send_message(msg)
    cfgData = [("CFG_UART2INPROT_UBX", 0), ("CFG_UART2INPROT_NMEA", 0), ("CFG_UART2INPROT_RTCM3X", 1)]
    msg = UBXMessage.config_set(layers, transaction, cfgData)
    send_message(msg)
    cfgData = [("CFG_UART2OUTPROT_UBX", 0), ("CFG_UART2OUTPROT_NMEA", 0), ("CFG_UART2OUTPROT_RTCM3X", 1)]
    msg = UBXMessage.config_set(layers, transaction, cfgData)
    send_message(msg)

    

def set_ubx_rtk_fix():
    # sets u-blox chip CFG-NAVHPG-DGNSSMODE to 3 - Ambiguities are fixed whenever possible
    transaction = 0
    layers = SET_LAYER_RAM  # *** NB: SET and DEL messages use different memory layer values to POLL ***
    cfgData = [("CFG_NAVHPG_DGNSSMODE", 3)]
    msg = UBXMessage.config_set(layers, transaction, cfgData)
    send_message(msg)



    
def get_chip_info():
    msg = UBXMessage("MON", "MON-VER", POLL)
    i = 0
    stop = False
    try:
        serial_lock.acquire()
        stream = set_up_stream()
        ubr = UBXReader(stream)
        send_message(msg, stream)
        while not stop:
            if stream.in_waiting:
                (raw_data, pd) = ubr.read()
    
                if isinstance(pd, str):
                    syslog.syslog(syslog.LOG_ERR, f"get_chip_info: got string from chip: {pd}")
                    i = i + 1
                    time.sleep(.5)
                    continue    # if we dont't get a ubx object from the read, keep reading
                
    #            print("pd identity: " + pd.identity)
        
                if pd.identity == "MON-VER":
                    ChipProtocol =  pd.extension_03[8:].decode('utf8').rstrip('\x00')
                    ChipType =  pd.extension_04[4:].decode('utf8').rstrip('\x00')
    #                print("Printing MON-VER: chip type: " + str(chip_type) + "  protocol version  " + str(chip_prot) + "   " )
    #                print("___" + chip_type + "___" + " == 'ZED-F9P'? " + str(chip_type == 'ZED-F9P'))
        
                    stream.close()
                    serial_lock.release()
                    stop = True
                    break
        
                if i > 10:
                    stream.close()
                    serial_lock.release()
                    raise ValueError("Failed after ten attempts to get MON-VER data from chip")
                    syslog.syslog(syslog.LOG_ERR, "get_chip_info: Failed after ten attempts to get MON-VER data from chip")
                    stop = True
                    break
    except Exception as e:
        # By this way we can know about the type of error occuring
        stream.close()
        serial_lock.release()
        print("pt.get_chip_info error: ",e)       
    
    return ChipType, ChipProtocol


def start_survey_in():

    global accLimit
    
    # send message to start survey-in with dummy values
    layers = 1
    transaction = 0
    cfgData = [("CFG_TMODE_MODE", 1), ("CFG_TMODE_SVIN_ACC_LIMIT", 5000), ("CFG_TMODE_SVIN_MIN_DUR",300)]
    msg = UBXMessage.config_set(layers, transaction, cfgData)
    send_message(msg)

    # sleep 5
    time.sleep(5)
    
    # send message to end survey-in 
    layers = 1
    transaction = 0
    cfgData = [("CFG_TMODE_MODE", 0)]
    msg = UBXMessage.config_set(layers, transaction, cfgData)
    send_message(msg)

    # sleep 5
    time.sleep(5)

    # send message to start survey-in with actual values
    layers = 1
    transaction = 0
    if debug_mode:
        acc_limit = 500000
    else:
        acc_limit = 100000
    syslog.syslog(syslog.LOG_INFO,'pyubx2_tools: acc_limit = ' + str(acc_limit))
    if debug_mode:
        print('pyubx2_tools: acc_limit = ' +str(acc_limit))
        
    cfgData = [("CFG_TMODE_MODE", 1), ("CFG_TMODE_SVIN_ACC_LIMIT", acc_limit), ("CFG_TMODE_SVIN_MIN_DUR",300)]
    msg = UBXMessage.config_set(layers, transaction, cfgData)
    send_message(msg)


def disable_survey_in():
    # send message to end survey-in 
    layers = 1
    transaction = 0
    cfgData = [("CFG_TMODE_MODE", 0)]
    msg = UBXMessage.config_set(layers, transaction, cfgData)
    send_message(msg)

    
def get_survey_status():
    msg = UBXMessage("NAV", "NAV-SVIN", POLL)
    i = dur = meanAcc = 0
    active=valid=3

    try:
        serial_lock.acquire()
        stream = set_up_stream()
        send_message(msg,stream)
        while True:
            if stream.in_waiting:
                ubr = UBXReader(stream)
                
                (raw_data, pd) = ubr.read()
                print("pd identity: " + str(pd))

                if isinstance(pd, str):
                    syslog.syslog(syslog.LOG_ERR, f"get_survey_status: got string from chip: {pd}")
                    i = i + 1
                    time.sleep(.5)
                    continue    # if we dont't get a ubx object from the read, keep reading
                 
                if pd.identity == "NAV-SVIN":
                    active = pd.active
                    valid = pd.valid
                    dur = pd.dur
                    meanAcc = pd.meanAcc
                    if debug_mode:
                        print(f"NAV-SVIN: active is {active}, valid is {valid}, dur is {dur}, meanAcc is {meanAcc}")
                    stream.close()
                    serial_lock.release()
                    break
                
                if i > 10:
                    stream.close()
                    serial_lock.release()
                    raise ValueError("Failed after ten attempts to get NAV-SVIN data from chip")
                    syslog.syslog(syslog.LOG_ERR,"pyubx2_tools: Failed after ten attempts to get NAV-SVIN data from chip")
                    break
    except Exception as e:
        print("get_survey_status - the error is: ",e)

    return active*10+valid, dur, meanAcc


def set_up_RTCM3_msgs():
    syslog.syslog(syslog.LOG_INFO,'pyubx2_tools: Setting up RTCM3 messages on UART2')
    layers = 1  # 1 = RAM, 2 = BBR, 4 = Flash (can be OR'd)
    transaction = 0
    PORT_TYPE = "UART2"  # choose from "USB", "UART1", "UART2"
    cfg_data = []
    for rtcm_type in (
        "1005",
        "1074",
        "1084",
        "1094",
        "1124",
    ):
        cfg = f"CFG_MSGOUT_RTCM_3X_TYPE{rtcm_type}_{PORT_TYPE}"
        cfg_data.append([cfg, 1])
    ubx = UBXMessage.config_set(layers, transaction, cfg_data)
    send_message(ubx)

    # send message 1230 every 10 seconds
    cfg_data = []
    rtcm_type = "1230"
    cfg = f"CFG_MSGOUT_RTCM_3X_TYPE{rtcm_type}_{PORT_TYPE}"
    cfg_data.append([cfg, 10])
    ubx = UBXMessage.config_set(layers, transaction, cfg_data)
    send_message(ubx)



def get_rover_fix_mode():
    msg = UBXMessage("NAV", "NAV-PVT", POLL)
    i = 0
    carrSoln=7
    try:
        serial_lock.acquire()
        stream = set_up_stream()
        send_message(msg, stream)
        while True:
            if stream.in_waiting:
                ubr = UBXReader(stream)
                (raw_data, pd) = ubr.read()
    #            print("pd identity: " + str(pd))
    
                if isinstance(pd, str):
                    syslog.syslog(syslog.LOG_ERR, f"get_chip_info: got string from chip: {pd}")
                    i = i + 1
                    time.sleep(.5)
                    continue    # if we dont't get a ubx object from the read, keep reading
                
                if pd.identity == "NAV-PVT":
    #                print(f"NAV-PVT: {pd}")
                    carrSoln = pd.carrSoln
    #                print(f"NAV-SVIN: carrSoln is  {carrSoln}")
                    stream.close()
                    serial_lock.release()
                    break
    
                if i > 10:
                    stream.close()
                    serial_lock.release()
                    raise ValueError("Failed after ten attempts to get NAV-PVT data from chip")
                    break
    except Exception as e:
       # By this way we can know about the type of error occuring
        print("get_rover_fix_mode - the error is: ",e)

    return carrSoln


def set_nav_soln_rate(msec):
    transaction = 0
    layers = SET_LAYER_RAM  # *** NB: SET and DEL messages use different memory layer values to POLL ***
    cfgData = [("CFG_RATE_MEAS", msec), ("CFG_RATE_NAV", 1)]
    msg = UBXMessage.config_set(layers, transaction, cfgData)
    send_message(msg)
#    try:
#    stream = set_up_stream()
#        while True:
#            ubr = UBXReader(stream)
#            send_message(stream, msg)
#            (raw_data, pd) = ubr.read()
#            print("pd: " + str(pd)) 
#    except KeyboardInterrupt:
#    stream.close()
    

def stop_nav_messages():
    
    for (msgid, msgname) in UBX_MSGIDS.items():
        if msgid[0] == 0x01:  # NAV
            msg = UBXMessage(
                "CFG",
                "CFG-MSG",
                SET,
                msgClass=msgid[0],
                msgID=msgid[1],
                rateUART1=0,
                rateUART2=0,
                rateUSB=0,
            )
    #        print( f"\nSetting message rate for {msgname} message type to 0...\n" )
            send_message(msg)
            time.sleep(.1)

# hello this is a comment that shopuld be in git


