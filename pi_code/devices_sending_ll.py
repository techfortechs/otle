#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to print out device IDs that are being received by the mqtt broke
#
# This version written 7 Oct 2022 by Scott Dynes scott@dynes.org
# 
# 23-Sept-2022 by Scott Dynes scott@dynes.org
# For brass rat defining boats and marks based on hw id  
# hw_id                boat/mark     id_num
# 100000005a8ff519      pin          128+2
# 10000000f7a7022e      base           128+0
# 100000009c7a8867      boat_12       12
# 1000000098221281      RC            128+1
# also implemented a dict to store most recent coords and directions for plotting
#

import numpy as np
#import pandas as pd
import geopandas as gpd
#import contextily as cx
import time
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#from shapely.geometry import Point, LineString
#from shapely.geometry import asPoint
from numpy import asarray
import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import os
#import datetime 
import math
import json
import signal
import pdb
from pathlib import Path
from argparse import ArgumentParser

file_dir =os.path.dirname(sys.argv[0])



sys.path.insert(0, '.')
sys.path.insert(0, '..')





gpd.options.display_precision = 9

# set the datetime epoch as not starting at the typical unix epoch but the otle epoch:

here_i_am_q = Queue()

MQTT_SERVER = '192.168.50.14' #  MIT t4t broker or Scott's laptop via tomato on Linksys
#MQTT_SERVER = "192.168.83.171"  # MQTT broker for a test on scott's hotspot network
MQTT_GPS_SEND = "here_i_am" # this is the name of topic, like temp
MQTT_OTLE_REC = "over_early_q"


# create file name to store mqtt messages. Good for testing?
# check to see if dir exists; create if it does not



def on_message(mosq, obj, msg):
    # This callback will be called for messages that we receive that do not
    # match any patterns defined in topic specific callbacks, i.e. in this case
    # those messages that do not have topics $SYS/broker/messages/# nor
    # $SYS/broker/bytes/#
    print("m", end = "")
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))

def on_here_i_am(mosq, obj, msg):
    #print("g", end = "")
    # this call back will handle here_i_am msgs
#    if not here_i_am_q.empty():
#         message = here_i_am_q.get()

    utm = None # will hold the computed EPSG
    
    message = msg
    pl = message.payload  # no decoding; it's a bytestring
    # check to see we have a valid payload
    if (pl[0:4] == b'otle'):
        #mqtt_msg_index = int.from_bytes(pl[4:6], byteorder='big', signed = True)
        #llat = int.from_bytes(pl[6:11], byteorder='big', signed = True)* 1e-9
        #llong = int.from_bytes(pl[11:16], byteorder='big', signed = True)* 1e-9
        #err_2d = int.from_bytes(pl[16:17], byteorder='big', signed = False)/200
        #meas_time = epoch + timedelta(seconds = (int.from_bytes(pl[17:22], byteorder='big', signed = False)/10))
        #course_t = int.from_bytes(pl[22:23], byteorder='big', signed = False)*360/256
        boat_id = int.from_bytes(pl[23:24], byteorder='big', signed = False)

        print(str(boat_id) + ".",  end = "")
        
    else:
        print("here_i_am: unexpected payload: " + str(pl))
        syslog.syslog(syslog.LOG_INFO,"here_i_am: unexpected payload: " + str(pl))

             
# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe("here_i_am")
    

# create the ctl-c handler
def handler(signum, frame):
    # close the connection and exit 
    ll_client.loop_stop()
    ll_client.disconnect()
    exit(0)

signal.signal(signal.SIGINT, handler)



# Set up the broker
client_name = 'otle_processor'

ll_client = mqtt.Client(client_name)
ll_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ll_client.on_message = on_message
ll_client.message_callback_add("here_i_am", on_here_i_am)
#ll_client.on_publish = on_publish        #attach function to callback
ll_client.reconnect_delay_set(min_delay=1, max_delay=1200) # need to change if races are longer than 20 minutes
ll_client.connect(MQTT_SERVER, keepalive=60)

ll_client.subscribe("#",0)  # subscribe to everything

ll_client.loop_start()  #use this line if you want to write any more code here



while True:

    time.sleep(.5)
            
    #    breakpoint()

