#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to fread a binary data file, unpack the data, and plot packet loss vs
# distance from some refernce device 
#
# Use: packet_loss_plot.py <referece device boat ID> <filename>
#

# first version 6 Sept Scott Dynes scott@dynes.org
#

import pandas as pd
import geopandas as gpd
import argparse
import numpy as np
import datetime
from datetime import datetime as dtime
from shapely.geometry import Point, LineString
from shapely.geometry import asPoint
import matplotlib.pyplot as plt

# set the datetime epoch:
epoch = dtime.strptime('Q2020-01-01T00:00:00.000Z', "Q%Y-%m-%dT%H:%M:%S.%f%z")

# set up and use the parser
parser = argparse.ArgumentParser(description="Reads an otle data file and plots lost packets vs. distance from some reference device identified by the boat_id of that device (doesn't have to be a boat)")
parser.add_argument("fname", type=str, help="the otle data file filename with path")
parser.add_argument("dev_boat_id", type=int, help="the boat_id of the reference device")
args = parser.parse_args()

fname = args.fname
dev_boat_id = args.dev_boat_id

def lonlat2UTM(lonlat):  # lonlat is [long, lat]
    utm = (np.floor((lonlat[0] + 180) / 6) % 60) + 1
    if(lonlat[1] > 0): 
        utm = utm + 32600
    else:
        utm = utm + 32700
    return int(utm)


# open file in binary mode
try:
    f = open(fname, "rb")
except OSError:
    print('cannot open', fname)

# read in data to raw_gdf
pl = f.read()

# create geopandas dataframe for storing data
headers = ['geometry','boat_id', 'msg_id', 'time']
raw_gdf = gpd.GeoDataFrame(columns=headers, crs="EPSG:4326")


headers = ['geometry','boat_id', 'msg_id', 'time']
raw_gdf = gpd.GeoDataFrame(columns=headers, crs="EPSG:4326")

fp = 0
dsize = len(pl)
while (fp < dsize):
    if (pl[fp+0:fp+4] == b'otle'):
        mqtt_msg_index = int.from_bytes(pl[fp+4:fp+6], byteorder='big', signed = True)
        llat = int.from_bytes(pl[fp+6:fp+11], byteorder='big', signed = True)* 1e-9
        llong = int.from_bytes(pl[fp+11:fp+16], byteorder='big', signed = True)* 1e-9
        err_2d = int.from_bytes(pl[fp+16:fp+17], byteorder='big', signed = False)/200
        meas_time = epoch + datetime.timedelta(seconds = (int.from_bytes(pl[fp+17:fp+22], byteorder='big', signed = True)/10))
        course_t = int.from_bytes(pl[fp+22:fp+23], byteorder='big', signed = True)*360/256
        boat_id = int.from_bytes(pl[fp+23:fp+24], byteorder='big', signed = True)
    else:
        print("first four bytes are not otle at offset " + str(fp))
        exit(1)
    
        # put data into raw_gdf
    raw_gdf = raw_gdf.append({"geometry":Point(llong,llat), "boat_id":int(boat_id), "msg_id": int(mqtt_msg_index),
                              "time": pd.Timestamp(meas_time)},ignore_index=True)
    raw_gdf.geometry.crs="EPSG:4326"

    # increment the file pointer
    fp += 24

    # should have put the data into the gdf

# change geometry into meters
    
if (len(raw_gdf) >  0):
    utm = lonlat2UTM([raw_gdf.geometry[0].x,raw_gdf.geometry[0].y ])
    proj = "EPSG:" + str(utm)
    raw_gdf.geometry = raw_gdf.geometry.to_crs(proj)

# declare the output headers
headers = ['distance','boat_id', 'percent_dropped']

# figure out the time things
t_max = np.max(raw_gdf.time)
t_min = np.min(raw_gdf.time)
min_t = t_min
max_t = min_t + datetime.timedelta(minutes=1)
num_windows = int((t_max - t_min).total_seconds()/60)

# create a list in which to put the calculated values
# num_windows * number of boat_ids
tmp_list = np.zeros(shape=(num_windows * len(raw_gdf.boat_id.unique()), len(headers))) - 1  # so all boat_ids are -1
tmp_list_i = 0

# loop thru and take 1-minute windows, calculate the 
# distances from the reference device, and the number of dropped packets
for i in range(0, num_windows):
    tmp_d = raw_gdf[(raw_gdf.time >= min_t) & (raw_gdf.time < max_t)]
    
    # get the mean x,y for the reference boat_id
    a = tmp_d[tmp_d.boat_id == dev_boat_id]
    ref_x = np.mean(a.geometry.x)
    ref_y = np.mean(a.geometry.y)
    
    # for each boat_id, loop thru and calculate/enter the data in the tmp_list array
    for j in (tmp_d.boat_id.unique()):
        a = tmp_d[tmp_d.boat_id == dev_boat_id]
        percent_dropped = 1.0-len(a.msg_id)/(1+np.max(a.msg_id)-np.min(a.msg_id))
        time = np.mean(a.time)
        distance = np.sqrt((ref_x - np.mean(a.geometry.x))**2 + (ref_y - np.mean(a.geometry.y))**2)
        tmp_list[tmp_list_i] = [distance, j, percent_dropped]
        tmp_list_i += 1
        
    
# create the out dataframe
df = pd.DataFrame(tmp_list, columns = headers)

# and plot
ax2 = df.plot.scatter(x='distance', y='percent_dropped', c='boat_id', colormap='viridis')
plt.show()

                   



