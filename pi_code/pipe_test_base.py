#!/usr/bin/python3

import sys, os, time

path = "rtcm3_pipe"

try:
    os.mkfifo(path)
except:
    pass

try:
    fifo = open(path, "w")
except Exception as e:
    print (e)
    sys.exit()
    
while True:
    for line in sys.stdin:
        fifo.write(line) 
        fifo.flush()
        print ("Sending:", str(line))
    print ("Closing")

    fifo.close()


