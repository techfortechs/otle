# set_up_rover.py: Used to set up RPi with Sparkfun U blox Z-F9P and Adafruit RFM95W LoRa Radio Transceiver Breakout
#
# This script by Scott Dynes scott@dynes.org
#
# This script based on breadboard setup
# First version 30-Mar-2022
#
# 5 Apr 2022 updated to account for tests. Using reliable datagrams, upping serial rate.
# 7 Apr 2022 removed the 'with_ack=True'
# 21 Apr 2022 updated to reflect switching of ttyAMA1 and ttyS0
# 01 Aug 2022 updated following testing of LoRa link
# 01 May 2023 updated to test if we are getting RTCM3 messages

from datetime import datetime as dtime
import rfm9x_tools   # expected to be in python path somewhere...
import time
import serial
import serial.threaded
import syslog
import sys
import os
from pyrtcm import RTCMReader
from threading import Thread, Event, Lock

debug_mode = False
serial_lock = Lock()
stop_event = Event()

# open file to write log of RTCM messages
log_file = open("/home/pi/t4t/RTCM_log_file.txt", "a")

# named pipe path. this has to be same for sending and receiving
pipe_path = "/home/pi/t4t/tmp_pipe_file"

# set up named pipe file
if os.path.exists(pipe_path):
    os.remove(pipe_path)
try:
    os.mkfifo(pipe_path)
except Exception as e:
    print (f"set_up_rover: can't create named pipe file; {e} ")
    sys.exit()


# node mumber of the rovers. This had better be unique in your radio environment...
rover_node = 142

# define the thread fcn to check the message
def get_RTCM_msg(
    log_file: object,
    lock: Lock,
    stop: Event,
):
    debug_mode = False
    try:
        stream = open(pipe_path, 'rb')
        print("*** set_up_rover: openign named pipe for reading **** \n")
    except Exception as err:
        print(f"\n\n set_up_rover:  something went wrong opening named pipe loop {err}\n\n")
        syslog.syslog(syslog.LOG_INFO,f"set_up_rover: something went wrong opening named pipe loop; {err} ")
    if debug_mode:
        syslog.syslog(syslog.LOG_INFO,f"set_up_rover: opened pipe_path as stream ")
        print(f"set_up_rover: opened pipe_path as stream ")                                                            
    rtr = RTCMReader(stream)
    try:
        while not stop.is_set():
            (raw_data, parsed_data) = rtr.read()
            if parsed_data:
                log_file.write(f" {parsed_data.identity}   {len(raw_data)}  {dtime.now().strftime('%Y-%m-%d %H:%M:%S')} \n ") 
                if debug_mode:
                    syslog.syslog(syslog.LOG_INFO,f"set_up_rover: parsed RTCM message  {parsed_data.identity} ")
                    print(f"set_up_rover: parseded RTCM message  {parsed_data.identity} ")                                                            
    except Exception as err:
        print(f"\n\n set_ip_rover: in read named pipe loop something went wrong {err}\n\n")
        syslog.syslog(syslog.LOG_INFO,'set_up_rover: in read named pipe loop something went wrong - ' + str(err))
    
    
# set up Pi serial port. Make sure that pi is configured so no serial login shell, and serial port is on
ser = serial.Serial(
        port='/dev/ttyAMA1', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        baudrate = 57600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
)
# This assumes that the serial port on the U blox chip (usually UART2) is set similarly.
# This is not the default; use u-center to check and change.

# Set up radio
try:
    rfm9x, node = rfm9x_tools.set_up(rover_node)  # the base node number can be anything; the rover nodes need to all be the same.
except Exception as err:
    print(f"\n\nSomething went wrong {err}\n\n")
    syslog.syslog(syslog.LOG_INFO,'set_up_rover: somethign went wrong - ' + str(err))

# set the msg index to zero
msg_index = 0

# define the thread
RTCM_thread = Thread(
    target= get_RTCM_msg,
    args=(
        log_file,
        serial_lock,
        stop_event,
    ),
)

# start the thread. should open the named pipe for reading
RTCM_thread.start()


# open named pipe path for writing binary:
try:
    fifo = open(pipe_path, "wb")
    print("***** set_up_rover: opening named pipe for writing - will block until reader is opened ****\n ")
except Exception as e:
    print (f"set_up_rover: can't open named pipe file; {e} ")
    sys.exit()


# Start reading the serial port. If something shows up, send it to the rovers

try:
    while 1:
        packet = None
            
        # write the lora_rssi every 100 messages
        if ((msg_index % 100) == 0):
            if debug_mode:
                print(f"writing lora_rssi; msg_index is {msg_index} ")
            text_file = open("/home/pi/t4t/lora_rssi.txt", "w")
            n = text_file.write(str(rfm9x.rssi))
            text_file.close() 
            
        # check for packet rx                                                                      
        packet = rfm9x.receive()
        if packet is not None:
            if debug_mode:
                print(f"listening to radio got hex  {packet.hex()} msg index is {msg_index} \n")
            ser.write(packet) # write packet to ublox
            fifo.write(packet) # write packet to named pipe so py2rtcm can see if it is a packet
            fifo.flush()
            msg_index += 1
    
except KeyboardInterrupt:  # capture Ctrl-C
    print("\n\nTerminated by user.")
    log_file.close()
    stop_event.set()
    fifo.close()
    try:
        os.unlink(fifo)
    except:
        pass


print("\nStop signal set. Waiting for threads to complete...")
RTCM_thread.join()
print("\nProcessing complete")        
    
        

    
        


