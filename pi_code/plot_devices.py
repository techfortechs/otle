#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to accept data from the mqtt broker, and plot the positions of the devices.
#
# At some point this should write to a database
#
# First version written 19 May 2022 by Scott Dynes scott@dynes.org
#

import numpy as np
import pandas as pd
import geopandas as gpd
import contextily as cx
import time
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from shapely.geometry import Point, LineString
from shapely.geometry import asPoint
from numpy import asarray

gpd.options.display_precision = 9

def lonlat2UTM(lonlat):  # lonlat is [long, lat]
    utm = (np.floor((lonlat[0] + 180) / 6) % 60) + 1
    if(lonlat[1] > 0): 
        utm = utm + 32600
    else:
        utm = utm + 32700
    return int(utm)

def rotation(r):
    return np.asarray([np.cos(r), -np.sin(r), np.sin(r), np.cos(r)]).reshape(2, 2)

    

df = gpd.read_file("/Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code/fileotle.json", rows=slice(-2,-1))
utm = lonlat2UTM([df.geometry[0].x,df.geometry[0].y ])
proj = "EPSG:" + str(utm)

fig = plt.figure( figsize=(16.0,8.0),facecolor='#DEDEDE')
ax = fig.add_subplot(1,1,1)

def plot_data(i):

    df = gpd.read_file("/Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code/fileotle.json", rows=slice(-50,-1))
    df.geometry = df.geometry.to_crs(proj)

    ax.clear()
    df.plot(column='boat_id', cmap='viridis', ax=ax, legend = True)

    # make plot square
    [y_min, y_max] = ax.get_ylim()
    [x_min, x_max] = ax.get_xlim()
    yd=y_max-y_min
    ym=(y_max+y_min)/2
    xd=x_max-x_min
    xm=(x_max+x_min)/2
    if(yd>xd):
       ax.set_xlim([xm-yd/2,xm+yd/2])
    else:
       ax.set_ylim([ym-xd/2,ym+xd/2])
    
    plt.title(str(i))
    
ani = animation.FuncAnimation(fig, plot_data, interval=200)

#plot_data()

plt.show()
