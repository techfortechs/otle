#!/bin/bash
# we want to use the USB wlan device. If wlan1 exists, shut down the onboard wlan0                                                                             
# wait a chunk of time so that ansible will be able to find this pi on the original IP
# after a reboot
sleep 120


if ip a | grep wlan1 | grep -q 'state UP'; then

    if ip a | grep wlan0 | grep -q 'state UP'; then    
        sudo ip link set wlan0 down
        echo "wlan1 is UP, wlan0 was up and then shut down"
    else
	echo "wlan1 is UP, wlan0 was down"
    fi
else
    echo "wlan1 not UP"
fi


