!#/usr/bin/python3

import rfm9x_tools   # expected to be in python path somewhere...
import time
import serial
import serial.threaded

# node mumber of the rovers. This had better be unique in your radio environment...
rover_node = 142

# set up Pi serial port. Make sure that pi is configured so no serial login shell, and serial port is on
ser = serial.Serial(
        port='/dev/ttyAMA1', #Replace ttyS0 with ttyAM0 for Pi1,Pi2,Pi0
        baudrate = 57600,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        timeout=1
)
# This assumes that the serial port on the U blox chip (usually UART2) is set similarly.
# This is not the default; use u-center to check and change.

# Set up radio
rfm9x, orig_node = rfm9x_tools.set_up(7)  # the base node number can be anything; the rover nodes need to all be the same.

rfm9x.destination = rover_node # the default destination, will be used in with_ack


# Start reading the serial port. If something shows up, send it to the rovers



stop_time = time.time() + 10 # send 10 seconds of data
data_dir = {}

data_dir[0] = b'd3005e4320005f70116200004082040200000000200080007b486988c8c8da97350bd12a21a26346446ec85265710a785ec4302e30fa40c209e405f59bcb2180231a824b58ffffdfa015cd2d4fdcf80000000000000000000000000000000000003ae35e'
data_dir[1]= b'd300044ce00080ededd6'
data_dir[2]= b'd3005e43c0008f9c662200000060b000000000002080000077a7e8e8c90819ead5737ef3211dc2351f7af5b76aee0eca1aa7c8984968c092d3ff7eebe9e037a733a061b6814729f61f2fff7ffff80453456d165140000000000000000000000000d841e4'
data_dir[3] = b'd300504460005f7011620000000000a140000000200100003fa92c2727bdb1539b269bdce1e5c8073f367980313ffc438ef7fa9fabdfd36f3597bcc0c9fd03240b53aaefffcf00d3f5d96c34800000000000003747c4'
data_dir[4] = b'd300264640005f6f4640000000000006000000002000000068ea5e3cc004a084e80ddfe0f724ff94d2007190'
data_dir[5] = b'd3005e4320005f70210200004082040200000000200080007b486988c8c8da97390ad14a2e7eecfee30c458b12240fc3eeafcbb99f686e015832032eb0247ff0384abecb75ffffdfa0164d254fdcf00000000000000000000000000000000000004b7cef'

i = 0
while (time.time() < stop_time):
    
    ## write to  the serial port
    ser.write(data_dir[i])
    print(f" {data_dir[i]} \n")
    time.sleep(0.4)
        
    i = (i + 1) % 5
    
    
        


