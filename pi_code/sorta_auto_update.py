#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to hack an autoupdate functionality
#
# Sets up the mqtt client, and pings and listens on 'otle_update' for the ip of the code repo
# Once get the ip, copies over the 'current_version.txt' file and compares the included
# hash with the local hash. If they differ, then rsyncs the .py and .sh files to the
# t4t dir.
#
# This should be replaced by something more better
#
# First version written 3 Aug 2022 by Scott Dynes scott@dynes.org
# 26 Oct 2022 changing boat_id to dev_id scott@dynes.org
#

import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import time
import subprocess
import gps
import datetime
import json
import math
import socket # should go away once have bluetooth way to find boat ID
import signal

# need to wait until the network is available - so wait 2 minutes
time.sleep(12)


#this is newer again

# this service should start before other otle services

MQTT_SERVER = "192.168.50.14" #  The MIT MQTT broker or Scott's laptop via tomato on Linksys

MQTT_UPDATE_TOPIC = "otle_update"

CLEAN_SESSION=True

t4t_homedir = "/home/pi/t4t/"


# get dev_id - this should run after the device ID has been set
dev_id = open((t4t_homedir + "dev_id.txt"), "r").readline().strip()
if (dev_id == ''):
    syslog.syslog(syslog.LOG_ERR,'dev_id is empty; exiting')
    exit(1)
    
my_topic =  MQTT_UPDATE_TOPIC + '/' + str(dev_id)
print( "my_topic: " + my_topic)

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'Paho connected with result code' +str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    print("In on_connect callback")
    ret = client.subscribe(my_topic)
    client.subscribe("otle_update/everyone_update")
    ip_client.publish(my_topic, "send_update_info")

    
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    # message should be of form 'current_version', <current hash>, <ip address>, <directory>
    # e.g. current_version, 42, 192.168.50.104, /Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code
    message = str(msg.payload.decode("utf-8","ignore"))
    syslog.syslog(syslog.LOG_INFO,'update message received: ' + str( message))
    print("In sorta_ on_message: received " + message)
    if(message != 'send_update_info'):
        print("In sorta_ on_message not send_update_info: received " + message)
        try_update(message)


def on_otle_update(client, userdata, msg):
    # this is a message to all otle_update listeners
    # This is registered to the topic otle_update/everyone_update and we should try to update
    message = str(msg.payload.decode("utf-8","ignore"))
    syslog.syslog(syslog.LOG_INFO,'on_otle_update message received: ' + str( message))
    print("In on_otle_update message: received " + message)
    try_update(message)

        
def try_update(message):
    print("In try_update")
    m = json.loads(message)
    # see if we have an appropriate message, and if so get the vars
    if (m[0] == "current_version"):
        current_hash = m[1]
        ip_addr = m[2]
        dir = m[3]
        syslog.syslog(syslog.LOG_INFO,"sorta_update: ip_addr and dir: " + str(ip_addr) + " " + str(dir))
        # check to see if the current version is equal to what was sent
        my_hash = open((t4t_homedir + "current_version.txt"), "r").readline().strip()
        if(my_hash != current_hash):
            syslog.syslog(syslog.LOG_INFO,"hashes don't match; running update_code.")
            print("hashes don't match; running update_code.")
            print("otle_update: " + ip_addr + " and dir = " + dir)
            cmd = "/home/pi/t4t/update_code.sh " +  str(ip_addr) + " " + str(dir)
            print("cnd: " + cmd)
            subprocess.run([cmd] , shell=True)
            ip_client.publish(MQTT_UPDATE_TOPIC, "Device " + str(dev_id) + " tried to update code")
        else:
            print("hashes match; not running update_code.")
            syslog.syslog(syslog.LOG_INFO,"hashes match; not running update_code.")


# create the ctl-c handler
def handler(signum, frame):
    # close the connection and exit 
    ip_client.loop_stop()
    ip_client.disconnect()
    exit(0)

signal.signal(signal.SIGINT, handler)

            
ip_client = mqtt.Client(dev_id,clean_session=CLEAN_SESSION)
ip_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ip_client.on_connect = on_connect
ip_client.on_message = on_message
ip_client.message_callback_add("otle_update/everyone_update", on_otle_update)
ip_client.reconnect_delay_set(min_delay=1, max_delay=1200) # need to change if races are longer than 20 minutes
ip_client.connect(MQTT_SERVER, keepalive=600)

ip_client.loop_start()

while True:
    time.sleep(30.0)
    print(".", end = "")


         
