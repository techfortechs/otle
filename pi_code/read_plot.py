#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Script to accept data from the mqtt broker and plot the positions of the devices.
#
# At some point this should write to a database
#
# First version written 15 July 2022 by Scott Dynes scott@dynes.org
#

import numpy as np
import pandas as pd
import geopandas as gpd
import contextily as cx
import time
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from shapely.geometry import Point, LineString
from shapely.geometry import asPoint
from numpy import asarray
import syslog
import paho.mqtt.client as mqtt #import library
from queue import Queue
import sys
import time
import os
import datetime
import math
import json
import signal
import pdb

gpd.options.display_precision = 9

q = Queue()

# create file name to store mqtt messages
# check to see if dir exists; create if it does not
if os.path.exists("/Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code/mqtt_data/"):
    fname = "/Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code/mqtt_data/base_test_" + time.strftime("%Y_%j_%H%M%S", time.localtime())
else:
    os.mkdir("/Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code/mqtt_data/")
    fname = "/Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code/mqtt_data/base_test_" + time.strftime("%Y_%j_%H%M%S", time.localtime())
# ... and open it in append mode
mqtt_file =  open(fname, "a")



# create the empty geopandas dataframe
headers = ['geometry','time', 'course_t', 'course_m', 'speed', 'boat_id', 'skipper']
df = gpd.GeoDataFrame(columns=headers, crs="EPSG:4326")

def lonlat2UTM(lonlat):  # lonlat is [long, lat]
    utm = (np.floor((lonlat[0] + 180) / 6) % 60) + 1
    if(lonlat[1] > 0): 
        utm = utm + 32600
    else:
        utm = utm + 32700
    return int(utm)

def rotation(r):
    return np.asarray([np.cos(r), -np.sin(r), np.sin(r), np.cos(r)]).reshape(2, 2)


# create the ctl-c handler
def handler(signum, frame):
    # close the file and exit
    mqtt_file.close()
    ll_client.loop_stop()
    ll_client.disconnect()
    exit(0)

signal.signal(signal.SIGINT, handler)


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    syslog.syslog(syslog.LOG_INFO,'Paho connected with result code' +str(rc))
 
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    ret = client.subscribe(MQTT_GPS_SEND )
    

# The callback for when a PUBLISH message is received from the server.
# All the data munging and recording work is done here.
def on_message(client, userdata, msg):
    # put the data into the gpd dataframe. Convert the long/lat to a gpd Point
    q.put(str(msg.payload.decode("utf-8","ignore")))



# Set up the broker

MQTT_SERVER = '192.168.50.14' #  MIT t4t broker or Scott's laptop via tomato on Linksys
#MQTT_SERVER = "192.168.83.171"  # MQTT broker for a test on scott's hotspot network
MQTT_GPS_SEND = "here_i_am" # this is the name of topic, like temp
MQTT_OTLE_REC = "over_early_q"

client_name = 'otle_processor'

ll_client = mqtt.Client(client_name)
ll_client.username_pw_set(username='t4t',password='GoBeavers5!') # set up authentication
ll_client.on_message = on_message
#ll_client.on_publish = on_publish        #attach function to callback
ll_client.reconnect_delay_set(min_delay=1, max_delay=1200) # need to change if races are longer than 20 minutes
ll_client.connect(MQTT_SERVER)

ll_client.subscribe(MQTT_GPS_SEND)  # subscribe to the 'here_i_am' messages

ll_client.loop_start()  #use this line if you want to write any more code here

# set up the plot in interactive mode

fig = plt.figure( figsize=(16.0,8.0),facecolor='#DEDEDE')
ax = fig.add_subplot(1,1,1)
ax.clear()
plt.ion()


# counter
frame = 0
while True:

    # clear the plot
    ax.clear()
    
    # recreate the geodataframe
    df = gpd.GeoDataFrame(columns=headers, crs="EPSG:4326")
    # print("before: " + str(df))
    while not q.empty():
        message = q.get()
        # write message to file
        mqtt_file.write(message)

        #print(message)
        msg = json.loads(message)

        df = df.append({"geometry":Point(msg["long"],msg["lat"]), "time": msg["time"], "course_t": msg["course_t"], "course_m": msg["course_m"], "speed": msg["speed"], "boat_id":msg["boat_id"], "skipper": msg["skipper"]}, ignore_index=True)
        df.geometry.crs="EPSG:4326"

    # print("after: " + str(df))
#        df.to_file("fileotle.json", driver="GeoJSON")
#df = gpd.read_file("/Users/sdynes/IBI/MIT_t4t/tech4techs/pi_code/fileotle.json", rows=slice(-2,-1))

    if (len(df) >  0):
        utm = lonlat2UTM([df.geometry[0].x,df.geometry[0].y ])
        proj = "EPSG:" + str(utm)
        df.geometry = df.geometry.to_crs(proj)

    # do the plotting
    #print("at plot: " + str(df))
    #breakpoint()
    df.plot(column='boat_id', cmap='viridis', ax=ax, legend = True)

    # make plot square
    [y_min, y_max] = ax.get_ylim()
    [x_min, x_max] = ax.get_xlim()
    yd=y_max-y_min
    ym=(y_max+y_min)/2
    xd=x_max-x_min
    xm=(x_max+x_min)/2
    if(yd>xd):
        qw=0
        ax.set_xlim([xm-yd/2,xm+yd/2])
    else:
        qw=0
        ax.set_ylim([ym-xd/2,ym+xd/2])
    
    plt.title(str(frame))
    frame +=1
    plt.show()
    plt.pause(0.001)

    q = []
    q = Queue()
    
    time.sleep(1.0)
