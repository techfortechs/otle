#
# a place to put OTLE python helper scripts in one place
#
#
 

def set_mag_heading():
    import subprocess

    global mag_heading

    # AHRS
    
    popen = subprocess.Popen(["minimu9-ahrs", "--output=euler"], stdout=subprocess.PIPE, universal_newlines=True)
    for stdout_line in iter(popen.stdout.readline, ""):

        vals = stdout_line.split()
        mag_heading = float(vals[0]) + 180.0 * int(abs(float(vals[1])) > 90.0)
        #print('magnetic heading: ' + str(mag_heading))
    popen.stdout.close()


