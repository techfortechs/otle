
# Paste this in

import json

from django.utils.dateparse import parse_datetime
from django.contrib.gis.geos import Point, LineString

from races.models import Race, Boat, RaceTick

start_line = LineString(Point(-71.0878439, 42.3584436), Point(-71.0860000, 42.3535000))
name = "Test Race"
file = "../fileotle.old"
with open(file, 'r') as fhl:
    js = json.load(fhl)

crs = js['crs']['properties']['name']
started = None

for f in js['features']:
    p = f['properties']
    p['time'] = parse_datetime(p['time'].strip('Q'))
    if started is None or p['time'] < started:
        started = p['time']

race = Race(name=name, start_dt=started, start_line=start_line)
race.save()

for f in js['features']:
    if 'properties' not in f or 'geometry' not in f:
        continue
    p = f['properties']
    g = f['geometry']
    boat, _ = race.boats.get_or_create(alt_id=p['boat_id'], defaults={
        'skipper': p['skipper'],
        'name': "boat:%s" % p['boat_id'],
    })
    # A one second resolution
    delta = (p['time'] - started).total_seconds()
    boat.ticks.get_or_create(race=race, ctime=delta,
        defaults={
            'coord': Point(*g['coordinates']),
            'course_t': p['course_t'],
            'course_m': p['course_m'],
            'speed': p['speed'],
        })
