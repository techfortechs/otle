import logging
import uuid

from datetime import timedelta

import numpy as np
from django.contrib.gis.db import models as gis
from django.contrib.postgres.aggregates import ArrayAgg
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.indexes import BrinIndex
from django.db import models
from django.db.models.functions import Now
from django.db.models.lookups import GreaterThanOrEqual
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django_cte import CTEManager

from races.functions import MakeLine

logger = logging.getLogger(__name__)


class Regatta(models.Model):
    """This model encompasses a 'Regatta'.
    Regattas are an ordered set of races which are scheduled.
    """

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=128)
    created_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"<Regatta {self.name} {self.created_on}/>"

    def get_absolute_url(self):
        return reverse("regatta-detail", args=[self.uuid])


class BoatOutline(models.Model):
    """
    This model represents the physical outline of a boat class. These
    2D outlines are used in the crossing boundry calculations for OTLE
    and so should be made as accurate as possible.
    """

    name = models.CharField(max_length=128, unique=True)
    outline = ArrayField(
        ArrayField(models.FloatField(), size=2),
        help_text="The 2D measurements of th boat outline. The array should "
        "be a list of lists with the inner list having 2 entries.",
    )  # 2 Dimensional Array
    meter_length = models.FloatField(default=15, blank=True)

    def __str__(self):
        return self.name

    @classmethod
    def outline_from_file(cls, file):
        """
        Create an outline from a file. Each listed point must be in meter
        units as offsets from a common point.

        File Format
        -----------
        x0_offset, y0_offset\n
        x1_offset, y1_offset\n
        ...
        """
        try:
            with open(file, "rt") as fin:
                array = np.loadtxt(fin, delimiter=",")
        except TypeError:
            array = np.loadtxt(file, delimiter=",")

        length = max(array[:, 1]) - min(array[:, 1])

        outline = array.tolist()
        return outline, length

    @classmethod
    def from_file(cls, file, name=None):
        outline, length = cls.outline_from_file(file)
        return cls(name=name, outline=outline, meter_length=length)


class Boat(models.Model):
    """
    This model represents a physical boat.
    """

    name = models.CharField(max_length=16)
    alt_id = models.CharField(max_length=32, db_index=True)
    outline = models.ForeignKey(BoatOutline, on_delete=models.CASCADE)

    # skipper = models.CharField(max_length=64, null=True, blank=True)

    def __str__(self):
        return f"<Boat {self.name}, aka {self.alt_id}>"


class Device(models.Model):
    """
    This model describes the physical sensors used to track boat position
    and orientation.
    """

    class DeviceChip(models.TextChoices):
        F9P = "FP", _("GPS RTK")
        F9R = "FR", _("GPS Fusion ")
        M8P = "MA", _("NEO-M8P-2 GPS RTK")

    class AsUsed(models.TextChoices):
        BASE = "BA", _("Base")
        ROVER = "RO", _("Boat/Mark")
        UNKNOWN = "UN", _("Unknown")

    name = models.CharField(max_length=16)
    hardware_id = models.CharField(max_length=16)
    chip = models.CharField(
        max_length=2, choices=DeviceChip.choices, default=DeviceChip.F9P
    )
    use = models.CharField(
        max_length=2, choices=AsUsed.choices, default=AsUsed.UNKNOWN
    )
    alt_id = models.CharField(max_length=32, db_index=True)

    def __str__(self):
        return self.name

    @property
    def position_channel(self):
        return f"{self.hardware_id}/position"

    @property
    def status_channel(self):
        return f"{self.hardware_id}/status"


class DevicePositionLog(models.Model):
    """
    This model describes the timeseries positional data that will be
    retrieved from devices.
    """

    class Meta:
        indexes = [
            BrinIndex(models.F("datetime"), name="devicelog_idx"),
        ]
        get_latest_by = "datetime"
        ordering = ["-datetime"]

    datetime = models.DateTimeField()
    device = models.ForeignKey(Device, db_index=True, on_delete=models.CASCADE)

    coordinate = gis.PointField(srid=4326)
    magnetic_heading = models.FloatField()  # TODO REVISE WITH NEW DATA
    true_heading = models.FloatField()
    position_uncertainty = models.FloatField()  # TODO REVISE WITH NEW DATA

    objects = CTEManager()

    def __str__(self):
        return f"<DPL {self.device_id}: {self.datetime} {self.coordinate}"


class Mark(models.Model):
    """
    This class describes physical markers in a race. These markers will
    have devices associated with them. However these devices may be
    swapped between races (i.e hardware failure and replacement).
    """

    class MarkType(models.TextChoices):
        PIN = "PI", _("Pin")
        RC = "RC", _("RC boat")
        MARK = "MA", _("Mark")

    name = models.CharField(max_length=16)
    mark_type = models.CharField(
        max_length=2,
        choices=MarkType.choices,
        default=MarkType.MARK,
    )
    static_position = gis.PointField(null=True, srid=4326)

    alt_id = models.CharField(max_length=32, db_index=True)

    def __str__(self):
        return self.name


class Line(models.Model):
    """
    This model represents either a start or finish line.
    """

    name = models.CharField(max_length=128)
    pin = models.ForeignKey(
        Mark, on_delete=models.CASCADE, related_name="as_pin_for"
    )
    rc_boat = models.ForeignKey(
        Mark, on_delete=models.CASCADE, related_name="as_rc_for"
    )

    is_rc_on_right = models.BooleanField(
        help_text="Is the RC boat on the left or right of the line?",
        default=True,
    )

    pin_device = models.ForeignKey(
        Device,
        on_delete=models.CASCADE,
        related_name="as_pin_devices",
        null=True,
    )

    rc_device = models.ForeignKey(
        Device,
        on_delete=models.CASCADE,
        related_name="as_rc_devices",
        null=True,
    )

    objects = CTEManager()

    def __str__(self):
        return self.name

    @classmethod
    def as_ST_Line(cls, prefix=""):
        return MakeLine()


class BoatSetup(models.Model):
    """
    This model represents the relationship of a participating boat and its
    device in a race. The same physical boat may participate in multiple
    races and its device (sensor) might change between races (i.e hardware
    failure).

    ``side_side_offset`` is the distance in meters from the centerline of the boat
    port-side having negative values and starboard-side having positive
    values to the ``registered_device``.

    ``fore_aft_offset`` is the distance in meters along the centerline from the 
    fiducial position to the device position. Distance towards the bow are psoitve.
    
    """

    race = models.ForeignKey(
        "Race", on_delete=models.CASCADE, related_name="boat_setups"
    )
    boat = models.ForeignKey(Boat, on_delete=models.CASCADE)

    side_side_offset = models.FloatField(null=False)
    fore_aft_offset = models.FloatField(null=False)
    registered_device = models.ForeignKey(Device, models.CASCADE)

    objects = CTEManager()

    def __str__(self):
        return f"BoatSetup for {self.race.name} and {self.boat.name}"


class MarkSetup(models.Model):
    """
    This model represents the relationship of a marker in the context of a
    race. Markers may be re-used throughout a Regatta. In addition, the
    Device hardware onboard a Marker may fail and thus Markers may have
    different Devices throughout a Regatta.
    """

    race = models.ForeignKey(
        "Race", on_delete=models.CASCADE, related_name="mark_setups"
    )
    mark = models.ForeignKey(Mark, on_delete=models.CASCADE)
    registered_device = models.ForeignKey(Device, on_delete=models.CASCADE)

    def __str__(self):
        return f"MarkSetup for {self.race.name} and {self.mark.name}"


class MarkCourseOrder(models.Model):
    course = models.ForeignKey(
        "Course", on_delete=models.CASCADE, related_name="mark_ordering"
    )
    mark = models.ForeignKey(
        Mark, on_delete=models.CASCADE, related_name="mark_orderings"
    )
    order = models.SmallIntegerField()

    class Meta:
        ordering = [
            "order",
        ]

    def __str__(self):
        return f"MarkCourseOrder: {self.course} {self.mark} {self.order}"


class Course(models.Model):
    """
    This model represents a course layout, complete with an ordered set of
    marks, a start line, and a finish line. The start and finish line might be
    the same object.
    """

    name = models.CharField(max_length=128, unique=True)
    starting_line = models.ForeignKey(
        Line, on_delete=models.CASCADE, related_name="as_starting_line_for"
    )
    finish_line = models.ForeignKey(
        Line, on_delete=models.CASCADE, related_name="as_finish_line_for"
    )
    marks = models.ManyToManyField(Mark, through=MarkCourseOrder)

    def __str__(self):
        return self.name


class Race(models.Model):
    """This model encompasses a 'Race'.
    Races are an event where boats race around a set course made of
    markers.
    """

    class RaceState(models.IntegerChoices):
        SETUP = 0
        RUNNING = 1
        FINISHED = 2

    class AllRaceStates(models.IntegerChoices):
        SETUP = 0
        FINISHED = 2

        COMMENCING = 3
        ONGOING = 4

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=128)
    parent_regatta = models.ForeignKey(
        Regatta, related_name="races", on_delete=models.CASCADE
    )
    _race_state = models.SmallIntegerField(
        db_column="race_state",
        choices=RaceState.choices,
        default=RaceState.SETUP,
    )

    start_time = models.DateTimeField()
    countdown_time = models.DurationField()

    course = models.ForeignKey(
        Course, on_delete=models.CASCADE, related_name="as_course_for"
    )

    # Define many to many relations
    boats = models.ManyToManyField(
        Boat, through=BoatSetup, related_name="races"
    )
    marks = models.ManyToManyField(
        Mark, through=MarkSetup, related_name="races"
    )

    def __str__(self):
        return f"{self.name} at {self.start_time}"

    def get_absolute_url(self):
        return reverse("race-detail", kwargs={"slug": self.uuid})

    @property
    def race_state(self):
        if self._race_state == self.RaceState.RUNNING:
            if timezone.now() + timedelta(0.2) < self.start_time: # the last data before the horn
                state = self.AllRaceStates.COMMENCING
            else:
                state = self.AllRaceStates.ONGOING
        elif self._race_state == self.RaceState.FINISHED:
            state = self.AllRaceStates.FINISHED
        elif self._race_state == self.RaceState.SETUP:
            state = self.AllRaceStates.SETUP
        else:
            raise RuntimeError(f"Unknown state {self._race_state}")
        logger.info(f'in race_state(self), returning {state}')
        return state

    @race_state.setter
    def race_state(self, value):
        if value == self.RaceState.SETUP:
            self._race_state = self.RaceState.SETUP
            return
        elif value == self.RaceState.RUNNING:
            if self._race_state != self.RaceState.SETUP:
                raise ValueError(
                    "Can't start race when it's state is "
                    f"{self._race_state}"
                )
            else:
                now = timezone.now()
                self._race_state = self.RaceState.RUNNING
                logger.debug(
                    f"Restarting {self.name} {now} + "
                    f"{self.countdown_time} = "
                    f"{now + self.countdown_time}"
                )
                self.start_time = now + self.countdown_time
            return
        elif value == self.RaceState.FINISHED:
            self._race_state = self.RaceState.FINISHED
            return
        else:
            raise ValueError(f"Attempting to set race state to {value}")

    @classmethod
    def race_state_expression(cls):
        expr = models.Case(
            models.When(
                GreaterThanOrEqual(
                    Now(), models.F("start_time") + models.F("countdown_time")
                ),
                then=models.Value(cls.AllRaceStates.ONGOING),
            ),
            default=models.Value(cls.AllRaceStates.COMMENCING),
        )

        return expr

    @property
    def is_in_setup(self):
        return self.race_state == self.AllRaceStates.SETUP

    @property
    def is_commencing(self):
        return self.race_state == self.AllRaceStates.COMMENCING

    @property
    def is_ongoing(self):
        return self.race_state == self.AllRaceStates.ONGOING

    def start(self):
        self.race_state = self.RaceState.RUNNING
        self.save()

    def restart(self):
        logger.info('in models.restart')
        self.race_state = self.RaceState.SETUP
        self.save() 

    def finish(self):
        self.race_state = self.RaceState.FINISHED
        self.save()

    @classmethod
    def get_race_summaries(cls):
        q = cls.objects
        q = q.annotate(
            boat_devices=ArrayAgg(
                "boat_setups__registered_device__hardware_id"
            ),
            mark_devices=ArrayAgg(
                "mark_setups__registered_device__hardware_id"
            ),
            is_rc_on_right_start_line=models.F(
                "course__starting_line__is_rc_on_right"
            ),
            is_rc_on_right_finish_line=models.F(
                "course__finish_line__is_rc_on_right"
            ),
            start_line_pin_device=models.F(
                "course__starting_line__pin_device__hardware_id"
            ),
            start_line_rc_device=models.F(
                "course__starting_line__rc_device__hardware_id"
            ),
            finish_line_pin_device=models.F(
                "course__finish_line__pin_device__hardware_id"
            ),
            finish_line_rc_device=models.F(
                "course__finish_line__rc_device__hardware_id"
            ),
            last_refreshed_on=Now(),
        )
        q = q.values(
            "pk",
            "_race_state",
            "start_time",
            "boat_devices",
            "mark_devices",
            "is_rc_on_right_start_line",
            "is_rc_on_right_finish_line",
            "start_line_pin_device",
            "start_line_rc_device",
            "finish_line_pin_device",
            "finish_line_rc_device",
            "last_refreshed_on",
        )
        return q
