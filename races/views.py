from django.conf import settings
from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Race, Regatta


class ListRegattaView(ListView):
    template_name = "races/regattas.html"
    queryset = Regatta.objects.order_by("-created_on")
    context_object_name = "regattas"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["mqtt_host"] = settings.MQTT_HOST
        context["mqtt_port"] = settings.MQTT_PORT
        context["mqtt_username"] = settings.MQTT_USERNAME
        context["mqtt_password"] = settings.MQTT_PASSWORD

        return context


class DetailRegattaView(DetailView):
    template_name = "races/regatta_detail.html"
    slug_field = "uuid"
    model = Regatta

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["races"] = self.object.races.order_by("-start_time").all()
        context["mqtt_host"] = settings.MQTT_HOST
        context["mqtt_port"] = settings.MQTT_PORT
        return context


class RaceView(DetailView):
    slug_field = "uuid"
    model = Race


def live_race(request, slug):
    race = Race.objects.get(uuid=slug)
    return render(
        request,
        "races/race.html",
        {
            "race": race,
            "mqtt_host": settings.MQTT_HOST,
            "mqtt_port": settings.MQTT_PORT,
            "mqtt_username": settings.MQTT_USERNAME,
            "mqtt_password": settings.MQTT_PASSWORD,
        },
    )


def debug(request):
    return render(
        request,
        "races/debug.html",
        {
            "mqtt_host": settings.MQTT_HOST,
            "mqtt_port": settings.MQTT_PORT,
            "mqtt_username": settings.MQTT_USERNAME,
            "mqtt_password": settings.MQTT_PASSWORD,
        },
    )
