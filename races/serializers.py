import serpy
import utm

from races.models import BoatSetup, MarkCourseOrder, MarkSetup


class PointField(serpy.Field):
    def to_value(self, value):
        return {
            "longitude": value.x,
            "latitude": value.y,
            "srid": value.srid,
        }


class LineField(serpy.Field):
    def to_value(self, value):
        e1, n1, _, _ = utm.from_latlon(value[0][1], value[0][0])
        e2, n2, _, _ = utm.from_latlon(value[1][1], value[1][0])
        return (
            {
                "longitude": value[0][0],
                "latitude": value[0][1],
                "easting": e1,
                "northing": n1,
            },
            {
                "longitude": value[1][0],
                "latitude": value[1][1],
                "easting": e1,
                "northing": n1,
            },
        )


class PolyField(serpy.Field):
    def to_value(self, value):
        return value.json()


class RegattaSerializer(serpy.Serializer):
    uuid = serpy.StrField()
    name = serpy.StrField()


class BoatOutlineSerializer(serpy.Serializer):
    id = serpy.IntField()
    name = serpy.StrField()
    meter_length = serpy.FloatField()


class DeviceSerializer(serpy.Serializer):
    id = serpy.IntField()
    name = serpy.StrField()
    hardware_id = serpy.StrField()
    chip = serpy.StrField()
    use = serpy.StrField()
    alt_id = serpy.StrField()

    status_channel = serpy.StrField()


class MarkSerializer(serpy.Serializer):
    id = serpy.IntField()
    name = serpy.StrField()
    mark_type = serpy.StrField()
    static_position = PointField()
    easting_northing = serpy.MethodField()
    alt_id = serpy.StrField()

    def get_easting_northing(self, instance):
        p = instance.static_position
        e, n, _, _ = utm.from_latlon(p.y, p.x)
        return {
            "easting": e,
            "northing": n,
        }


class LineModelSerializer(serpy.Serializer):
    id = serpy.IntField()
    name = serpy.StrField()
    pin = MarkSerializer()
    rc_boat = MarkSerializer()

    pin_device = DeviceSerializer()
    rc_device = DeviceSerializer()


class BoatSerializer(serpy.Serializer):
    id = serpy.IntField()
    name = serpy.StrField()
    alt_id = serpy.StrField()

    outline = BoatOutlineSerializer()


class MarkCourseOrderSerializer(serpy.Serializer):
    mark = MarkSerializer()
    order = serpy.IntField()


class CourseSerializer(serpy.Serializer):
    id = serpy.IntField()
    name = serpy.StrField()
    starting_line = LineModelSerializer()
    finish_line = LineModelSerializer()

    marks = serpy.MethodField()

    def get_marks(self, instance):
        q = MarkCourseOrder.objects.select_related("course", "mark")
        q = q.filter(course=instance).order_by("order")
        return MarkCourseOrderSerializer(q.all(), many=True).data


class MarkSetupSerializer(serpy.Serializer):
    mark = MarkSerializer()
    registered_device = DeviceSerializer()


class BoatSetupSerializer(serpy.Serializer):
    boat = BoatSerializer()
    registered_device = DeviceSerializer()


class RaceSerializer(serpy.Serializer):
    uuid = serpy.StrField()
    name = serpy.StrField()
    start_time = serpy.StrField()
    countdown_time = serpy.MethodField()
    race_state = serpy.IntField()

    course = CourseSerializer()

    boat_setups = serpy.MethodField()
    mark_setups = serpy.MethodField()

    race_state_reference = serpy.MethodField()

    def get_boat_setups(self, inst):
        q = BoatSetup.objects.filter(race=inst)
        return BoatSetupSerializer(q.all(), many=True).data

    def get_mark_setups(self, inst):
        q = MarkSetup.objects.filter(race=inst)
        return MarkSetupSerializer(q.all(), many=True).data

    def get_race_state_reference(self, inst):
        result = {}
        for val, label in inst.AllRaceStates.choices:
            result[label] = val
        return result

    def get_countdown_time(self, inst):
        return inst.countdown_time.total_seconds()
