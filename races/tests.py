import logging
import pathlib
from datetime import datetime
from time import sleep
from unittest.mock import patch

import numpy as np
import shapely
from django.conf import settings as django_settings
from django.contrib.gis.geos import Point
from django.utils import timezone
from django.utils.timezone import timedelta
from hypothesis import assume, given, note
from hypothesis import strategies as st
from hypothesis.extra.django import TestCase, from_model
from hypothesis.extra.numpy import arrays
from hypothesis.extra.pytz import timezones
from paho.mqtt.client import Client, MQTTv5

from races import models as m
from races import otle, packets
from races.utils import rotate_points

logger = logging.getLogger(__name__)


def rotation(r):
    return np.asarray([np.cos(r), -np.sin(r), np.sin(r), np.cos(r)]).reshape(
        2, 2
    )


def headings():
    return st.floats(
        min_value=0,
        max_value=360,
        exclude_max=True,
        allow_infinity=False,
        allow_nan=False,
        width=32,
    )


def physical_floats():
    return st.floats(
        allow_nan=False,
        allow_infinity=False,
        max_value=40.075 * 10**6,  # circumference of Earth
        min_value=-40.075 * 10**6,  # circumference of Earth
    )


def regattas():
    return from_model(m.Regatta)


def boat_outlines():
    simulation_dir = pathlib.Path("simulation")
    raw_measurements = np.loadtxt(
        simulation_dir / "raw_measurements.txt", delimiter=","
    )
    centered_measurements = raw_measurements - [0, 1]
    translated_measurements = centered_measurements - [-939, 0]
    scale_factor = 3.937 / np.abs(
        translated_measurements[0, 0] - translated_measurements[-1, 0]
    )
    scaled_measurements = translated_measurements * scale_factor
    # Orient so boat is facing "North"
    R = rotation(np.pi / 2)
    rotated_measurements = np.matmul(scaled_measurements, R)
    mirrored_measurements = np.vstack(
        (rotated_measurements, rotated_measurements * [-1, 1])
    )

    measurement_ring = np.concatenate(
        (
            mirrored_measurements,
            [mirrored_measurements[0]],
        ),
        axis=0,
    )
    return from_model(
        m.BoatOutline, outline=st.just(measurement_ring.tolist())
    )


def boats():
    return from_model(m.Boat, outline=boat_outlines())


def devices():
    return from_model(
        m.Device,
        id=st.none(),
        hardware_id=st.integers(min_value=0, max_value=2**16 - 1).map(str),
    )


def points(
    latitudes=st.floats(
        min_value=-66.91, max_value=49.38, exclude_min=True, exclude_max=True
    ),
    longitudes=st.floats(min_value=-124.79, max_value=24.41),
):
    return st.builds(
        Point,
        longitudes,
        latitudes,
        srid=st.just(4326),
    )


def marks():
    return from_model(
        m.Mark,
        id=st.none(),
        static_position=points(),
    )


def lines():
    return from_model(
        m.Line,
        pin=marks(),
        rc_boat=marks(),
        pin_device=devices(),
        rc_device=devices(),
    )


def courses():
    return from_model(m.Course, starting_line=lines(), finish_line=lines())


def generate_layout(course):
    return st.lists(
        from_model(m.MarkCourseOrder, course=st.just(course), mark=marks())
    ).map(lambda _: course)


def courses_w_marks():
    return courses().flatmap(generate_layout)


def point_in_polygon(polygon):
    ring = polygon[0]
    coordinates = list(ring)
    shapely_poly = shapely.Polygon(coordinates)

    if any(np.isnan(bound) for bound in shapely_poly.bounds):
        raise ValueError(
            f"Invalid bounds {shapely_poly.bounds} for "
            f"coordinates {coordinates} for "
            f"{ring} in {polygon}"
        )

    min_x, min_y, max_x, max_y = shapely_poly.bounds
    return st.builds(
        shapely.Point,
        st.floats(
            min_value=min_x,
            max_value=max_x,
            allow_infinity=False,
            allow_nan=False,
        ),
        st.floats(
            min_value=min_y,
            max_value=max_y,
            allow_infinity=False,
            allow_nan=False,
        ),
    ).filter(lambda p: shapely_poly.contains(p))


def defined_float(**kwargs):
    return st.floats(**kwargs, allow_infinity=False, allow_nan=False)


def position_packets():
    return st.builds(
        packets.PositionPacket,
        nonce=st.integers(min_value=0, max_value=2**16 - 1),
        time=st.datetimes(
            min_value=packets.EPOCH_2022.replace(day=2, tzinfo=None),
            max_value=datetime(5506, 1, 1, 0, 0),
            timezones=timezones(),
        ),
        latitude=defined_float(
            min_value=packets.MIN_LATITUDE, max_value=packets.MAX_LATITUDE
        ),
        longitude=defined_float(
            min_value=packets.MIN_LONGITUDE, max_value=packets.MAX_LONGITUDE
        ),
        error_2d=st.integers(min_value=0, max_value=255),
        magnetic_heading=defined_float(
            min_value=packets.MIN_HEADING, max_value=packets.MAX_HEADING
        ),
        true_heading=defined_float(
            min_value=packets.MIN_HEADING, max_value=packets.MAX_HEADING
        ),
    )


def device_logs(marker_strategy):
    return from_model(
        m.DevicePositionLog, device=marker_strategy, coordinate=points()
    )


def boat_setups(
    race_strategy=None,
    boat_strategy=None,
    side_side_offset=st.just(0),
    fore_aft_offset=st.just(0),
    device_strategy=None,
):
    race_strategy = races() if race_strategy is None else race_strategy
    boat_strategy = boats() if boat_strategy is None else boat_strategy
    device_strategy = devices() if device_strategy is None else device_strategy

    return from_model(
        m.BoatSetup,
        race=race_strategy,
        boat=boat_strategy,
        side_side_offset=side_side_offset,
        fore_aft_offset=fore_aft_offset,
        registered_device=device_strategy,
    )


def races(race_state=None, start_time=None):
    if race_state is None:
        race_state = st.sampled_from([x for x, _ in m.Race.RaceState.choices])
    if start_time is None:
        start_time = st.datetimes(timezones=timezones())

    return from_model(
        m.Race,
        parent_regatta=regattas(),
        course=courses(),
        start_time=start_time,
        countdown_time=st.timedeltas(max_value=timedelta(days=365)),
        _race_state=race_state,
    )


def generate_with_race(race):
    return st.lists(boat_setups(race_strategy=st.just(race)), min_size=1).map(
        lambda _: race
    )


def races_w_setups():
    return races().flatmap(generate_with_race)


class PacketSerialization(TestCase):
    @given(position_packets())
    def test_serialization(self, packet):
        packed = packet.pack()
        instance = packets.PositionPacket.from_binary_packet(packed)
        fields = packets.PositionPacket.fields
        try:
            for field in fields:
                ref = getattr(packet, field)
                chk = getattr(instance, field)
                self.assertEqual(ref, chk)
        except AssertionError:
            print(packed)
            raise


class MQTTRelayTests(TestCase):
    def test_mqtt_connection(self):
        host = django_settings.MQTT_HOST
        port = int(django_settings.MQTT_PORT)
        client = Client("testclient", transport="websockets", protocol=MQTTv5)
        client.username_pw_set(
            django_settings.MQTT_USERNAME, django_settings.MQTT_PASSWORD
        )

        def on_connect(client, userdata, flags, rc, props=None):
            self.assertEqual(rc, 0)

        client.on_connect = on_connect
        client.connect(host, port)
        client.loop_start()
        sleep(1)
        self.assertTrue(client.is_connected())


def _write_parameter(name, param, fout):
    msg = f"{name};{str(param)}\n"
    fout.write(msg)


def _report_failure(chosen_point, ref_outline, line):
    with open("last_failure.txt", "wt") as fout:
        _write_parameter("chosen_point", chosen_point, fout)
        _write_parameter("desired_position", line.desired_position, fout)
        _write_parameter("ref_outline", ref_outline, fout)
        _write_parameter("geo_line", line.geo_line, fout)
        _write_parameter("outline", line.outline, fout)
        _write_parameter("offset_point", line.offset_point, fout)
        _write_parameter("new_centroid", line.new_centroid, fout)

    return f"{line.offset_point} != {line.outline.centroid}"


class OTLEDetectionTests(TestCase):
    @given(
        arrays(
            float,
            (2, 2),
            elements=st.floats(allow_nan=False, allow_infinity=False),
        )
    )
    def test_line_heading_calc(self, line):
        angle = otle.true_heading_of_line(line)
        diff = line[1] - line[0]

        easting_angle = np.degrees(np.arctan2(diff[1], diff[0]))

        self.assertAlmostEqual(angle, 90 - easting_angle)

    @given(
        arrays(
            np.longdouble,
            (10, 2),
            elements=physical_floats(),
        ),
        arrays(
            float,
            2,
            elements=physical_floats(),
        ),
    )
    def test_rotation(self, array, rotation_point):
        check = rotate_points(array, 180, origin=rotation_point)
        note(check)
        check = rotate_points(np.copy(check), 180, origin=rotation_point)
        note(check)
        self.assertTrue(
            np.allclose(
                rotate_points(array, 360, origin=rotation_point),
                check,
                rtol=float(django_settings.ALLOWED_ERROR),
            )
        )

    @given(
        boat_outlines(),
        arrays(np.longdouble, (2, 2), elements=physical_floats()),
    )
    def test_get_distance(self, boat_outline, line):
        geometry = np.array(boat_outline.outline)
        assume(not np.alltrue(line[0] == line[1]))
        assume(line[0][0] != line[1][0])
        rotated_geometry, check_line = otle.orient_line_and_outline(
            geometry,
            line,
            0,
            0,
            0,
        )
        distances = otle.calculate_distances(rotated_geometry, check_line)
        for idx in range(geometry.shape[0]):
            point = geometry[idx]

            # y = mx + b
            # aX + bY + C = 0
            # (-m)X + (1)Y - b = 0
            slope = (line[1][1] - line[0][1]) / (line[1][0] - line[0][0])
            intersept = line[0][1] - slope * line[0][0]

            # distance to line in standard form
            a = -slope
            b = 1
            c = -intersept

            nominator = np.abs(a * point[0] + b * point[1] + c)
            denominator = np.sqrt(a**2 + b**2)
            ref_distance = nominator / denominator
            check_distance = distances[idx]

            self.assertAlmostEqual(np.abs(check_distance), ref_distance)


class RaceStateCaseTests(TestCase):
    def test_congruent_states(self):
        self.assertEqual(m.Race.RaceState.SETUP, m.Race.AllRaceStates.SETUP)
        self.assertEqual(
            m.Race.RaceState.FINISHED, m.Race.AllRaceStates.FINISHED
        )

        other_states = [
            m.Race.RaceState.SETUP,
            m.Race.RaceState.RUNNING,
            m.Race.RaceState.FINISHED,
        ]

        for state in other_states:
            self.assertNotEqual(m.Race.AllRaceStates.COMMENCING, state)
            self.assertNotEqual(m.Race.AllRaceStates.ONGOING, state)

    @given(
        races(
            race_state=st.just(m.Race.RaceState.RUNNING),
            start_time=st.datetimes(
                min_value=timezone.now().replace(tzinfo=None)
                + timedelta(days=1),
                timezones=timezones(),
            ),
        )
    )
    def test_commencing(self, race):
        self.assertEqual(race.race_state, m.Race.AllRaceStates.COMMENCING)

    @given(races(race_state=st.just(m.Race.RaceState.RUNNING)))
    def test_ongoing_vs_commencing(self, race):
        note(f"{timezone.now()} + {race.countdown_time} = {race.start_time}")
        if race.race_state == m.Race.AllRaceStates.ONGOING:
            self.assertGreaterEqual(timezone.now(), race.start_time)
        elif race.race_state == m.Race.AllRaceStates.COMMENCING:
            self.assertLess(timezone.now(), race.start_time)

    @given(races(race_state=st.just(m.Race.RaceState.SETUP)))
    def test_race_start(self, race):
        assume(race.countdown_time.total_seconds() > 10)
        self.assertFalse(race.is_commencing)

        note(f"{timezone.now()} + {race.countdown_time} = {race.start_time}")
        race.start()

        note(f"{timezone.now()} + {race.countdown_time} = {race.start_time}")
        self.assertNotEqual(race.race_state, m.Race.RaceState.SETUP)
        self.assertTrue(race.is_commencing)
        now = timezone.now()

        with patch(
            "django.utils.timezone.now", lambda: now + race.countdown_time
        ):
            self.assertTrue(race.is_ongoing)
