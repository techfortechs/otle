import json
import logging
import multiprocessing as mp
import random
from time import sleep

import numpy as np
import pyproj
from django import db
from django.conf import settings
from django.utils import timezone

from races import models as m
from races.packets import PositionPacket
from races.utils import get_magnetic_offset

logger = logging.getLogger(__name__)


class DeviceSimulator(mp.Process):
    def __init__(
        self, init_lat, init_lon, init_heading, hardware_id, poll_rate=0.2
    ):
        super().__init__(daemon=True)
        self.latitude_0 = init_lat
        self.longitude_0 = init_lon
        self.heading_0 = init_heading
        self.hardware_id = hardware_id
        self.poll_rate = poll_rate
        self.nonce = 0

        self.latitude = self.latitude_0
        self.longitude = self.longitude_0
        self.heading = self.heading_0
        self.db_context = {}
        self.geodesic = pyproj.Geod(ellps="WGS84")

        self.mqtt_client = None

    def get_next_position(self):
        raise NotImplementedError

    def get_random_position_nearby(self, ref_lat, ref_lon, step):
        noise_heading = random.uniform(0, 359)
        lon, lat, _ = self.geodesic.fwd(ref_lon, ref_lat, noise_heading, step)
        return lat, lon

    def make_on_message(self):
        def on_message(client, userdata, msg):
            return

        return on_message

    def make_on_connect(self):
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                logger.info(f"Client {client} connected to MQTT broker.")
            else:
                logger.error(f"Client {client} could not connect. RC: {rc}")

        return on_connect

    def submit_packet(self):

        magnetic_offset = np.radians(
            get_magnetic_offset(self.latitude, self.longitude)
        )
        clamped_heading = np.degrees(self.heading + magnetic_offset) % 360.0

        packet = PositionPacket(
            nonce=self.nonce % (2 ** (8 * 2)),
            latitude=self.latitude,
            longitude=self.longitude,
            time=timezone.now(),
            error_2d=0,
            magnetic_heading=clamped_heading,
            true_heading=clamped_heading,
        )
        self.mqtt_client.publish(
            f"{self.hardware_id}/position",
            payload=packet.pack(),
            qos=0,
            retain=False,
        )
        self.nonce += 1

    def connect_to_mqtt(self):
        from paho.mqtt import client as mqtt_client

        client = mqtt_client.Client(
            f"py_{self.hardware_id}", transport="websockets"
        )
        client.on_connect = self.make_on_connect()
        client.on_message = self.make_on_message()

        client.username_pw_set(settings.MQTT_USERNAME, settings.MQTT_PASSWORD)
        client.connect(host=settings.MQTT_HOST, port=int(settings.MQTT_PORT))
        client.loop_start()
        self.mqtt_client = client
        return client

    def load_context_from_db(self):
        pass

    def run(self):
        db.connection.close()
        self.load_context_from_db()
        self.connect_to_mqtt()
        while True:
            self.get_next_position()
            self.submit_packet()
            sleep(self.poll_rate)


class BouySimulator(DeviceSimulator):
    def __init__(self, race_uuid, device, mark, **device_kwargs):
        hardware_id = device.hardware_id
        lat = mark.static_position.y
        lon = mark.static_position.x

        super().__init__(
            hardware_id=hardware_id,
            init_lat=lat,
            init_lon=lon,
            init_heading=0,
            **device_kwargs,
        )

        self.race_uuid = race_uuid

    def get_next_position(self):
        noise_heading = random.uniform(0, 359)
        step = random.uniform(0, 1)

        self.longitude, self.latitude, _ = self.geodesic.fwd(
            self.longitude_0, self.latitude_0, noise_heading, step
        )


class BoatSimulator(DeviceSimulator):
    def __init__(self, race_uuid, boat_id, **device_kwargs):
        setup = m.BoatSetup.objects.get(race__pk=race_uuid, boat_id=boat_id)

        super().__init__(
            hardware_id=setup.registered_device.hardware_id,
            init_lat=0,
            init_lon=0,
            init_heading=0,
            **device_kwargs,
        )
        lat, lon = self.get_random_position_nearby(
            setup.race.course.starting_line.pin.static_position.y,
            setup.race.course.starting_line.pin.static_position.x,
            100,
        )
        self.latitude = lat
        self.longitude = lon
        self.latitude_0 = lat
        self.longitude_0 = lon
        self.goal_step = 0
        self.speed = 2
        self.max_turn_speed = np.pi / 8
        self.setup_id = setup.id

        self.goal_device = setup.race.course.starting_line.pin_device
        self.position_cache = {}

    def get_next_position(self):
        self.longitude, self.latitude, _ = self.geodesic.fwd(
            self.longitude,
            self.latitude,
            np.degrees(self.heading) % 360.0,
            self.speed * self.poll_rate,
        )

    def make_on_connect(self):
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                logger.info(f"Client {client} connected to MQTT broker.")
                client.subscribe("+/status")
            else:
                logger.error(f"Client {client} could not connect. RC: {rc}")

        return on_connect

    def make_on_message(self):
        def on_message(client, userdata, msg):
            hardware_id, _ = msg.topic.split("/")
            self.position_cache[hardware_id] = json.loads(msg.payload)

        return on_message

    def get_sensor_reading(self, offset):
        sensor_orientation = self.heading + offset
        s_long, s_lat, _ = self.geodesic.fwd(
            self.longitude,
            self.latitude,
            np.degrees(sensor_orientation) % 360.0,
            1,  # Meter
        )
        try:
            status = self.position_cache[self.goal_device.hardware_id]
        except KeyError:
            # Return no cost, boat can't see it
            return 0
        distance = self.geodesic.line_length(
            [s_long, status["longitude"]], [s_lat, status["latitude"]]
        )
        return distance

    def get_next_heading(self):
        left_reading = self.get_sensor_reading(-np.pi / 4)
        right_reading = self.get_sensor_reading(np.pi / 4)

        if left_reading < right_reading:
            # Goal is to the left
            self.heading -= self.max_turn_speed
        else:
            # Goal is to the right
            self.heading += self.max_turn_speed

    def run(self):
        db.connection.close()
        self.load_context_from_db()
        self.connect_to_mqtt()

        while True:
            self.get_next_heading()
            self.get_next_position()
            self.submit_packet()
            sleep(self.poll_rate)
