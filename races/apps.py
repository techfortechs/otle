import logging
from multiprocessing import Manager, Queue

from django.apps import AppConfig
from django.core.exceptions import FieldError
from django.db import ProgrammingError
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

logger = logging.getLogger(__name__)


class RacesConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "races"

    def ready(self):

        import sys

        if "runserver" not in sys.argv:
            return

        try:
            from django.conf import settings

            from races import mqtt_relay
            from races.models import Race
            from races.packets import PacketQueueRunner

            manager = Manager()
            queue = Queue(maxsize=10**6)
            status_cache = manager.dict()
            race_cache = manager.dict()
            outline_cache = manager.dict()

            otle_cache = manager.dict()
            finish_cache = manager.dict()

            # Prepopulate cache with database state
            for row in Race.get_race_summaries().all():
                row["race_state"] = row.pop("_race_state")
                row["starting_line"] = {
                    "pin_device": row.pop("start_line_pin_device"),
                    "rc_device": row.pop("start_line_rc_device"),
                    "is_rc_on_right": row.pop("is_rc_on_right_start_line"),
                }
                row["finish_line"] = {
                    "pin_device": row.pop("finish_line_pin_device"),
                    "rc_device": row.pop("finish_line_rc_device"),
                    "is_rc_on_right": row.pop("is_rc_on_right_finish_line"),
                }
                race_cache[row["pk"]] = row

            # Upon saving any race, update the race
            def race_subscription(sender, **kwargs):
                race = kwargs["instance"]
                if race is not None:
                    summary = {}
                    summary["pk"] = race.pk
                    summary["race_state"] = race.race_state
                    summary["boat_devices"] = set()
                    summary["start_time"] = race.start_time
                    for setup in race.boat_setups.all():
                        summary["boat_devices"].add(
                            setup.registered_device.hardware_id
                        )
                    summary["mark_devices"] = set()
                    for setup in race.mark_setups.all():
                        summary["mark_devices"].add(
                            setup.registered_device.hardware_id
                        )
                    summary["starting_line"] = {
                        "pin_device": (
                            race.course.starting_line.pin_device.hardware_id
                        ),
                        "rc_device": (
                            race.course.starting_line.rc_device.hardware_id
                        ),
                        "is_rc_on_right": (
                            race.course.starting_line.is_rc_on_right
                        ),
                    }
                    summary["finish_line"] = {
                        "pin_device": (
                            race.course.finish_line.pin_device.hardware_id
                        ),
                        "rc_device": (
                            race.course.finish_line.rc_device.hardware_id
                        ),
                        "is_rc_on_right": (
                            race.course.finish_line.is_rc_on_right
                        ),
                    }
                    summary["last_refreshed_on"] = timezone.now()

                    race_cache[summary["pk"]] = summary

            receiver(post_save, sender=Race)(race_subscription)

            runners = [
                PacketQueueRunner(
                    queue,
                    status_cache,
                    race_cache,
                    outline_cache,
                    otle_cache,
                    finish_cache,
                )
                for _ in range(1)
            ]
            [r.start() for r in runners]
            print("in apps_py; starting mqtt_relay.MQTTRelay")
            relay = mqtt_relay.MQTTRelay(
                settings.MQTT_HOST,
                int(settings.MQTT_PORT),
                mqtt_relay.on_connect,
                mqtt_relay.make_message(queue),
                daemon=True,
            )
            relay.start()
        except (ProgrammingError, FieldError) as e:
            logger.error(
                "MQTT Relay unable to start, is the database alive or "
                f"is there a migration pending? Reason for error {e}"
            )
