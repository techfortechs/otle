from datetime import timedelta

import logging

import time
import matplotlib.pyplot as plt
import numpy as np
from django.conf import settings
from django.contrib.gis.db.models.functions import (
    Distance,
    Transform,
    Translate,
)
from django.db.models import OuterRef, Subquery, Value
from django.utils.timezone import now

from races import models as m
from races.functions import (
    GetX,
    GetY,
    MakeLine,
    MakePoint,
    Radians,
    Rotate,
    SetSRID,
)

from races.utils import rotate_points

logger = logging.getLogger(__name__)
logging.getLogger('matplotlib.font_manager').disabled = True
logging.getLogger('matplotlib.<module>').disabled = True
logging.getLogger('matplotlib.wrapper').disabled = True

#EUCLIDEAN_SRID = settings.EUCLIDEAN_SRID
EUCLIDEAN_SRID = 'Error'  # we should not be using this, and if we do I want an error
GLOBAL_SRID = settings.GLOBAL_SRID


def place_boat_setups_at(latitude, longitude, heading):
    q = m.BoatSetup.objects.annotate(
        offset=SetSRID(
            MakePoint("centerline_offset", "bow_offset"), EUCLIDEAN_SRID
        ),
        desired_position=Transform(
            SetSRID(MakePoint(Value(longitude), Value(latitude)), GLOBAL_SRID),
            EUCLIDEAN_SRID,
        ),
    )

    q = q.annotate(
        outline=Transform(
            Translate(
                Rotate(
                    Transform("boat__outline__outline", EUCLIDEAN_SRID),
                    Radians(Value(-heading)),
                    "offset",
                ),
                GetX("desired_position") - GetX("offset"),
                GetY("desired_position") - GetY("offset"),
            ),
            GLOBAL_SRID,
        )
    )
    return q


def place_outlines_at(
    latitude, longitude, heading, fore_aft_offset, side_side_offset
):
    logger.warning("in place_outlines_at - a definition we hope to abandon")
    q = m.BoatOutline.objects.annotate(
        offset=SetSRID(
            MakePoint(Value(fore_aft_offset), Value(side_side_offset)),
            EUCLIDEAN_SRID,
        ),
        desired_position=Transform(
            SetSRID(MakePoint(Value(longitude), Value(latitude)), GLOBAL_SRID),
            EUCLIDEAN_SRID,
        ),
    )

    q = q.annotate(
        transposed_outline=Transform(
            Translate(
                Rotate(
                    Transform("outline", EUCLIDEAN_SRID),
                    Radians(Value(-heading)),
                    "offset",
                ),
                GetX("desired_position") - GetX("offset"),
                GetY("desired_position") - GetY("offset"),
            ),
            GLOBAL_SRID,
        )
    )
    return q


def latest_line_position(time_lookback_threshold=100):
    time_threshold = now() - timedelta(seconds=time_lookback_threshold)
    latest_pin_coordinate = m.DevicePositionLog.objects.filter(
        device=OuterRef("pin_device"), datetime__gt=time_threshold
    )
    latest_rc_coordinate = m.DevicePositionLog.objects.filter(
        device=OuterRef("rc_device"), datetime__gt=time_threshold
    )
    #logger.info(f'latest_pin: {latest_pin_coordinate}  latest_rc: {latest_rc_coordinate}')
    q = m.Line.objects.annotate(
        latest_pin_coordinate=Subquery(
            latest_pin_coordinate.values("coordinate")[:1]
        ),
        latest_rc_coordinate=Subquery(
            latest_rc_coordinate.values("coordinate")[:1]
        ),
    )
    q = q.annotate(
        geo_line=MakeLine("latest_pin_coordinate", "latest_rc_coordinate")
    )
    return q


def return_line_crossing_q(hardware_id, latitude, longitude, heading):
    outline = (
        place_boat_setups_at(latitude, longitude, heading).filter(
            registered_device__hardware_id=hardware_id
        )
        # .filter(race__race_state=m.Race.RaceState.COMMENCING)
        .get()
    )
    boat_geometry = outline.outline

    line_q = latest_line_position().annotate(
        distance=Distance("geo_line", boat_geometry)
    )

    return line_q


def orient_boat_outline(
    standard_outline_array, x_offset, y_offset, orientation
):
    """
    Orient a boat outline to be rotated according to the specified orientation
    angle and translate the outline by the specified X and Y offsets.

    Parameters
    ----------
    standard_outline_array: np.ndarray((N, 2))
        The 2D outline array. The columns of this array should be the spatial
        axis of the outline.

    x_offset: float
        The meter offset in the X dimension (added)
    y_offset: float
        The meter offset in the Y dimension (added)

    orientation: float
        The degree angle to rotate the outline (CW)
    """
    rotated_outline = rotate_points(
        np.copy(standard_outline_array), orientation
    )
    rotated_outline[:, 0] = rotated_outline[:, 0] + x_offset
    rotated_outline[:, 1] = rotated_outline[:, 1] + y_offset

    return rotated_outline


def standardized_line(line_array):
    """
    Center the given line on the first element of the array and rotate the
    line such that the Y axis is flattened.

    Returns
    -------
    rotated_other: (x, y)
        The rotated and translated line point. The origin of the line will
        be centered on (0, 0)
    line_angle:
        The degree rotation used to orient the line such that it lies flat
        on the Y axis.
    vector:
        The vector used to translate the origin to (0, 0) (added).
    """
    origin = line_array[0]
    other = line_array[1]

    translated_other = other - origin
    ref = np.pi / 2
    line_angle = np.degrees(
        ref - np.arctan2(translated_other[1], translated_other[0])
    )
    rotated_other = rotate_points(translated_other, -line_angle)
    return rotated_other + origin, line_angle, -1 * origin


def true_heading_of_line(line_array):
    origin = line_array[0]
    other = line_array[1]

    diff = other - origin
    ref = np.pi / 2
    line_angle = ref - np.arctan2(diff[1], diff[0])

    return np.degrees(line_angle)


def center_line_and_outline(
    outline, line, easting, northing, magnetic_heading
):
    rotated_rc, line_angle, origin_vector = standardized_line(line)
    oriented_outline = orient_boat_outline(
        outline, easting, northing, magnetic_heading - line_angle
    )

    return rotated_rc, oriented_outline, line_angle


def orient_line_and_outline(
    outline, line, easting, northing, magnetic_heading
):

    # fig, ax = plt.subplots(1, figsize = (8,8))
    # # plot the unrotated boat sensor location   looks good
    # ax.scatter(outline[:,0], outline[:,1], c ='b')
    # ax.set_aspect('equal')
    # plt.show()
    
    
    # Rotate and place the boat in UTM space
    oriented_boat_outline = rotate_points(np.copy(outline), magnetic_heading)
    eastings = oriented_boat_outline[:, 0] + easting
    northings = oriented_boat_outline[:, 1] + northing
    placed_outline = np.stack((eastings, northings), axis=1)

    # logger.info(f"line: {str(line)}")
    # fig, ax = plt.subplots(1, figsize = (8,8))
    #  # plot the unrotated starting line
    # ax.plot(line[:,0],line[:,1], c='b', markersize=2)
    # ax.set_title("orient_line_and_outline")
    # # plot the unrotated boat sensor location
    # ax.scatter(easting,northing, c ='b')
    # # plot the oriented boat outline
    # ax.scatter(placed_outline[:,0], placed_outline[:,1], c ='b')  
    # ax.set_aspect('equal')
    # plt.show()
    
    
    # Determine line orientation
    line_orientation = true_heading_of_line(line)

    # Rotate the line such that it rests on the Northing axis
    line_check = rotate_points(
        np.copy(line[1]),
        -line_orientation,
        origin=np.copy(line[0]),
    )
    
    # Rotate the boat location
    rot_boat_pos = rotate_points(
        np.copy([easting, northing]),
        -line_orientation,
        origin=line[0],
    )

    # Rotate the outline once again in respect to the line rotation
    reference_outline = rotate_points(
        np.copy(placed_outline), -line_orientation, origin=line[0]
    )
    
    #   logger.info(f'qdistance: {line_check[0] - min(reference_outline[:,0]) } ')
    
    # fig, ax = plt.subplots(1, figsize = (8,8))
    # plt.ion()
    #  # plot the unrotated starting line
    # #logger.info(f' at the top line[:,0]: {line[:,0]}   line[:,1]: {line[:,1]} ')
    # ax.plot(line[:,0],line[:,1], c='b', markersize=2)
    # ax.scatter(line[0,0],line[0,1], s=[128], c =['b'], marker = '$L$')
    # ax.scatter(line[1,0],line[1,1], s=[128], c =['b'], marker = '$R$')
    # # plot the unrotated boat sensor location
    # ax.scatter(easting,northing, c ='b')
    # # plot the unrotated boat outline
    # ax.scatter(placed_outline[:,0], placed_outline[:,1], c ='b')                       
    # # plot the rotated starting line
    # #logger.info(f' in the middle line: {line}   line_check: {line_check} ')
    # ax.plot( [line[0,0], line_check[0]], [line[0,1], line_check[1]] ,c='r')
    # ax.scatter(line_check[0],line_check[1], s=[128], c =['r'], marker = '$R$')
    # # # plot the rotated boat sensor location
    # ax.scatter(rot_boat_pos[0],rot_boat_pos[1], c =['r'])
    # # # plot the rotated boat outline
    # ax.scatter(reference_outline[:,0], reference_outline[:,1], c ='r')
    
    # ax.set_aspect('equal')
    # plt.show()
    # plt.pause(5)
    # plt.close()
 
 
    
    return reference_outline, line_check 
 
 
 
 # used by DEBUG page
def calculate_distances(oriented_outline, oriented_line_point):
    ref_axis = oriented_line_point[0]
    return ref_axis - oriented_outline[:, 0]
