import json
import logging

import numpy as np
import utm
from django.http import JsonResponse
from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from races import otle
from races.models import (
    BoatOutline,
    Device,
    DevicePositionLog,
    Line,
    Race,
    Regatta,
)
from races.serializers import (
    BoatOutlineSerializer,
    DeviceSerializer,
    LineModelSerializer,
    RaceSerializer,
    RegattaSerializer,
)
from races.utils import rotate_points

logger = logging.getLogger(__name__)


def list_regattas(response):
    regattas = RegattaSerializer(Regatta.objects.all(), many=True)
    return JsonResponse(regattas.data, safe=False)


def get_regatta(response, uuid):
    regatta = Regatta.objects.get(uuid=uuid)
    serialized = RegattaSerializer(regatta).data
    races = []
    q = Regatta.objects.filter(uuid=uuid)
    for uuid, name in q.values_list("races__uuid", "races__name"):
        races.append({"uuid": uuid, "name": name})

    serialized["races"] = races
    return JsonResponse(serialized)


def list_devices(response):
    q = Device.objects.all()
    s = DeviceSerializer(q.all(), many=True)
    result = s.data
    return JsonResponse(result, safe=False)


def list_lines(response):
    q = Line.objects

    s = LineModelSerializer(q.all(), many=True)
    return JsonResponse(s.data, safe=False)


def get_race(response, uuid):
    race = Race.objects.get(uuid=uuid)
    return JsonResponse(RaceSerializer(race).data)


def get_boat_devices(response, slug):
    q = Regatta.object.values_list("boat_devices__pk", flat=True).filter(
        uuid=slug
    )
    return JsonResponse(q.all(), status=200)


def get_marker_devices(response, slug):
    q = Regatta.object.values_list("mark_devices__pk", flat=True).filter(
        uuid=slug
    )
    return JsonResponse(q.all(), status=200)


def start_race(response, uuid):
    race = Race.objects.get(pk=uuid)
    race.start()

    return JsonResponse(RaceSerializer(race).data, status=200)


def restart_race(response, uuid):
    race = Race.objects.get(pk=uuid)
    race.restart()

    return JsonResponse(RaceSerializer(race).data, status=200)


def finish_race(response, uuid):
    race = Race.objects.get(pk=uuid)
    race.finish()

    return JsonResponse(RaceSerializer(race).data, status=200)


def DEBUG_STATE(response):
    lines = Line.objects.all()
    devices = Device.objects.all()
    outlines = BoatOutline.objects.all()

    result = {
        "lines": LineModelSerializer(lines, many=True).data,
        "devices": DeviceSerializer(devices, many=True).data,
        "outlines": BoatOutlineSerializer(outlines, many=True).data,
    }

    return JsonResponse(result, status=200)


@csrf_exempt
def DEBUG_VIEW(response):
    data = json.loads(response.body)
    log = (
        DevicePositionLog.objects.filter(device_id=data["device_id"])
        .order_by("-datetime")
        .first()
    )
    ref_e, ref_n, num, letter = utm.from_latlon(
        log.coordinate.y, log.coordinate.x
    )
    outline = np.array(BoatOutline.objects.get(pk=data["outline_id"]).outline)
    line = otle.latest_line_position().get(pk=data["line_id"]).geo_line

    is_rc_on_right = Line.objects.get(pk=data["line_id"]).is_rc_on_right

    origin_e, origin_n, _, _ = utm.from_latlon(line[0][1], line[0][0])
    other_e, other_n, _, _ = utm.from_latlon(line[1][1], line[1][0])
    geo_line = np.array(line)
    if is_rc_on_right:
        line = np.array([[origin_e, origin_n], [other_e, other_n]])
    else:
        line = np.array([[other_e, other_n], [origin_e, origin_n]])

    oriented_outline, line_check = otle.orient_line_and_outline(
        outline, line, ref_e, ref_n, log.true_heading
    )

    distances = otle.calculate_distances(oriented_outline, line_check)

    # Undo line orientation
    physical_boat = rotate_points(
        oriented_outline, otle.true_heading_of_line(line), origin=line[0]
    )
    lat, lon = utm.to_latlon(
        physical_boat[:, 0], physical_boat[:, 1], num, letter
    )
    boat = np.stack((lon, lat), axis=1)

    value = {
        "outline": boat.tolist(),
        "line": geo_line.tolist(),
        "distance": min(distances),
    }
    return JsonResponse(value)


apipatterns = [
    path("list_regattas", list_regattas, name="list-regattas-api"),
    path("list_devices", list_devices, name="list-devices"),
    path("list_lines", list_lines, name="list-lines"),
    path("debug_state", DEBUG_STATE, name="DEBUG-STATE"),
    path("DEBUG", DEBUG_VIEW, name="DEBUG"),
    path("race/<uuid:uuid>", get_race, name="race-detail-api"),
    path("race/<uuid:uuid>/start", start_race),
    path("race/<uuid:uuid>/restart", restart_race),
    path("race/<uuid:uuid>/finish", finish_race),
    path("regatta/<uuid:uuid>", get_regatta, name="regatta-detail-api"),
]
