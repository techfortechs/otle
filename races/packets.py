import json
import logging
import struct
from collections import defaultdict, deque
from datetime import datetime, timedelta
from multiprocessing import Process

import time
import matplotlib.pyplot as plt
import numpy as np
import paho.mqtt.client as mqtt
import pytz
import utm
from django import db
from django.conf import settings
from django.contrib.gis.geos import Point
from django.core.serializers.json import DjangoJSONEncoder
from django.utils import timezone

from races import models as m
from races import otle
from races.utils import get_magnetic_offset

EPOCH_2022 = datetime(2022, 1, 1, tzinfo=pytz.utc)

logger = logging.getLogger(__name__)

MAX_INT_SIZE = 2**32 - 1

MAX_LATITUDE = 90
MIN_LATITUDE = -90

MAX_LONGITUDE = 180
MIN_LONGITUDE = -180

MIN_HEADING = 0
MAX_HEADING = 359


def rescale(value, value_min, value_max, range_min, range_max):
    """
    Scale `value` to the interval `[range_min, range_max]`.
    """
    if not (value_min <= value <= value_max):
        raise ValueError(
            f"Given value {value} does not reside in the given range:"
            f"[{value_min}, {value_max}]"
        )
    nominator = value - value_min
    denominator = value_max - value_min
    scaler = range_max - range_min
    offset = range_min
    return (nominator / denominator) * scaler + offset


class PositionPacket:
    fields = {
        "nonce",
        "time",
        "latitude",
        "longitude",
        "error_2d",
        "magnetic_heading",
        "true_heading",
    }

    def __init__(
        self,
        **kwargs,
    ):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def pack(self):
        packet = bytearray("otle", encoding="ascii")
        packet.extend(self.nonce.to_bytes(2, byteorder="big", signed=False))
        packet.extend(self._time.to_bytes(5, byteorder="big", signed=False))
        packet.extend(self._latitude.to_bytes(5, byteorder="big", signed=True))
        packet.extend(
            self._longitude.to_bytes(5, byteorder="big", signed=True)
        )
        packet.extend(self.error_2d.to_bytes(1, byteorder="big", signed=False))
        packet.extend(
            self._magnetic_heading.to_bytes(1, byteorder="big", signed=False)
        )
        packet.extend(
            self._true_heading.to_bytes(1, byteorder="big", signed=False)
        )
        return packet

    @classmethod
    def from_binary_packet(cls, binary_message):
        fmt_str = ">4sH5s5s5sBBB"
        try:
            data = struct.unpack(fmt_str, binary_message)
        except struct.error:
            print(binary_message)
            raise
        instance = cls(
            nonce=data[1],
            _time=int.from_bytes(data[2], "big", signed=False),
            _latitude=int.from_bytes(data[3], "big", signed=True),
            _longitude=int.from_bytes(data[4], "big", signed=True),
            error_2d=data[5],
            _magnetic_heading=data[6],
            _true_heading=data[7],
        )
        return instance

    @property
    def time(self):
        raise NotImplementedError

    @time.setter
    def time(self, datetime_inst):
        td = datetime_inst - EPOCH_2022
        self._time = int(td.total_seconds() * 10)

    @time.getter
    def time(self):
        td = timedelta(seconds=self._time / 10)
        return EPOCH_2022 + td

    # Latitude and Longitude values are float values as their precision
    # can be infinite. However, information in OTLE packets must be
    # integers. So we must rescale the coordinate ranges into the
    # representation-range of a 32-bit unsigned integer.
    @property
    def latitude(self):
        raise NotImplementedError

    @latitude.setter
    def latitude(self, value):
        # Rescale
        self._latitude = int(value * 10e8)

    @latitude.getter
    def latitude(self):
        # Undo scale
        return self._latitude / 10e8

    @property
    def longitude(self):
        raise NotImplementedError

    @longitude.setter
    def longitude(self, value):
        self._longitude = int(value * 10e8)

    @longitude.getter
    def longitude(self):
        return self._longitude / 10e8

    @property
    def magnetic_heading(self):
        raise NotImplementedError

    @magnetic_heading.setter
    def magnetic_heading(self, value):
        self._magnetic_heading = int(
            rescale(value, MIN_HEADING, MAX_HEADING, 0, 2**8 - 1)
        )

    @magnetic_heading.getter
    def magnetic_heading(self):
        return rescale(
            self._magnetic_heading, 0, 2**8, MIN_HEADING, MAX_HEADING
        )

    @property
    def true_heading(self):
        raise NotImplementedError

    @true_heading.setter
    def true_heading(self, value):
        self._true_heading = int(
            rescale(value, MIN_HEADING, MAX_HEADING, 0, 2**8 - 1)
        )

    @true_heading.getter
    def true_heading(self):
        return rescale(
            self._true_heading, 0, 2**8 - 1, MIN_HEADING, MAX_HEADING
        )


class PacketQueueRunner(Process):
    def __init__(
        self,
        packet_queue,
        status_cache,
        race_cache,
        outline_cache,
        otle_cache,
        finish_cache,
    ):
        super().__init__(daemon=True)
        self.packet_queue = packet_queue
        self.mqtt_client = None
        self.meta_telemetry = defaultdict(lambda: deque(maxlen=100))
        self.status_cache = status_cache
        self.outline_cache = outline_cache
        self.race_cache = race_cache
        self.otle_cache = otle_cache
        self.finish_cache = finish_cache

        self.log_buffer = []
        self.hardware_id_map = {}

    def save_packet_to_db(self, packet):
        hardware_id = packet["hardware_id"]
        try:
            device_id = self.hardware_id_map[hardware_id]
        except KeyError:
            device_id = (
                m.Device.objects.values_list("pk").get(hardware_id=hardware_id)
            )[0]
            self.hardware_id_map[hardware_id] = device_id

        log = m.DevicePositionLog(
            datetime=packet["datetime"],
            device_id=device_id,
            coordinate=Point(
                packet["longitude"],
                packet["latitude"],
            ),
            magnetic_heading=packet["magnetic_heading"],
            true_heading=packet["true_heading"],
            position_uncertainty=packet["error_2d"],
        )
        self.log_buffer.append(log)

        self.status_cache[hardware_id] = packet

    def flush_log_buffer(self):
        m.DevicePositionLog.objects.bulk_create(self.log_buffer)
        self.log_buffer = []

    def get_latest_device_status(self, hardware_id):
        try:
            return self.status_cache[hardware_id]
        except KeyError:
            q = m.DevicePositionLog.objects.order_by("-datetime")
            q = q.filter(device__hardware_id=hardware_id)
            result = q.first()
            if result is None:
                return None
            status = {
                "hardware_id": result.device.hardware_id,
                "datetime": result.datetime,
                "latitude": result.coordinate.y,
                "longitude": result.coordinate.x,
                "magnetic_heading": result.magnetic_heading,
                "true_heading": result.true_heading,
                "error_2d": result.position_uncertainty,
            }
            e, n, num, letter = utm.from_latlon(
                status["latitude"], status["longitude"]
            )
            status["easting"] = e
            status["northing"] = n
            status["zone_number"] = num
            status["zone_letter"] = letter
            self.status_cache[hardware_id] = status
            return status

    def get_outline_for(self, packet):
        hardware_id = packet["hardware_id"]
        try:
            outline = self.outline_cache[hardware_id]
        except KeyError:
            raw_outline = (
                m.BoatSetup.objects.filter(
                    registered_device__hardware_id=hardware_id,
                )
                .exclude(race___race_state=m.Race.RaceState.FINISHED)
                .values("boat__outline__outline")
                .first()["boat__outline__outline"]
            )
            outline = np.array(raw_outline)  # this should be the outline in meters at many n,e points about the fiducial point
            
            # get the offset of the device antenna for this boat (hw_id)
            side_side_offset= (
                m.BoatSetup.objects.filter(
                    registered_device__hardware_id=hardware_id,
                )
                .exclude(race___race_state=m.Race.RaceState.FINISHED)
                .values("side_side_offset")
                .first()["side_side_offset"]
            )
            fore_aft_offset = (
                m.BoatSetup.objects.filter(
                    registered_device__hardware_id=hardware_id,
                )
                .exclude(race___race_state=m.Race.RaceState.FINISHED)
                .values("fore_aft_offset")
                .first()["fore_aft_offset"]
            )      
            
            # move the outline based on the offset of the device - the fiducial point offset should be subtracetd from the outline
            outline[:,0] -= side_side_offset
            outline[:,1] -= fore_aft_offset
            
            # put the outline for this boat in the cache. *** Note - this will need to be updated if the offest changes***
            self.outline_cache[hardware_id] = outline

        return outline

    def get_line(self, line_def):
        # logger.info(f'line_def: {line_def} pin: {str(line_def["pin_device"])}  rc:  {str(line_def["rc_device"])}')
        # logger.info(f'pin_status: {self.status_cache[line_def["pin_device"]]}  rc_status:  {self.status_cache[line_def["rc_device"]]}')
        
        if (line_def["pin_device"] in self.status_cache):
            pin_status = self.status_cache[line_def["pin_device"]]
            pin_e = pin_status["easting"]
            pin_n = pin_status["northing"]
        else:
            logger.info(f'no pin data, line is {line_def}')
            pin_e = 100000.0
            pin_n = 100000.0
            
        if (line_def["rc_device"] in self.status_cache):
            rc_status = self.status_cache[line_def["rc_device"]]
            rc_e = rc_status["easting"]
            rc_n = rc_status["northing"]
        else:
            logger.info(f'no rc data, line is {line_def}')
            rc_e = 100000.0
            rc_n = 100000.0
            # logger.info(f'Not getting line device data;  keys: {self.status_cache.keys()} count: {bad_data_count} \
            #    status_cache: {self.status_cache}')
        
        if line_def["is_rc_on_right"]:
            return np.array(
                [
                    [pin_e, pin_n],
                    [rc_e, rc_n],
                ]
            )
        else:
            return np.array(
                [
                    [rc_e, rc_n],
                    [pin_e, pin_n],
                ]
            )

    def relay_packet(self, packet):
        hardware_id = packet.pop("hardware_id")
        #print("in relay_packet, sending packet from hw " + str(hardware_id))
        self.mqtt_client.publish(
            f"{hardware_id}/{settings.RELAY_PUBLISH_CHANNEL}",
            payload=json.dumps(packet, cls=DjangoJSONEncoder),
            qos=0,
            retain=False,
        )

    def calculate_distances(self, packet):
        loop_total = 0

        races = list(self.race_cache.values())
        outline = self.get_outline_for(packet)

        boat_statuses = {}
        for race in races:
            uuid = str(race["pk"])
            race_state = race["race_state"]
            my_start_time = race["start_time"]
                               
            t1 = timezone.now()
            
            #logger.info(f't1: {t1}  race time: {str((t1 - my_start_time).seconds)}')

            # Get relevant line and line properties based on race state

            # line = self.get_line(race["starting_line"])
            line = self.get_line(race["starting_line"])
            
            oriented_outline, line_check = otle.orient_line_and_outline(
                outline,
                line,
                packet["easting"],
                packet["northing"],
                packet["true_heading"],
            )
            
            
            distance = line_check[0] - min(oriented_outline[:,0])

            packet["line_distances"][uuid] = distance
            #logger.info(f'line_dstances: {packet["line_distances"]}')

            last_otle_state = self.otle_cache.get(packet["hardware_id"], False)
            boat_statuses[uuid] = "racing"
            is_otle = bool(distance > 0) & bool((my_start_time+timedelta(0.2))< t1) #  ocs at last datapoint before horn? 
            # need to address I flag rule race (rrs 30.1)       
             
            if last_otle_state is True:
                boat_statuses[uuid] = "otle"
            else:
                if is_otle and race_state == m.Race.AllRaceStates.COMMENCING:
                    # Transitioning into otle state, set appropriate
                    # tracking context
                    boat_statuses[uuid] = "otle"
                    self.otle_cache[packet["hardware_id"]] = True

            elapsed = (timezone.now() - t1).total_seconds()
            self.meta_telemetry["loop"].append(elapsed)
            loop_total += elapsed

        packet["status_in_race"] = boat_statuses
        self.meta_telemetry["loop"].append(loop_total)

    def process_packet(self, packet):
        easting, northing, zone_number, zone_letter = utm.from_latlon(
            packet["latitude"], packet["longitude"]
        )
        packet["easting"] = easting
        packet["northing"] = northing
        packet["zone_number"] = zone_number
        packet["zone_letter"] = zone_letter

        magnetic_dec = get_magnetic_offset(
            packet["latitude"], packet["longitude"], (str(round(packet["latitude"])) + "|" + str(round(packet["longitude"])))
        )
        packet["true_heading"] = packet["magnetic_heading"] + magnetic_dec
#        print(f"mag_heading: %f  dec: %f  true_heading:   %f \n", packet["magnetic_heading"], magnetic_dec,  packet["true_heading"])
        prev_nonce = self.status_cache.get(
            packet["hardware_id"], {"nonce": 0}
        )["nonce"]
        self.save_packet_to_db(packet)

        if prev_nonce > packet["nonce"]:
            return

        packet["line_distances"] = {}
        hardware_id = packet["hardware_id"]
        races = list(self.race_cache.values())

        if any(hardware_id in r["boat_devices"] for r in races):
            self.calculate_distances(packet)

        overhead = (timezone.now() - packet["datetime"]).total_seconds()
        packet["proc_overhead"] = overhead
        self.meta_telemetry["rec_to_snd"].append(overhead)

        self.relay_packet(packet)

    def run(self):
        db.connection.close()
        self.mqtt_client = mqtt.Client(transport="websockets")
        self.mqtt_client.username_pw_set(
            settings.MQTT_USERNAME, settings.MQTT_PASSWORD
        )
        self.mqtt_client.connect(settings.MQTT_HOST, int(settings.MQTT_PORT))
        self.mqtt_client.loop_start()

        t0 = timezone.now()
        flush_timer = timezone.now()

        while True:
            packet = self.packet_queue.get()
            self.process_packet(packet)

            # diff = (timezone.now() - t0).total_seconds()
            # flush_diff = (timezone.now() - flush_timer).total_seconds()

            # if flush_diff > 1:
            #     self.flush_log_buffer()
            #     flush_timer = timezone.now()

            # if diff > 10:
            #     logger.debug(f"Monitoring races {self.race_cache}")
            #     means = {
            #         f"mean_{field}": np.mean(list(telem))
            #         for field, telem in self.meta_telemetry.items()
            #     }
            #     qsize = self.packet_queue.qsize()
            #     msg = f"Status: QSIZE={qsize:3d}, "
            #     msg += ", ".join((f"{k}={v:.5f}" for k, v in means.items()))

            #     logger.debug(msg)
            #     t0 = timezone.now()
 
