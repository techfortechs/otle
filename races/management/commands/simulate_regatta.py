import logging
import pathlib
import random
import re
from datetime import datetime, timedelta
from itertools import chain

import numpy as np
import pyproj
import pytz
from django.conf import settings
from django.contrib.gis.geos import Point as gis_Point
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone
from shapely.geometry import Point, Polygon

from races import simulator
from races.models import (
    Boat,
    BoatOutline,
    BoatSetup,
    Course,
    Device,
    Line,
    Mark,
    MarkCourseOrder,
    MarkSetup,
    Race,
    Regatta,
)

logger = logging.getLogger(__name__)

EPOCH_2022 = datetime(2022, 1, 1, tzinfo=pytz.utc)


def rotation(r):
    return np.asarray([np.cos(r), -np.sin(r), np.sin(r), np.cos(r)]).reshape(
        2, 2
    )


def _make_unique_device():
    hw_id = None
    while hw_id is None:
        sample = random.randint(0, 2**32)
        if not Device.objects.filter(hardware_id=sample).exists():
            hw_id = sample
    hw_hex = f"{hw_id:08x}"
    device = Device(
        name=hw_hex, hardware_id=hw_hex, chip="FP", use="RO", alt_id="unused?"
    )
    device.save()
    return device


def _parse_gmaps_polygon(path):
    with open(path, "rt") as fin:
        for i, line in enumerate(fin.readlines()):
            if i == 0:
                continue
            polygon, name = line.rsplit(",", 1)
            break
    raw_coordinates = re.findall(r"[-\d\.]+\s[-\d\.]+", polygon)
    coordinates = [
        (float(line.split()[0]), float(line.split()[1]))
        for line in raw_coordinates
    ]
    return Polygon(coordinates)


def _create_points_in_polygon(polygon, n_points):
    points = []
    min_long, min_lat, max_long, max_lat = polygon.bounds
    while len(points) < n_points:
        lat = np.random.uniform(min_lat, max_lat)
        long = np.random.uniform(min_long, max_long)
        point = Point(long, lat)
        if polygon.contains(point):
            points.append(point)
    return points


def connect_to_mqtt_broker(device_id):
    from paho.mqtt import client as mqtt_client  # imported here for mp support

    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            logger.info(f"Client {client} connected to MQTT broker")
        else:
            logger.error(f"Client {client} could not connect. RC: {rc}")

    def on_disconnect(client, userdata, rc):
        if rc != 0:
            logger.error(f"{client} disconnecting for {rc}")
            client.reconnect()

    client = mqtt_client.Client(f"py_{device_id}", transport="websockets")
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.username_pw_set(settings.MQTT_USERNAME, settings.MQTT_PASSWORD)
    client.connect(host=settings.MQTT_HOST, port=int(settings.MQTT_PORT))
    return client


def get_next_pos(positions, counter, jitter_scale=0.001):
    N_POS = len(positions)
    longitude, latitude = positions[counter % N_POS]
    lat_jitter = random.uniform(-jitter_scale, jitter_scale)
    long_jitter = random.uniform(-jitter_scale, jitter_scale)

    return latitude + lat_jitter, longitude + long_jitter


def project_linear_x(x1, distance, slope):
    x2 = distance * (1 / np.sqrt(1 + slope**2)) + x1
    return x2


def project_linear_y(y1, distance, slope):
    y2 = distance * (slope / np.sqrt(1 + slope**2)) + y1
    return y2


def get_or_create_boat_outline():
    try:
        boat_outline = BoatOutline.objects.get(name="test-outline")
    except BoatOutline.DoesNotExist:
        simulation_dir = pathlib.Path("simulation")
        raw_measurements = np.loadtxt(
            simulation_dir / "raw_measurements.txt", delimiter=","
        )
        centered_measurements = raw_measurements - [0, 1]
        translated_measurements = centered_measurements - [-939, 0]
        scale_factor = 3.937 / np.abs(
            translated_measurements[0, 0] - translated_measurements[-1, 0]
        )
        scaled_measurements = translated_measurements * scale_factor
        # Orient so boat is facing "North"
        R = rotation(np.pi / 2)
        rotated_measurements = np.matmul(scaled_measurements, R)
        mirrored_measurements = np.vstack(
            (rotated_measurements, rotated_measurements * [-1, 1])
        )

        measurement_ring = np.concatenate(
            (
                mirrored_measurements,
                [mirrored_measurements[0]],
            ),
            axis=0,
        )
        np.savetxt(
            simulation_dir / "standardized_outline.txt",
            measurement_ring,
            delimiter=",",
        )
        boat_outline = BoatOutline.from_file(
            simulation_dir / "standardized_outline.txt", name="test-outline"
        )
        boat_outline.save()
    return boat_outline


def simulate_race(race_uuid, mark_sims):
    race = Race.objects.get(uuid=race_uuid)
    boat_processes = [
        simulator.BoatSimulator(race_uuid, boat.id)
        for boat in race.boats.all()
    ]

    bouy_processes = []

    for mark_id, device_id in mark_sims:
        mark = Mark.objects.get(pk=mark_id)
        device = Device.objects.get(pk=device_id)
        proc = simulator.BouySimulator(race_uuid, device, mark)
        bouy_processes.append(proc)

    for p in chain(boat_processes, bouy_processes):
        p.start()

    try:
        for p in chain(boat_processes, bouy_processes):
            p.join()
    finally:
        for p in chain(boat_processes, bouy_processes):
            p.terminate()


class Command(BaseCommand):
    help = "Simulate a Regatta instance in the Charles River"

    def add_arguments(self, parser):
        parser.add_argument("regatta_name", type=str)
        parser.add_argument("--minutes-to-start", type=int, default=2)
        parser.add_argument(
            "--polygon-file",
            type=str,
            default="simulation/Boat Positions- Valid Boat Positions.csv",
        )
        parser.add_argument("--n-boats", type=int, default=20)
        parser.add_argument("--n-marks", type=int, default=6)
        parser.add_argument("--start-line-width", type=int, default=20)

    def handle(self, *args, **kwargs):
        regatta = Regatta(name=kwargs["regatta_name"])
        polygon = _parse_gmaps_polygon(kwargs["polygon_file"])
        mark_sims = []

        p1 = _create_points_in_polygon(polygon, 1)[0]
        geodesic = pyproj.Geod(ellps="WGS84")

        # Create starting line using the given width at a random
        # orientation
        orientation = np.random.uniform(0, 359)
        long, lat, _ = geodesic.fwd(
            p1.x, p1.y, orientation, kwargs["start_line_width"]
        )
        p2 = (long, lat)

        pin = Mark(
            name="pin", mark_type="PI", static_position=gis_Point(p1.x, p1.y)
        )
        rc = Mark(name="rc", mark_type="RC", static_position=gis_Point(*p2))
        n_required_devices = kwargs["n_boats"] + kwargs["n_marks"]
        device_positions = _create_points_in_polygon(
            polygon, n_required_devices
        )

        with transaction.atomic():
            pin.save()
            rc.save()
            devices = []

            line = Line(
                name="test-line",
                pin=pin,
                rc_boat=rc,
                pin_device=_make_unique_device(),
                rc_device=_make_unique_device(),
            )
            line.save()
            mark_sims.append((line.pin.id, line.pin_device.id))
            mark_sims.append((line.rc_boat.id, line.rc_device.id))

            now = datetime.now().isoformat()
            course = Course(
                name=f"course: {now}", starting_line=line, finish_line=line
            )

            race = Race(
                parent_regatta=regatta,
                name="test-race",
                start_time=timezone.now(),
                course=course,
                countdown_time=timedelta(seconds=100),
            )

            regatta.save()
            course.save()
            race.save()

            boats = []
            marks = []

            for i, _ in enumerate(device_positions):
                devices.append(_make_unique_device())

            boat_outline = get_or_create_boat_outline()
            for i in range(kwargs["n_boats"]):
                boat = Boat(
                    name=f"Boat-{i}",
                    alt_id=f"Alt-boat-{i}",
                    outline=boat_outline,
                )
                boats.append(boat)

            for j in range(kwargs["n_marks"]):
                position = device_positions[j + i]
                mark = Mark(
                    name=f"mark-{j}",
                    mark_type="MA",
                    alt_id=f"mark-{j}",
                    static_position=gis_Point(position.x, position.y),
                )
                marks.append(mark)

            Boat.objects.bulk_create(boats)
            Mark.objects.bulk_create(marks)

            # Setup Course
            for order, mark in enumerate(marks):
                mo = MarkCourseOrder(course=course, mark=mark, order=order)
                mo.save()

            # Setup Race
            for i, boat in enumerate(boats):
                BoatSetup(
                    race=race,
                    boat=boat,
                    registered_device=devices[i],
                    side_side_offset=0,
                    fore_aft_offset=0,
                ).save()
            for i, mark in enumerate(marks, start=len(boats)):
                MarkSetup(
                    race=race,
                    mark=mark,
                    registered_device=devices[i],
                ).save()
                mark_sims.append((mark.id, devices[i].id))

        # Simulate Race
        race.save()
        simulate_race(race.uuid, mark_sims)
