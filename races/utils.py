import logging
from datetime import datetime, timedelta

import numpy as np
from django.utils.timezone import make_aware, now
from geomag import WorldMagneticModel

wmm = WorldMagneticModel()

def to_dt(val, start):
    if val is None or val == "":
        return None
    if isinstance(val, str):
        if val.lstrip("-", 1).isdecimal():
            val = int(val)
        if val.lstrip("-", 1).replace(".", "").isdecimal():
            val = float(val)
        else:
            val = datetime.fromisoformat(val)
    if isinstance(val, int) and val < 970273376:  # year 2000
        val = float(val)
    if isinstance(val, float):
        delta = timedelta(seconds=int(val), milliseconds=(val % 1) * 1000)
        if start and val > 0:
            val = start + delta
        elif val <= 0:
            val = now() + delta  # Negative
    if isinstance(val, int):
        val = datetime.fromtimestamp(val)
    if isinstance(val, datetime):
        if not val.tzinfo:
            val = make_aware(val)
        return val
    logging.warning(f"Unknown datetime value: {val} ({type(val).__name__})")
    return None


def make_rotation_matrix(radians):
    return np.asarray(
        [np.cos(radians), -np.sin(radians), np.sin(radians), np.cos(radians)]
    ).reshape(2, 2)


def rotate_points(points, degrees, origin=(0, 0)):
    R = make_rotation_matrix(np.radians(-degrees))
    o = np.atleast_2d(origin)
    p = np.atleast_2d(points)

    return np.squeeze((R @ (p.T - o.T) + o.T).T)


MAGNETIC_DEC_CACHE = {}


def get_magnetic_offset(latitude, longitude, zone=None):
    if zone is None:
        zone = str(round(latitude)) + "|" + str(round(longitude))

    try:
        dec = MAGNETIC_DEC_CACHE[zone]
    except KeyError:
        dec = wmm.calc_mag_field(latitude, longitude).declination 
        MAGNETIC_DEC_CACHE[zone] = dec

    return dec
    # returning positive value works, but doesn't make sense. Not the biggest issue right now.
