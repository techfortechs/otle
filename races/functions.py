from django.contrib.gis.db import models as gis_models
from django.contrib.gis.db.models import (
    LineStringField,
    PointField,
    PolygonField,
)
from django.db.models import FloatField, Func, Value


class Rotate(Func):
    function = "ST_Rotate"
    output_field = PolygonField(dim=2)


class MakeLine(Func):
    """The standard MakeLine function wrapper provided by Django
    assumes this field is an aggregation function. But OTLE does
    not need it for an aggreation, we just want to construct a line
    from a set of points.
    """

    function = "ST_MakeLine"
    output_field = LineStringField()


class MakePoint(Func):
    """Wrapper for ST_MakeLine"""

    function = "ST_MakePoint"
    output_field = PointField()


class SetSRID(Func):
    """Wrapper for ST_SetSRID"""

    function = "ST_SetSRID"
    output_field = PointField()


class Distance(Func):
    """
    Wrapper for ST_Distance
    """

    function = "ST_Distance"
    output_field = FloatField()


def MakeBuffer(expression, distance, Field, **kwargs):
    if len(kwargs) > 0:
        kwarg_strings = [f"{k}={v}" for k, v in kwargs.items()]
        kwarg_str = " ".join(kwarg_strings)
        func = Func(
            expression,
            Value(distance),
            Value(kwarg_str),
            function="ST_Buffer",
            output_field=getattr(gis_models, f"{Field.__name__}Field")(),
        )
        return func
    return Func(
        expression,
        Value(distance),
        function="ST_Buffer",
        output_field=getattr(gis_models, f"{Field.__name__}Field"),
    )


class GetX(Func):
    """
    Wrapper for ST_X
    """

    function = "ST_X"
    output_field = FloatField()


class GetY(Func):
    """
    Wrapper for ST_Y
    """

    function = "ST_Y"
    output_field = FloatField()


class Radians(Func):
    """
    Wrapper for RADIANS
    """

    function = "RADIANS"
    outputfield = FloatField()
