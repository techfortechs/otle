from django import forms

from races import models as m


class BoatOutlineAdminForm(forms.ModelForm):
    class Meta:
        model = m.BoatOutline
        fields = ["name", "outline_file"]

    name = forms.CharField(label="Outline name", max_length=128)
    outline_file = forms.FileField(label="Upload outline csv")
