import logging
import os
from multiprocessing import Process

import paho.mqtt.client as mqtt
from django.conf import settings

from races.packets import PositionPacket

logger = logging.getLogger(__name__)


def translate_packet(packet: PositionPacket):
    #print( "lat: " + str(packet.latitude) + " lon: " + str(packet.longitude) + " mag_head: " + str(packet.magnetic_heading))
    return {
        "nonce": packet.nonce,
        "datetime": packet.time,
        "latitude": packet.latitude,
        "longitude": packet.longitude,
        "error_2d": packet.error_2d,
        "magnetic_heading": packet.magnetic_heading,
        "true_heading": packet.true_heading,
    }


def on_connect(client, userdata, flags, rc):
    logger.info(f"Connecting to mqtt {client}, {flags}, {rc}")
    if rc == 0:
        client.subscribe(settings.POSITION_CHANNEL)
    else:
        if rc == 1:
            logger.error(
                "MQTT connection refused - incorrect protocol version"
            )
        elif rc == 2:
            logger.error("MQTT connection refused - invalid client identifier")
        elif rc == 3:
            logger.error("MQTT connection refused - server unavailable")
        elif rc == 4:
            logger.error(
                "MQTT connection refused - bad connection credentials"
            )
        elif rc == 5:
            logger.error("MQTT connection refused - not authorized")
        else:
            logger.error(
                "MQTT connection refused. Server return a non-standard value "
                f"of {rc}"
            )


def make_message(queue):
    from races import mqtt_relay
    from races.packets import PositionPacket

    def on_message(client, userdata, message):
        packet = PositionPacket.from_binary_packet(message.payload)
        translated_packet = mqtt_relay.translate_packet(packet)
        hardware_id, _ = message.topic.split("/")
        translated_packet["hardware_id"] = hardware_id
        print(f"received and ttranslated packet from {hardware_id}")
        queue.put(translated_packet)
        

    return on_message


class MQTTRelay(Process):
    def __init__(
        self, host, port, connect_callback, message_callback, **kwargs
    ):
        super().__init__(**kwargs)
        self.host = host
        self.port = port
        self.client = mqtt.Client(transport="websockets")
        self.client.on_connect = connect_callback
        self.client.on_message = message_callback

    def run(self):

        logger.info(f"Entering MQTT Relay main runtime at pid {os.getpid()}")
        self.client.username_pw_set(
            settings.MQTT_USERNAME, settings.MQTT_PASSWORD
        )
        self.client.connect(
            self.host,
            self.port,
        )
        logger.info("Entering blocking MQTT Client thread")
        self.client.loop_forever()
