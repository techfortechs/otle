from django.contrib import admin
from django.contrib.gis.admin import GeoModelAdmin, GISModelAdmin

from races import models as m
from races.forms import BoatOutlineAdminForm


class RaceInlineAdmin(admin.TabularInline):
    model = m.Race


class BoatSetupInlineAdmin(admin.TabularInline):
    model = m.BoatSetup


class MarkSetupInlineAdmin(admin.TabularInline):
    model = m.MarkSetup


@admin.register(m.BoatOutline)
class BoatOutlineAdmin(GeoModelAdmin):
    form = BoatOutlineAdminForm
    modifiable = False
    fields = ["name", "outline_file", "outline", "meter_length"]
    readonly_fields = ("outline",)
    gis_widget_kwargs = {
        "attrs": {
            "default_zoom": 25,
        },
    }

    def save_model(self, request, obj, form, change):
        file_obj = request.FILES["outline_file"]
        outline, length = m.BoatOutline.outline_from_file(file_obj)
        obj.outline = outline
        obj.meter_length = length
        super().save_model(request, obj, form, change)


@admin.register(m.Race)
class RaceAdmin(GISModelAdmin):
    list_display = [
        "name",
        "start_time",
    ]
    inlines = [BoatSetupInlineAdmin, MarkSetupInlineAdmin]


@admin.register(m.Regatta)
class RegattaAdmin(admin.ModelAdmin):
    inlines = [RaceInlineAdmin]
    model = m.Regatta


@admin.register(m.Course)
class CourseAdmin(admin.ModelAdmin):
    model = m.Course


@admin.register(m.Line)
class LineAdmin(admin.ModelAdmin):
    model = m.Line


admin.site.register(m.Boat)
admin.site.register(m.Mark)
admin.site.register(m.Device)
admin.site.register(m.DevicePositionLog, GISModelAdmin)
