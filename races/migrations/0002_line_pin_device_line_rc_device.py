import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("races", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="line",
            name="pin_device",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="as_pin_devices",
                to="races.device",
            ),
        ),
        migrations.AddField(
            model_name="line",
            name="rc_device",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="as_rc_devices",
                to="races.device",
            ),
        ),
    ]
