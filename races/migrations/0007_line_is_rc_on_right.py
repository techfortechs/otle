# Generated by Django 4.0.6 on 2023-06-08 14:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("races", "0006_devicepositionlog_adjusted_magnetic_heading"),
    ]

    operations = [
        migrations.AddField(
            model_name="line",
            name="is_rc_on_right",
            field=models.BooleanField(
                default=True,
                help_text="Is the RC boat on the left or right of the line?",
            ),
        ),
    ]
