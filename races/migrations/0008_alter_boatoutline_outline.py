# Generated by Django 4.0.6 on 2023-06-13 01:01

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("races", "0007_line_is_rc_on_right"),
    ]

    operations = [
        migrations.RemoveField(model_name="boatoutline", name="outline"),
    ]
