/**
 * Boating race mapping application based on Leaflet.js
 *
 * Author: Martin Owens
 *
 * Copyright 2022, Scott Dynes
 *
 * All Rights Reserved.
 */

var STATIC_URL = "./";
var boatIcon = null;
var bouyIcon = null;
var map = null;
var boats = {};
var fast_forward = 20;

var is_running = false;
var race = {
    'id': -1,
    'url': '', // Location where information can be found
    'timer': 500,
    'ct': 0, // // This is the timer in ms
    'dt': 0, // This is the race start date/time for converting race_ct to a dt.
};

function swap(c) {
    return [c[1], c[0]];
}

function loadRace(race_id, request_url, static_url, start) {
    STATIC_URL = static_url;
    boatIcon = L.icon({
        iconUrl: static_url + '/boat.svg',
        iconSize: [16, 32],
        iconAnchor: [8, 16],
        popupAnchor: [-3, -76],
        //shadowUrl: 'boat-shadow.svg',
        //shadowSize: [68, 95],
        //shadowAnchor: [22, 94]
    });
    bouyIcon = L.icon({
        iconUrl: static_url + '/bouy.svg',
        iconSize: [8, 8],
        iconAnchor: [4, 4],
    });

    map = loadMap('map_node', [42.3584436, -71.0878439]);
    //addBoat(map, swap(start[0]), 60);
    //addBouy(map, swap(start[1]));
    //addLine(map, swap(start[0]), swap(start[1]));

    startRace(race_id, request_url, 500, true); // Cadence tick to request data in ms (0.5s)
}

/* ==== Timers and race management ==== */

function startRace(race_id, url, ms, live) {
    is_running = true;
    race['id'] = race_id;
    race['url'] = url;
    race['ct'] = 0;
    race['overs'] = 0;
    boats = {};

    raceTock(ms, live);
}

function raceTock(ms, live) {
    setTimeout(function() {
        race.ct = raceTick(ms * fast_forward, live);
        if (is_running) {
            raceTock(ms, live);
        } // else something went wrong, all STOP!
    }, ms);
}

function raceTick(ms, live) {

    // If a server request is already happening, skip this tick (and warn about it)
    if (race.xhr) {
         console.warn("Tick skipped, still asking for data.", race.ct);
         race['overs'] += 1;
         // TODO: Check overs for the maximum amount of time we will wait.
         return race.ct;
    }

    race.xhr = new XMLHttpRequest();
    race.xhr.open('POST', race['url'], true);
    race.xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8');
    race.xhr.setRequestHeader('X-CSRFToken', document.body.dataset.csrf);
    race.xhr.onload = function () {
        if(race.xhr.status === 200) {
            setBoats(JSON.parse(race.xhr.response));
        }
        if(race.xhr.status === 403 || race.xhr.status == 500) {
            return;
        }
        delete race['xhr'];
    }
    if (live) {
        // Live is just everything in the last time slice, whatever that is.
        // We ask for 1.5x the data to cover any slippage and over-ask
        race.xhr.send(JSON.stringify({'from': ms / 1000 * -1.2}));
        return ms * -1.0;
    }
    var next_ct = race.ct + ms;
    race.xhr.send(JSON.stringify({
        'from': race.ct / 1000,
        'to': next_ct / 1000,
    }));
    return next_ct;
}

function setBoats(packet)
{
    console.log("setBoats", packet);
    for (let i = 0; i < packet.ticks.length; i++) {
        var tick_id = packet.ticks[i][0];
        var boat_id = packet.ticks[i][1];
        var coord = swap(packet.ticks[i][2]);
        var course_t = packet.ticks[i][3];

        if (boats[boat_id] == null) {
            boats[boat_id] = {
                marker: addBoat(map, coord, 0),
                last_tick: tick_id,
            }
        } else if (boats[boat_id].last_tick < tick_id) {
            boats[boat_id].marker.setLatLng(coord);
            boats[boat_id].marker.options.rotationAngle = course_t;
            boats[boat_id].last_tick = tick_id;
        }
    }
}

/*==== Loading and moving mapping elements ====*/

function loadMap(elem, center) {
    var mymap = L.map(elem).setView(center, 15);

    var vectorStyles = {
      // Style for background is in leaflet.css .leaflet-container
      land: {
        fill: true,
        weight: 1,
        fillColor: '#ead4b1',
        color: '#000000',
        fillOpacity: 1.0,
        opacity: 0.2,
      },
    };

    // Contours map
    var url = "https://api.maptiler.com/tiles/land/{z}/{x}/{y}.pbf?key={key}";

    L.vectorGrid.protobuf(url, {
        vectorTileLayerStyles: vectorStyles,
        subdomains: '0123',
        attribution: '© OpenStreetMap contributors, © MapTiler',
        key: 'PCbFEoCKVzRsYKQnPScb',
        maxNativeZoom: 14,
        minNativeZoom: 0,
        maxZoom: 19,
        minZoom: 10,
    }).addTo(mymap);

    return mymap;
}

function addBoat(map, center, angle) {

    var marker = L.marker(center, {
        icon: boatIcon,
        rotationAngle: angle,
    })
    marker.addTo(map);
    return marker;
}

function addBouy(map, center) {

    L.marker(center, {
        icon: bouyIcon,
    }).addTo(map);
}

function addLine(map, a, b) {
    var pointA = new L.LatLng(a[0], a[1]);
    var pointB = new L.LatLng(b[0], b[1]);

    new L.Polyline([pointA, pointB], {
        color: 'grey',
        dashArray: '6 4',
        weight: 2,
        opacity: 0.5,
        smoothFactor: 1
    }).addTo(map);
}

/*==== Parsing data formats ====*/

function ltrim(x, characters) {
    var start = 0;
    while (characters.indexOf(x[start]) >= 0) {
        start += 1;
    }
    var end = x.length - 1;
    return x.substr(start);
}

function parseDateTime(string)
{
    return new Date(Date.parse(ltrim(string, "Q")));
}

