/**
 * Quickly define a bunch of commonly used functions.
 * @author William Fong
 */

function clearDOMChildren(ele) {
  while(ele.firstChild) {
    ele.removeChild(ele.lastChild);
  }
};
