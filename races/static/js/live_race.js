var globalAppData = {
  rootElem: undefined,
  regatta: undefined,
  mqttClient: undefined,
  mqttHost: undefined,
  mqttPort: undefined,
  mapDisplay: undefined,
  currentRace: undefined,
  deviceWhitelist: undefined,
  deviceToMarker: undefined,
  boatIcon: undefined,
  markIcon: undefined,
  boatStatuses: undefined,
};

const TEXT_DECODER = new TextDecoder("ascii")

const INT_MAX = 2**32 - 1;
const LATITUDE_MAX = 90;
const LATITUDE_MIN = -90;
const LONGITUDE_MAX = 180;
const LONGITUDE_MIN = -180;

const HEADING_MAX = 359;
const HEADING_MIN = 0;

const OTLE_BYTES = 4;
const NONCE_BYTES = 2;
const LATITUDE_BYTES = 5;
const LONGITUDE_BYTES = 5;
const ERR_BYTES = 1;
const TIMESTAMP_BYTES = 5;
const HEADING_M_BYTES = 1;
const HEADING_GPS_BYTES = 1;

const EPOCH_2022 = new Date("2022-01-01");


function countdownToRaceStart(timeStart, elementID) {
  const countdownElement = document.getElementById(elementID);

  const interval = setInterval(
    () => {
      const now = new Date().getTime();
      var diff = timeStart.getTime() - now;
      var sign;
      if (diff > 0) {
        sign = "-";
      } else {
        sign = "+";
      }

      diff = Math.abs(diff);

      var days = Math.floor(diff / (1000 * 60 * 60 * 24));
      var hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((diff % (1000 * 60)) / 1000);

      days = days.toString().padStart(2, "0");
      hours = hours.toString().padStart(2, "0");
      minutes = minutes.toString().padStart(2, "0");
      seconds = seconds.toString().padStart(2, "0");

      countdownElement.innerHTML = `${sign}${days}d ${hours}h ${minutes}m ${seconds}s`;
    },
    1000
  );
  return interval;
};


function onConnect(mqtt, deviceIDs) {
  return () => {
    console.log('Connected to mqtt websocket!');
    console.log(`Registering subscriptions for ${deviceIDs}`);
    for (i in deviceIDs) {
      mqtt.subscribe(`${deviceIDs[i]}/position`);
    }
  };
}

function rescale(value, value_min, value_max, range_min, range_max) {
  nominator = (value - value_min);
  denominator = (value_max - value_min);
  scaler = (range_max - range_min);
  offset = range_min;

  return (nominator / denominator) * scaler + offset;
};

function reconstructMultiByteInteger(bufferSlice, signed) {
  // Assumes big endian
  var nBytes = bufferSlice.length;
  var value = 0;
  for (let i=0; i < nBytes; i++) {
    value = (value * 256) + bufferSlice[i];
  }

  if (signed) {
    sign = bufferSlice[0] & 0x80;
    if (sign) {
      value -= Math.pow(2, nBytes * 8);
    }
  }
  return value;
};


function interpretPositionPayload(msg) {
  var buf = msg.payloadBytes;

  var packetBytes = buf.slice(OTLE_BYTES);
  var offset = 0;
  var binaryNonce = packetBytes.slice(offset, offset + NONCE_BYTES);
  offset += NONCE_BYTES;
  var binaryTime = packetBytes.slice(offset, offset + TIMESTAMP_BYTES);
  offset += TIMESTAMP_BYTES;
  var binaryLat = packetBytes.slice(offset, offset + LATITUDE_BYTES);
  offset += LATITUDE_BYTES;
  var binaryLong = packetBytes.slice(offset, offset + LONGITUDE_BYTES);
  offset += LONGITUDE_BYTES;
  var binaryError2D = packetBytes.slice(offset, offset + ERR_BYTES);
  offset += ERR_BYTES;
  var binaryHeadingM = packetBytes.slice(offset, offset + HEADING_M_BYTES);
  offset += HEADING_M_BYTES;
  var binaryHeadingGPS = packetBytes.slice(offset, offset + HEADING_GPS_BYTES);
  offset += HEADING_GPS_BYTES;


  var nonce = reconstructMultiByteInteger(binaryNonce, false);
  var intTime = reconstructMultiByteInteger(binaryTime, false);
  var intLatitude = reconstructMultiByteInteger(binaryLat, true);
  var intLongitude = reconstructMultiByteInteger(binaryLong, true);
  var intHeadingM = binaryHeadingM[0];
  var intHeadingGPS = binaryHeadingGPS[0];
  var error2D = binaryError2D[0];

  var data = {
    nonce, error2D,
  };

  data.time = new Date(EPOCH_2022.getTime() + (intTime * 100));

  data.latitude = intLatitude / 10e8;
  data.longitude = intLongitude / 10e8;
  data.magneticHeading = rescale(
    binaryHeadingM,
    0,
    256,
    HEADING_MIN,
    HEADING_MAX
  );
  data.gpsHeading = rescale(
    binaryHeadingGPS,
    0,
    256,
    HEADING_MIN,
    HEADING_MAX
  )
  return data;
};


function makeRaceSelection() {
  const regatta = globalAppData.regatta;
  var select = document.createElement("select");
  var defaultOption = document.createElement("option");
  select.onchange = () => {
    var curSelection = document.getElementById("race-selection").value;
    getRace(curSelection);
  };
  select.id = "race-selection";
  defaultOption.disabled = true;
  defaultOption.selected = true;
  defaultOption.value = null;
  defaultOption.text = "-- Select A Race--";
  select.appendChild(defaultOption);
  regatta.races.forEach(
    (race) => {
      var option = document.createElement("option");
      option.value = race.uuid;
      option.text = race.name;
      select.appendChild(option);
    }
  );
  return select;
};

function placeMarksOnMap(markSetups, map) {
  markSetups.forEach(markSetup => {
    var staticPosition = markSetup.mark.static_position;
    var marker = L.marker(
      [staticPosition.latitude, staticPosition.longitude],
      {
        icon: globalAppData.markIcon,
        title: markSetup.mark.name,
      }
    );
    marker.addTo(map);
  });
};

function placeStartingLineOnMap(startingLine, map) {
  var line = L.polyline(
    [
      [startingLine[0].latitude, startingLine[0].longitude],
      [startingLine[1].latitude, startingLine[1].longitude],
    ],
    {}
  );
  line.addTo(map);
};

function projectLinearX(x1, distance, slope) {
  var x2 = distance * (1 / Math.sqrt(1 + Math.pow(slope, 2))) + x1;
  return x2;
};

function projectLinearY(y1, distance, slope) {
  var y2 = distance * (slope / Math.sqrt(1 + Math.pow(slope, 2))) + y1;
  return y2;
};

function placePolygonOnMap(startingLine, map) {
  var long_1 = startingLine[0].longitude;
  var long_2 = startingLine[1].longitude;
  var lat_1 = startingLine[0].latitude;
  var lat_2 = startingLine[1].latitude;

  var p1 = fromLatLon(startingLine[0].latitude, startingLine[0].longitude);
  var p2 = fromLatLon(startingLine[1].latitude, startingLine[1].longitude);

  var zoneNum = p1.zoneNum;
  var zoneLetter = p1.zoneLetter;

  var slope = (p2.northing - p1.northing) / (p2.easting - p1.easting);
  var behindAngle = Math.atan(slope) + 90.0;
  var behindSlope = Math.tan(behindAngle);

  console.log({
    slope,
    behindAngle,
    behindSlope,
  });

  var p3 = {
    easting: projectLinearX(p2.easting, 10, behindSlope),
    northing: projectLinearY(p2.northing, 10, behindSlope),
  };
  var p4 = {
    easting: projectLinearX(p1.easting, 10, behindSlope),
    northing: projectLinearY(p1.northing, 10, behindSlope),
  };

  var transP3 = toLatLon(p3.easting, p3.northing, zoneNum, zoneLetter);
  var transP4 = toLatLon(p4.easting, p4.northing, zoneNum, zoneLetter);

  console.log(p1, p2, p3, p4);

  var polygon = L.polygon(
    [
      [lat_1, long_1],
      [lat_2, long_2],
      [transP3.latitude, transP3.longitude],
      [transP4.latitude, transP4.longitude],
    ],
    {
      color: "red",
      opacity: 0.1,
    },
  );
  polygon.addTo(map);
};

function loadRegattaResponse(json) {
  var appDiv = document.createElement("div")
  appDiv.id = "application";

  globalAppData.regatta = json;
  globalAppData.deviceWhitelist = new Set();

  appDiv.appendChild(makeRaceSelection());

  globalAppData.rootElem.replaceWith(appDiv);
};

function loadRaceResponse(json) {
  console.log(json);
  globalAppData.currentRace = json;

  var markSetups = globalAppData.currentRace.mark_setups;
  var boatSetups = globalAppData.currentRace.boat_setups;
  var whitelist = new Set();
  var deviceIDs = new Array();

  markSetups.forEach(
    setup => {
      whitelist.add(setup.registered_device.hardware_id);
      deviceIDs.push(setup.registered_device.hardware_id);
    }
  );
  boatSetups.forEach(
    setup => {
      whitelist.add(setup.registered_device.hardware_id);
      deviceIDs.push(setup.registered_device.hardware_id);
    }
  );

  globalAppData.deviceWhitelist = whitelist;
  globalAppData.deviceToMarker = new Map();

  if (globalAppData.mapDisplay === undefined) {
    globalAppData.mapDisplay = L.map("map");
    L.tileLayer(
      "http://192.168.4.1:8088/tiles/1.0.0/osm_demo/webmercator/{z}/{x}/{y}.png",
	{maxZoom: 19,
	 attribution: "",
	 tiled:true
	}
    ).addTo(globalAppData.mapDisplay);
  }
  globalAppData.mapDisplay.setView(
    [
      markSetups[0].mark.static_position.latitude,
      markSetups[0].mark.static_position.longitude,
    ],
    13
  );

  placeMarksOnMap(markSetups, globalAppData.mapDisplay);
  placeStartingLineOnMap(json.starting_line, globalAppData.mapDisplay);
  placePolygonOnMap(json.starting_line, globalAppData.mapDisplay);
  countdownToRaceStart(new Date(json.start_time), "timer");

  if (globalAppData.mqttClient === undefined) {
    globalAppData.mqttClient = establishMQTTConn(deviceIDs);
  } else {
    // TODO Clear Markers and Reload Map to reflect new race.
  }

  globalAppData.boatStatuses = new Map();
};

async function getRegatta(regatta_id) {
  var url = `/api/regatta/${regatta_id}`;
  console.log("loading:", url);
  await fetch(url)
    .then((data) => {return data.json()})
    .then(loadRegattaResponse)
    .catch((error) => {console.log("Error:", error)})
};

async function getRace(race_id) {
  var url = `/api/race/${race_id}`;
  await fetch(url)
    .then((data) => {return data.json()})
    .then(loadRaceResponse)
    .catch((error) => {console.log("Error:", error)});
};

function makeBoatIcon(positionPacket) {
  var marker = L.marker(
    [positionPacket.latitude, positionPacket.longitude],
    {
      icon: globalAppData.boatIcon,
      rotationAngle: positionPacket.heading,
    }
  );
  return marker;
};

function onMessageArrived(msg) {
  try {
    var data = interpretPositionPayload(msg);
    data.deviceID = msg.destinationName.split("/")[0];
    let marker;  // Forward Declare
    if (globalAppData.deviceToMarker.has(data.deviceID)) {
      marker = globalAppData.deviceToMarker.get(data.deviceID);
      marker.setLatLng([data.latitude, data.longitude]);
      marker.options.rotationAngle = data.gpsHeading;
    } else {
      marker = makeBoatIcon(data);
      marker.addTo(globalAppData.mapDisplay);
      globalAppData.deviceToMarker.set(
        data.deviceID,
        marker
      );
    }
    // Update status
    distance = getBoatDistanceFromLine(data.latitude, data.longitude);
    console.log(data.deviceID, distance);
  } catch (error) {
    console.log(error);
  }
}

function establishMQTTConn(deviceIDs) {
  console.log("Connecting to mqtt:", globalAppData.mqttHost, globalAppData.mqttPort);
  var mqtt = new Paho.MQTT.Client(
    globalAppData.mqttHost,
    globalAppData.mqttPort,
    "clientjs"
  );
  mqtt.onMessageArrived = onMessageArrived;
  mqtt.connect({
    timeout: 3,
    onSuccess: onConnect(mqtt, deviceIDs),
  });
  return mqtt;
}


function getBoatDistanceFromLine(latitude, longitude) {
  const startingLine = globalAppData.currentRace.starting_line;
  const p1 = fromLatLon(startingLine[0].latitude, startingLine[0].longitude);
  const p2 = fromLatLon(startingLine[1].latitude, startingLine[1].longitude);
  const pos = fromLatLon(latitude, longitude);

  const lineLength = Math.sqrt(
    Math.pow(p2.easting - p1.easting, 2) +
    Math.pow(p2.northing - p1.northing, 2)
  );
  const area = Math.abs(
    (p2.easting - p1.easting)*(p1.northing - pos.northing) -
    (p1.easting - pos.easting)*(p2.northing - p1.northing)
  );

  return area / lineLength;
};

function initializeApp(rootElemID, regatta_id, mqttHost, mqttPort) {
  console.log("Entering runtime");
  globalAppData.rootElem = document.getElementById(rootElemID);
  globalAppData.mqttHost = mqttHost;
  globalAppData.mqttPort = mqttPort;
  globalAppData.rootElem.innerHTML = "Loading Regatta";
  globalAppData.boatIcon = L.icon({
    iconUrl: "/static/boat.svg",
    iconSize: [16, 32],
    iconAnchor: [8, 16],
    popupAnchor: [-3. -76]
  });
  globalAppData.markIcon = L.icon({
    iconUrl: "/static/bouy.svg",
    iconSize: [8, 8],
    iconAnchor: [4, 4],
  });
  getRegatta(regatta_id);
};
