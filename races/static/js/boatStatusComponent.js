// import 'core'

const boatStatusData = {
  rootElem: undefined,
  boatStatuses: undefined,
  boatToEleMap: undefined,
  boatLastTimestamp: undefined,
  atLeastOneBoatLengthAwayEle: undefined,
  underOneBoatLengthAwayEle: undefined,
  overTheLineEle: undefined,
};

const BOAT_METER_LENGTH = 3;

function constructBoatStatus(ele) {
  // Clear any previous boat status.
  clearDOMChildren(ele);

  const atLeastOneBoatLengthAway = document.createElement("ul");
  const underOneBoatLengthAway = document.createElement("ul");
  const overTheLine = document.createElement("ul");

  ele.appendChild(atLeastOneBoatLengthAway);
  ele.appendChild(underOneBoatLengthAway);
  ele.appendChild(overTheLine);

  boatStatusData.atLeastOneBoatLengthAwayEle = atLeastOneBoatLengthAway;
  boatStatusData.underOneBoatLengthAwayEle = underOneBoatLengthAway;
  boatStatusData.overTheLineEle = overTheLine;
  boatStatusData.rootElem = ele;

  return ele;
};


function mountBoatStatus(elementID) {
  const statusElement = document.getElementById(elementID);
  boatStatusData.boatStatuses = new Map();
  boatStatus.boatToListEleMap = new Map();
  constructBoatStatus(statusElement);
};


function updateStatuses(deviceID, distance, timestamp) {
  let status;
  if (boatStatusData.boatStatuses.has(deviceID)) {
    // Update status
    status = boatStatusData.get(deviceID);
  } else {
    // New device signal, add to map
    status = {
      distance: distance,
      timestamp: timestamp,
    };
    boatStatusData.boatStatuses.set(deviceID, status);
    boatStatusData.boatToEleMap.set(
      deviceID,
      document.createElement("li")
    );
    var initialList = listElementFromDistance(status.distance);
    initialList.appendChild(boatStatusData.boatToEleMap.get(deviceID));
  }
};


function listElementFromDistance(distance) {
  if (distance <= -BOAT_METER_LENGTH) {
    // Good distance
    return boatStatusData.atLeastOneBoatLengthAwayEle;
  } else if (distance < 0 && distance > -BOAT_METER_LENGTH) {
    // Too close
    return boatStatusData.underOneBoatLengthAwayEle;
  } else {
    // Over the line
    return boatStatusData.overTheLineEle;
  }
};

function reflectStatusToEle() {
  const elementMap = boatStatusData.boatToEleMap;
  boatStatusData.boatStatuses.forEach((status, deviceID) => {
    var { parentList, node } = elementMap.get(deviceID);

    var appropriateList = listElementFromDistance(status.distance);
    if (appropriateList !== parentList) {
      // Device has changed its status
      parentList.removeChild(node);
      appropriateList.appendChild(node);
    }
  });
};
