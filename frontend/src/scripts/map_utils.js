import {getIcon} from './core';
import L from 'leaflet';

const isDeviceInRaceLines = (
  hardwareID,
  packet,
  raceLines
) => {
  var check = false;
  if (hardwareID == raceLines.startingLinePinDevice) {
    raceLines.startingLinePos.pin = packet;
    check = true;
  }
  if (hardwareID == raceLines.startingLineRCDevice) {
    raceLines.startingLinePos.rc = packet;
    check = true;
  }
  if (hardwareID == raceLines.finishLinePinDevice) {
    raceLines.finishLinePos.pin = packet;
    check = true;
  }
  if (hardwareID == raceLines.finishLineRCDevice) {
    raceLines.finishLinePos.rc = packet;
    check = true;
  }
  return check;
};

export const updateMarker = (
  state,
  registeredMaps,
  hardwareIDToMarker,
  raceLines,
  mqttPayload
) => {
  const hardwareID = mqttPayload.hardwareID;
  if (hardwareIDToMarker.has(hardwareID)) {
    const marker = hardwareIDToMarker.get(hardwareID);
    marker.setLatLng([
      mqttPayload.latitude,
      mqttPayload.longitude
    ]);
    marker.options.rotationAngle = mqttPayload.true_heading;
  } else {
    // We need to instantiate new markers
    const deviceType = state.relevantHardwareIDs[hardwareID];
    const icon = getIcon(deviceType);
    const newMarker = L.marker([
        mqttPayload.latitude,
        mqttPayload.longitude,
      ],
      {
        icon: icon,
        rotationAngle: mqttPayload.true_heading,
      }
    );
    hardwareIDToMarker.set(hardwareID, newMarker);
    // Add each marker to each map
    registeredMaps.forEach((map) => {
      newMarker.addTo(map);
    });
  }
  if (isDeviceInRaceLines(hardwareID, mqttPayload, raceLines)) {
    registeredMaps.forEach((map) => {
      updateLines(
        state.currentRace.course,
        raceLines,
        map
      );
    });
  }
};

export const addMarkersToMap = (
  race,
  hardwareIDToMarker,
  raceLines,
  map
) => {
  hardwareIDToMarker.forEach((marker) => {
    marker.addTo(map);
  });

  updateLines(race.course, raceLines, map);
};

export const updateStartingLine = (
  raceLines,
  map
) => {
  if (raceLines.startingLine) {
    map.removeLayer(raceLines.startingLine);
    raceLines.startingLine = undefined;
  }
  const pos = raceLines.startingLinePos;
  const line = L.polyline(
    [
      [pos.pin.latitude, pos.pin.longitude],
      [pos.rc.latitude, pos.rc.longitude],
    ]
  );
  raceLines.startingLine = line;
  line.addTo(map);
};

export const updateFinishLine = (
  raceLines,
  map,
) => {
  if (raceLines.finishLine) {
    map.removeLayer(raceLines.finishLine);
    raceLines.finishLine = undefined;
  }
  const pos = raceLines.finishLinePos;
  const line = L.polyline(
    [
      [pos.pin.latitude, pos.pin.longitude],
      [pos.rc.latitude, pos.rc.longitude],
    ]
  );
  raceLines.finishLine = line;
  line.addTo(map);
};

export const updateLines = (
  course,
  raceLines,
  map
) => {
  updateStartingLine(
    raceLines,
    map
  );

  updateFinishLine(
    raceLines,
    map
  );
};
