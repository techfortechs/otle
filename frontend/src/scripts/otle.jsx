import React from 'react';
import { Provider } from 'react-redux';
import { store } from './reduxstores/common';
import { createRoot } from 'react-dom/client';
import OtleApp from './components/otleapp.jsx';


const domNode = document.getElementById("application");
const root = createRoot(domNode);

root.render(
  <Provider store={store}>
    <OtleApp />
  </Provider>
);
