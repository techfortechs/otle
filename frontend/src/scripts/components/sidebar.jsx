import React from 'react';
import { connect } from 'react-redux';
import { BoatTable } from "./boat_status_table.jsx";


const SideBar = (props) => {
  switch(props.raceView) {
    case "map":
      return <BoatTable />
    default:
      return <BoatTable />
      //return <div className="placeholder"></div>;
  }
};


const mapStateToProps = ({otle}, ownProps) => {
  return {
    ...ownProps,
    raceView: otle.raceView,
  };
};


export default connect(mapStateToProps)(SideBar);
 