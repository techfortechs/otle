import React from 'react';

export const RegattaSelect = ({regattas}) => {
  const options = regattas.map((regatta) => (
    <option key={regatta.pk}>{regatta.name}</option>
  ));
  return (
    <select>{options}</select>
  );
};
