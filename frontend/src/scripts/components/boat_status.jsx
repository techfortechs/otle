import React from "react";
import { connect } from 'react-redux';


class BoatStatus extends React.Component {
  render() {
    const boats = this.props.validBoats.map(id => (
      <button key={id} className="boat-status-icon">{id}</button>
    ));
    return (
      <>
        <h2>{this.props.name}</h2>
        {boats}
      </>
    );
  }
};


const mapStateToProps = (state, ownProps) => {
  const otle = state.otle;
  const validBoats = [];
  const race = state.otle.currentRace;

  for (const hardwareID in otle.relevantHardwareIDs) {
    if (otle.relevantHardwareIDs[hardwareID] !== "boat") {
      continue
    }
    const status = otle.latestDeviceStatus[hardwareID];

    if (status === undefined) { continue; };

    const name = otle.nameLookup[hardwareID];

    if (ownProps.discriminator(race, status)) {
      validBoats.push(name);
    }
  }
  return {...ownProps, validBoats: validBoats};
};

export default connect(mapStateToProps)(BoatStatus);
