import React from 'react';
import { connect } from 'react-redux';


class SelectRace extends React.Component {
  render() {
    if (this.props.currentRegatta) {
      const races = this.props.currentRegatta.races.map(race => (
        <option
          value={race.uuid}
          key={race.uuid}
        >
          {race.name}
        </option>
      ));
      return (
        <select
          className="otle-button"
          onChange={
            (e) => this.props.callback(e.target.value)
          }
          value={this.props.currentRaceUUID}
        >
          <option value="">Select Race</option>
          {races}
        </select>
      );

    }
    return (
      <select className="otle-button" disabled>
        <option value={null}>No Races</option>
      </select>
    );
  }
};


const mapStateToProps = ({otle}) => {
  const currentRace = otle.currentRace === undefined ? "": otle.currentRace.uuid;
  return {
    currentRegatta: otle.currentRegatta,
    currentRaceUUID: currentRace,
  };
};

export default connect(mapStateToProps)(SelectRace);
