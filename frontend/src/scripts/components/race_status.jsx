import React from 'react';
import LiveMap from './live_map.jsx';
import { FullBoatTable } from "./boat_status_table.jsx";
import { connect } from 'react-redux';
import SelectRace from './select_race.jsx';
import RaceViewControl from './race_view_control.jsx';
import {fetchRace} from '../reduxstores/actions.js';
import { OtleSlice } from '../reduxstores/common.js';


class RaceStatus extends React.Component {
  render() {
    const selectFunc = this.props.setCurrentRace.bind(this);
    var raceView;

    switch(this.props.raceView) {
      case "map":
        raceView = <LiveMap mapID="map" />;
        break;
      case "status":
        raceView = <FullBoatTable></FullBoatTable>;
        break;
      default:
        raceView = null;
    };

    return (
      <div className="race-status">
        <RaceViewControl>
          <SelectRace callback={selectFunc} />
        </RaceViewControl>
        {raceView}
      </div>
    );
  }
};


const mapStateToProps = (state, ownProps) => {
  const otleData = state.otle;
  if (otleData.currentRace) {
    return {
      raceSelected: otleData.currentRace !== undefined,
      race: otleData.currentRace,
      raceView: otleData.raceView,
      ...ownProps
    };
  }

  return {
    raceSelected: otleData.currentRace !== undefined,
    ...otleData,
    ...ownProps,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentRace: (raceID) => dispatch(fetchRace(raceID)),
    setRaceView: (value) => dispatch(
      OtleSlice.actions.setRaceView(value)
    ),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(RaceStatus);
