import React from 'react';
import BoatStatus from './boat_status.jsx';
import Grid from '@react-css/grid';
import Timer from './timer.jsx';


const between = (l, min, max) => {
  return l >= min && l < max;
};

export class BoatTable extends React.Component {
  render() {
    return (
      <Grid rows="auto auto auto auto">
        <Grid.Item>
          <BoatStatus
            name="Pre-Start"
            discriminator={
              (race, s) => {
                const start = new Date(race.start_time).getTime();
                const now = new Date(Date.now()).getTime();
                const raceCheck = now < start;
                const stateCheck = race.race_state !== 0;
                const distCheck = !s.otle && s.boatLengthsFromLine < 2;
                return raceCheck && stateCheck && distCheck;
              }
            }
          />
        </Grid.Item>
        <Grid.Item>
          <BoatStatus
            name="OCS"
            discriminator={
              (race, s) => race.raceState !==0 && s.otle
            }
          />
        </Grid.Item>
        <Grid.Item>
          <BoatStatus
            name="Started"
            discriminator={
              (race, s) => {
                const start = new Date(race.start_time).getTime();
                const now = new Date(Date.now()).getTime();
                const raceCheck = race.race_state != 0 && now >= start;
                const distCheck = s.boatLengthsFromLine > 0.0;
                const statusCheck = !s.otle && !s.finished;
                return raceCheck && statusCheck && distCheck;
              }
            }
          />
        </Grid.Item>
        <Grid.Item>
          <BoatStatus
            name="Finished"
            discriminator={
              (race, s) => {
                return race.raceState >= 1 && s.finished;
              }
            }
          />
        </Grid.Item>
      </Grid>
    );
  }
};
 
export class FullBoatTable extends React.Component {
  render() {
    return (
      <Grid justifyItemsCenter columns="1fr" rows="auto auto auto auto">
        <Grid.Item className="full-width">
          <Timer />
        </Grid.Item>
        <Grid.Item className="full-width ocs-background">
          <BoatStatus
            name="OCS"
            discriminator={
              (race, s) => {
                const boatCheck = s.boatLengthsFromLine >0 ;
                const raceCheck = race.race_state == 3 || race.race_state == 4 ;
                console.log(`blfl: ${s.boatLengthsFromLine}, race_state: ${race.race_state}`);
                return raceCheck && boatCheck;
              }
            }
          />
        </Grid.Item>
        <Grid.Item className="full-width critical-background">
          <BoatStatus
            name="< 1 Boat Length"
            discriminator={
              (race, s) => {
                const boatCheck = !s.otle && s.boatLengthsFromLine < 0 && s.boatLengthsFromLine > -1;
                const raceCheck = race.race_state == 3 || race.race_state == 4;
                return raceCheck && boatCheck;

              }
            }
          />
        </Grid.Item>
        <Grid.Item className="full-width good-background">
          <BoatStatus
            name="< 2 Boat Length"
            discriminator={
              (race, s) => {
                const boatCheck = !s.otle && s.boatLengthsFromLine < -1 && s.boatLengthsFromLine > -2;;
                const raceCheck = race.race_state == 3 || race.race_state == 4;
                return boatCheck && raceCheck;
              }
            }
          />
        </Grid.Item>
      </Grid>
    );
  }
};
