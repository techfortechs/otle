import React, {useEffect, useState} from 'react';
import { connect } from 'react-redux';


const timeDeltaToStr = (deltaT) => {
  const sign = deltaT < 0 ? "+" : "-";
  const secondsT = Math.floor(Math.abs(deltaT) / 1000);
  const minutesT = Math.floor(secondsT / 60);
  const hoursT = Math.floor(minutesT / 60);

  const secondsStr = String(secondsT % 60).padStart(2, "0");
  const minutesStr = String(minutesT % 60).padStart(2, "0");
  const hoursStr = String(hoursT)

  return `${sign}${hoursStr}:${minutesStr}:${secondsStr}`;
};


const Timer = ({referenceTime, frozen, countdownTime, raceState}) => {
  const [timeString, setTimeString] = useState(timeDeltaToStr(countdownTime * 1000));



  useEffect(
    () => {
      if (raceState == 0) {
        setTimeString(timeDeltaToStr(countdownTime * 1000));
      }
      if (frozen) { return };
      const interval = setInterval(() => {
        if (referenceTime !== undefined) {
          const deltaT = (
           new Date(referenceTime).getTime() - new Date(Date.now()).getTime()
          );
          const str = timeDeltaToStr(deltaT);

          if (!frozen) {
            setTimeString(str);
          }
        }
      });
      return () => {
        clearInterval(interval);
      };
    },
    [referenceTime, frozen, raceState]
  );
  return (
    <div className="timer">
      <h3>{timeString}</h3>
    </div>
  );
};


const mapStateToProps = ({otle}, ownProps) => {
  if (otle.currentRace === undefined) {
    return {
      countdownTime: 0,
      frozen: true,
      ...ownProps,
    };
  }
  const raceState = otle.currentRace.race_state;
  const lookup = otle.currentRace.race_state_reference;
  const referenceTime = otle.currentRace.start_time;
  const countdownTime = otle.currentRace.countdown_time;

  const timeToShow = raceState == lookup["Setup"] ? countdownTime : referenceTime;
  return {
    referenceTime: timeToShow,
    countdownTime: countdownTime,
    frozen: raceState == lookup["Setup"] || raceState == lookup["Finished"],
    raceState: raceState,
  };
};

export default connect(mapStateToProps)(Timer);
