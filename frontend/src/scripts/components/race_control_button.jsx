import React from 'react';


export default ({race, targetRaceState, name, action}) => {
  const disabled = race === undefined || race.state === race.race_state_reference[targetRaceState];
  return (
    <button
      className="otle-button"
      disabled={disabled}
      onClick={e => action(race.uuid)}
    >
      {name}
    </button>
  );
};
