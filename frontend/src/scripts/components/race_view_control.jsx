import React from 'react';
import {connect} from 'react-redux';
import {OtleSlice} from '../reduxstores/common';
import { getStartLinePos, getEndLinePos } from '../core';

const RaceViewControl = (props) => {
  const currentRace = props.currentRace;
  return (
    <div className="control-bar">
      {props.children}
      <button
        className="otle-button"
        disabled={currentRace === undefined || props.raceView === "status"}
        onClick={() => props.setViewTo("status")}
      >
        Boat Status
      </button>
      <button
        className="otle-button"
        disabled={currentRace === undefined}
        onClick={() => {
          props.setMapToCourse(15);
        }}
      >
        Course View
      </button>
      <button
        className="otle-button"
        disabled={currentRace === undefined}
        onClick={() => {
          props.setMapToStartingLine(18);
        }}
      >
        Start Line
      </button>
      <button
        className="otle-button"
        disabled={currentRace === undefined}
        onClick={() => {
          props.setMapToFinishLine(18);
        }}
      >
        Finish Line
      </button>
    </div>
  );
};


const mapStateToProps = ({otle}) => {
  return {
    raceView: otle.raceView,
    currentRace: otle.currentRace,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setViewTo: (value) => dispatch(
      OtleSlice.actions.setRaceView({raceView: value})
    ),
    setMapToCourse: (zoom) => dispatch(
      OtleSlice.actions.setMapToStartingLine({zoom})
    ),
    setMapToStartingLine: (zoom) => dispatch(
      OtleSlice.actions.setMapToStartingLine({zoom})
    ),
    setMapToFinishLine: (zoom) => dispatch(
      OtleSlice.actions.setMapToFinishLine({zoom})
    ),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(RaceViewControl);
