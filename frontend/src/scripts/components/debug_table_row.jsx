import React from 'react';
import { connect } from 'react-redux';

const DebugRow = (props) => {
  const cells = props.fields.map((field) =>(
      <td key={field}>
        {props[field]}
      </td>
  ));
  return (
    <tr>
      {cells}
    </tr>
  );
};

const mapStateToProps = ({otle}, ownProps) => {
  const hardwareID = ownProps.hardwareID;
  const latestState = otle.latestDeviceStatus[hardwareID];

  return {
    ...latestState,
    ...ownProps,
  };
};

export default connect(mapStateToProps)(DebugRow);
