import React from 'react';
import Timer from './timer.jsx';
import ControlButton from './race_control_button.jsx';
import { connect } from 'react-redux';
import {restartRace, startRace, finishRace} from '../reduxstores/actions.js';


const ControlBar = (props) => {
  const uuid = props.race ? props.race.uuid : undefined;

  if (props.race === undefined) {
    return <div className="control-bar"></div>;
  }
  return (
    <div className="control-bar">
      <Timer />
      <ControlButton
        race={props.race}
        targetRaceState="Commencing"
        name="Start Race"
        action={props.startRaceFunc(uuid)}
      />
      <ControlButton
        race={props.race}
        targetRaceState="Ongoing"
        name="Restart Race"
        action={props.restartRaceFunc(uuid)}
      />
      <ControlButton
        race={props.race}
        targetRaceState="Finished"
        name="Finish Race"
        action={props.finishRaceFunc(uuid)}
      />
    </div>
  );
};


const mapStateToProps = ({otle}) => {
  return {
    race: otle.currentRace,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    startRaceFunc: (uuid) => () => dispatch(startRace(uuid)),
    restartRaceFunc: (uuid) => () => dispatch(restartRace(uuid)),
    finishRaceFunc: (uuid) => () => dispatch(finishRace(uuid)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ControlBar);
