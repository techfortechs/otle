import React from 'react';
import { forIn } from 'lodash';
import { viewStates, viewStateNames, OtleSlice } from  '../reduxstores/common';
import { connect } from 'react-redux';


class ViewSelect extends React.Component {
  render() {
    // const curViewState = this.props.viewState;
    const views = [];
    forIn(viewStates, (value, _) => {
      const name = viewStateNames[value];
      const onClick = this.props.setViewTo;
      views.push(
        <li key={value}>
          <a onClick={() => onClick(value)} href="#">{name}</a>
        </li>
      )
    });

    return (
      <div className="status-bar">
        <ul>
          {views}
        </ul>
      </div>
    );
  }
};

const mapStateToProps = (state, ownProps) => {
  const otleData = state.otle;
  return {
    ...ownProps,
    ...otleData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setViewTo: (viewState) => dispatch(OtleSlice.actions.setView(viewState)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ViewSelect);
