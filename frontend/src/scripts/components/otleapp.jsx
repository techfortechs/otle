import "../../styles/otle.sass";
import React from 'react';
import { viewStates } from '../reduxstores/common';
import ViewSelect from './view_select.jsx';
import * as actions from '../reduxstores/actions';
import { connect } from 'react-redux';
import RegattaSetup from './regatta_setup.jsx';
import RaceStatus from './race_status.jsx';
import Grid from '@react-css/grid';
import SideBar from './sidebar.jsx';
import ControlBar from './race_control_bar.jsx';
import DebugView from './debug_view.jsx';
import { globalClient } from "../mqtt_utils";


class OtleApp extends React.Component {
  componentDidMount() {
    this.props.loadRegattas();
  }

  getCurrentModal() {
    switch (this.props.viewState) {
      case viewStates.RegattaSetup:
        return <RegattaSetup />;
      case viewStates.RaceStatus:
        return <RaceStatus client={this.props.client} />;
      case viewStates.DebugView:
        return <DebugView />;
      default:
        // Unknown modal
        return <p>Unknown App State {this.props.viewState}</p>;
    }
  }

  render() {

    return (
      <Grid columns="0.25fr 1fr 0.25fr">
        <ViewSelect />
        {this.getCurrentModal()}
        <SideBar />

        <div className="placeholder"></div>
        <ControlBar />
        <div className="placeholder"></div>
      </Grid>
    )
  }
};


const mapStateToProps = (state, ownProps) => {
  const otleData = state.otle;
  return {...ownProps, ...otleData};
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadRegattas: () => dispatch(actions.fetchRegattas()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OtleApp);
