import React, {useEffect} from 'react';
import { hardwareIDToMarker, raceLines, registeredMaps } from '../reduxstores/common';
import L from 'leaflet';
import {connect} from 'react-redux';
import {addMarkersToMap} from '../map_utils';

const LiveMap = ({mapID, currentRace, mapArguments}) => {

  useEffect(() => {
    const map = L.map(mapID);
    registeredMaps.set(mapID, map);
    L.tileLayer(
      "http://192.168.4.1:8088/tiles/1.0.0/osm_demo/webmercator/{z}/{x}/{y}.png",
	{maxZoom: 20,
	 attribution: "",
	 tiled:true
	}
    ).addTo(map);
    map.setView([
      mapArguments.lat,
      mapArguments.lon
    ],
    mapArguments.zoom);

    addMarkersToMap(currentRace, hardwareIDToMarker, raceLines, map);

    return () => {
      for (const hardwareID in hardwareIDToMarker) {
        const marker = hardwareIDToMarker[hardwareID];
        map.removeLayer(marker);
      }
      hardwareIDToMarker.clear();
      map.off();
      map.remove();
      registeredMaps.delete(mapID);
    };
  }, [currentRace]);

  return (
    <div id={mapID} style={{minHeight: "80vh"}}>
    </div>
  );
};


const mapStateToProps = ({otle}, ownProps) => {
  return {
    currentRace: otle.currentRace,
    mapArguments: otle.mapArguments,
    ...ownProps,
  };
};

export default connect(mapStateToProps)(LiveMap);
