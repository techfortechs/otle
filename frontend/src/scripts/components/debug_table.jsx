import React from 'react';
import { connect } from 'react-redux';
import DebugRow from './debug_table_row.jsx';


const DebugView = ({hardwareIDs}) => {

  const rows = [];
  for (const hardwareID in hardwareIDs) {
    rows.push(
      <DebugRow
        hardwareID={hardwareID}
        key={hardwareID}
        fields={[
          "hardwareID",
          "datetime",
          "latitude",
          "longitude",
          "proc_overhead"
        ]}
      />
    );
  }

  return (
    <div className="debug-table">
      <table>
        <thead>
          <tr>
            <th>Hardware ID</th>
            <th>Datetime</th>
            <th>Latitude</th>
            <th>Longitude</th>
            <th>Overhead Time</th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    </div>
  );
};


const mapStateToProps = ({otle}) => {
  return {
    hardwareIDs: otle.relevantHardwareIDs,
  };
};

export default connect(mapStateToProps)(DebugView);
