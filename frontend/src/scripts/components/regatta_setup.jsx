import React from 'react';
import { connect } from 'react-redux';
import { fetchRegatta, fetchRegattas } from '../reduxstores/actions';


const SelectRegatta = ({regattas, onChange, curVal}) => {
  if (regattas === undefined) {
    // Wait
    return (
      <select
        className="otle-button"
        disabled={true}
        value={curVal}
      >
          <option>--Select Regatta--</option>
      </select>
    );
  }
  const options = regattas.map(regatta => {
    return (
      <option key={regatta.uuid} value={regatta.uuid}>
        {regatta.name}
      </option>
    );
  });
  return (
    <select className="otle-button" value={curVal} onChange={onChange}>
      <option value="">--Select Regatta--</option>
      {options}
    </select>
  );
};


class RegattaSetup extends React.Component {
  render() {
    const onChange = (e) => {
      this.props.setCurrentRegatta(e.target.value);
    };
    return (
      <div className="regatta-setup">
        <SelectRegatta
          regattas={this.props.regattas}
          onChange={onChange}
          curVal={this.props.currentRegatta}
        />
      </div>
    );
  }
};


const mapStateToProps = ({otle}) => {
  const regatta = otle.currentRegatta;
  return {
    regattas: otle.regattas,
    currentRegatta: regatta === undefined ? "" : regatta.uuid,
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    setCurrentRegatta: (regattaPK) => dispatch(fetchRegatta(regattaPK)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RegattaSetup);
