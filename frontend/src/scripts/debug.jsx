import L from 'leaflet';
import React, {useEffect, useState} from 'react';
import { createRoot } from 'react-dom/client';

const domNode = document.getElementById("application");


const Choices = ({choices, nameField, onChange, emptyName}) => {
  const options = choices.map(choice => (
    <option key={choice.id} value={choice.id}>
      {choice[nameField]}
    </option>
  ));

  return (
    <select onChange={(e) => onChange(e.target.value)}>
      <option value="">{emptyName}</option>
      {options}
    </select>
  );
};

const DebugForm = ({setDevice, setLine, setOutline, setCenterLineOffset, setBowLineOffset}) => {
  const [choices, setChoices] = useState(
    {
      outlines: [],
      lines: [],
      devices: [],
    }
  );

  useEffect(() => {
    fetch('/api/debug_state')
      .then(response => response.json())
      .then(json => setChoices(json));
  }, []);

  return (
    <form>
      <Choices
        choices={choices.devices}
        nameField="name"
        onChange={setDevice}
        emptyName="Select Device"
      />
      <Choices
        choices={choices.lines}
        nameField="name"
        onChange={setLine}
        emptyName="Select Line"
      />
      <Choices
        choices={choices.outlines}
        nameField="name"
        onChange={setOutline}
        emptyName="Select Outline"
      />
      <label for="centerlineoff">CenterLine Offset</label>
      <input
        type="number"
        id="centerlineoff"
        onChange={(e) => setCenterLineOffset(e.target.value)}
      />
      <label for="bowlineoff">Bowline Offset</label>
      <input
        type="number"
        id="bowlineoff"
        onChange={(e) => setBowLineOffset(e.target.value)}
      />
    </form>
  );

};

const DebugMap = ({device, line, outline, centerLineOffset, bowLineOffset, setDistance}) => {
  if (
    device == undefined ||
    line === undefined ||
    outline === undefined
  ) {
    return null;
  }

  var set = false;
  var boatMarker = undefined;
  var lineMarker = undefined;

  useEffect(() => {
    const map = L.map("map");
    L.tileLayer(
      "http://192.168.4.1:8088/tiles/1.0.0/osm_demo/webmercator/{z}/{x}/{y}.png",
      	{maxZoom: 25,
	 attribution: "",
	 tiled:true
	}
    ).addTo(map);

    const interval = setInterval(() => {
      const url = '/api/DEBUG';
      const payload = {
        "device_id": device,
        "outline_id": outline,
        "line_id": line,
        "centerline_offset": centerLineOffset,
        "bowline_offset": bowLineOffset,
      };
      fetch(
        url,
        {
          method: "POST",
          headers: {'Content-Type': 'application/json'},
          body: JSON.stringify(payload)
        })
        .then(response => response.json())
        .then(json => {
          setDistance(json.distance);
          map.setView([
            json.outline[0][1],
            json.outline[0][0],
          ], map.getZoom() || 25);
          if (boatMarker !== undefined) {
            map.removeLayer(boatMarker);
          }
          if (lineMarker !== undefined) {
            map.removeLayer(lineMarker);
          }
          const polycoor = json.outline.map((c) => (
            [c[1], c[0]]
          ));

          const boat = L.polygon(polycoor, {color: 'red'});

          boat.addTo(map);
          boatMarker = boat;

          const line = L.polyline([
            [json.line[0][1], json.line[0][0]],
            [json.line[1][1], json.line[1][0]]
          ]);

          line.addTo(map);
          lineMarker = line;

        });
    }, 1000);
    return () => {
      console.log("Refreshing");
      clearInterval(interval);
      map.off();
      map.remove();
    };
  }, [device, outline, line, centerLineOffset, bowLineOffset]);

  return (
    <div id="map" style={{minHeight: "80vh"}}></div>
  );
};


const DebugApplication = () => {
  const [device, setDevice] = useState(undefined);
  const [line, setLine] = useState(undefined);
  const [outline, setOutline] = useState(undefined);
  const [centerLineOffset, setCenterLineOffset] = useState(0);
  const [bowLineOffset, setBowLineOffset] = useState(0);
  const [distance, setDistance] = useState(0);

  return (
    <div className="debug-app">
      <DebugForm
        setDevice={setDevice}
        setLine={setLine}
        setOutline={setOutline}
        setCenterLineOffset={setCenterLineOffset}
        setBowLineOffset={setBowLineOffset}
      />
      <span>Distance {distance}</span>
      <DebugMap
        device={device}
        line={line}
        outline={outline}
        centerLineOffset={centerLineOffset}
        bowLineOffset={bowLineOffset}
        setDistance={setDistance}
      />
    </div>
  );
};

const root = createRoot(domNode);

root.render(
  <DebugApplication />
);
