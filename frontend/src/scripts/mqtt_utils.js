import { connect } from 'precompiled-mqtt';
import { OtleSlice, store } from './reduxstores/common';
import {createContext} from 'react';


export const makeClient = (mqttHost, mqttPort, mqttUser, mqttPass) => {
  const client = connect(
    `ws://${mqttHost}:${mqttPort}`,
    {
      username: mqttUser,
      password: mqttPass,
    }
  );
  client.on(
    "connect",
    () => {
      console.log("Established MQTT connection.");
      client.subscribe("+/status");
    }
  );
  client.on(
    "message",
    (topic, message) => {
      const payload = JSON.parse(message.toString());
      const hardwareID = topic.split("/")[0];
      payload.hardwareID = hardwareID;
      store.dispatch(OtleSlice.actions.setLatestDeviceStatus(
        payload
      ));

    }
  );

  return client;
};


const domNode = document.getElementById("application");
const mqttHost = domNode.getAttribute("mqtt-host");
const mqttPort = domNode.getAttribute("mqtt-port");
const mqttUser = domNode.getAttribute("mqtt-username");
const mqttPass = domNode.getAttribute("mqtt-password");

export const globalClient = createContext(
  makeClient(mqttHost, mqttPort, mqttUser, mqttPass)
);
