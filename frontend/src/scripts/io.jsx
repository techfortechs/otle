import { Paho } from 'paho-mqtt';

const prefix = "clientjs";


const createClientID = () => {
  const randomString = Math.random().toString(36).substr(2, 10);
  const timestamp = new Date().getTime();
  return `${prefix}_${timestamp}_${randomString}`;
};


export const createMQTTConnection = (host, port) => {
  const mqtt = new Paho.MQTT.Client(host, port, createClientID());
};
