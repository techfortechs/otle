import { OtleSlice } from './common';
import 'leaflet-rotatedmarker';


const makeAPICall = (url) => {
  return fetch(url).catch(e => console.log(e));
};


export const fetchRegattas = () => {
  const url = '/api/list_regattas';
  console.log(url);
  return (dispatch) => {
    makeAPICall(url)
      .then(response => response.json())
      .then(json => dispatch(OtleSlice.actions.setRegattaList(json)));
  };
};

export const fetchRegatta = (regattaId) => {
  const url = `/api/regatta/${regattaId}`;
  return (dispatch) => {
    makeAPICall(url)
      .then(response => response.json())
      .then(json => dispatch(OtleSlice.actions.setCurrentRegatta(json)));
  };
};

export const fetchRace = (raceId) => {
  const url = `/api/race/${raceId}`;
  return (dispatch) => {
    makeAPICall(url)
      .then(response => response.json())
      .then(json => {
        dispatch(OtleSlice.actions.setCurrentRace(json));
      });
  };
};


export const startRace = (raceId) => {
  const url = `/api/race/${raceId}/start`;
  return (dispatch) => {
    makeAPICall(url)
      .then(response => response.json())
      .then(
        json => dispatch(OtleSlice.actions.updateRace(json))
      );
  };
};

export const restartRace = (raceId) => {
  const url = `/api/race/${raceId}/restart`;
  return (dispatch) => {
    makeAPICall(url)
      .then(response => response.json())
      .then(
        json => dispatch(OtleSlice.actions.updateRace(json))
      );
  };
};

export const finishRace = (raceId) => {
  const url = `/api/race/${raceId}/finish`;
  return (dispatch) => {
    makeAPICall(url)
      .then(response => response.json())
      .then(
        json => dispatch(OtleSlice.actions.updateRace(json))
      );
  };
};
