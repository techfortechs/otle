import { configureStore, createSlice } from '@reduxjs/toolkit';
import { has } from 'lodash';
import { getLatestFinishLinePos, getLatestStartLinePos } from '../core';
import {updateMarker} from '../map_utils';

export const viewStates = {
  RegattaSetup: "regattasetup",
  RaceStatus: "racestatus",
  RaceResults: "raceresults",
  DebugView: "debugview",
};

export const viewStateNames = {
  "regattasetup": "Regatta Setup",
  "racestatus": "Race Status",
  "raceresults": "Race Results",
  "debugview": "Debug",
};


export const registeredMaps = new Map();
export const hardwareIDToMarker = new Map();
export const raceLines = {
  startingLine: undefined,
  finishLine: undefined,
  startingLinePos: {pin: undefined, rc: undefined},
  finishLinePos: {pin: undefined, rc: undefined},
  startingLinePK: undefined,
  finishLinePK: undefined,
  startingLinePinDevice: undefined,
  startingLineRCDevice: undefined,
  finishLinePinDevice: undefined,
  finishLineRCDevice: undefined,
};

const _isRelevantPacket = (state, payload) => {
  const hardwareID = payload.hardwareID;
  return has(state.relevantHardwareIDs, hardwareID);
};

export const OtleSlice = createSlice({
  name: "otle",
  initialState: {
    boatLengths: {},
    currentRace: undefined,
    currentRegatta: undefined,
    relevantHardwareIDs: {},
    latestDeviceStatus: {},
    latestBoatStatus: {},
    mapArguments: {
      lat: undefined,
      lon: undefined,
      zoom: 15,
    },
    mqttHost: undefined,
    mqttPort: undefined,
    nameLookup: {},
    raceView: undefined,
    regattas: undefined,
    viewState: viewStates.RegattaSetup,
  },
  reducers: {
    setRegattaList: (state, action) => {
      state.regattas = action.payload;
    },
    setCurrentRegatta: (state, action) => {
      state.currentRegatta = action.payload;
    },
    setCurrentRace: (state, action) => {
      console.log("Setting Race context", action.payload);
      const start = action.payload.course.starting_line;
      const finish = action.payload.course.finish_line;

      state.currentRace = action.payload;
      state.relevantHardwareIDs = {};
      state.latestBoatStatus = {};
      hardwareIDToMarker.clear();

      action.payload.boat_setups.forEach(setup => {
        const hardwareID = setup.registered_device.hardware_id;
        state.nameLookup[hardwareID] = setup.boat.name;
        state.relevantHardwareIDs[hardwareID] = "boat";
        state.boatLengths[hardwareID] = setup.boat.outline.meter_length;
      });
      action.payload.mark_setups.forEach(setup => {
        const hardwareID = setup.registered_device.hardware_id;
        state.nameLookup[hardwareID] = setup.mark.name;
        state.relevantHardwareIDs[hardwareID] = "mark";
      });

      raceLines.startingLinePK = start.id;
      raceLines.finishLinePK = finish.id;

      state.relevantHardwareIDs[start.pin_device.hardware_id] = "pin";
      state.relevantHardwareIDs[start.rc_device.hardware_id] = "rc";
      state.relevantHardwareIDs[finish.pin_device.hardware_id] = "pin";
      state.relevantHardwareIDs[finish.rc_device.hardware_id] = "rc";

      raceLines.startingLinePinDevice = start.pin_device.hardware_id;
      raceLines.startingLineRCDevice = start.rc_device.hardware_id;
      raceLines.finishLinePinDevice = finish.pin_device.hardware_id;
      raceLines.finishLineRCDevice = finish.rc_device.hardware_id;

      raceLines.startingLinePos.pin = start.pin.static_position;
      raceLines.startingLinePos.rc = start.rc_boat.static_position;
      raceLines.finishLinePos.pin = finish.pin.static_position;
      raceLines.finishLinePos.rc = finish.rc_boat.static_position;

      if (state.raceView === undefined) {
        state.raceView = "map";
      }
      const pos = getLatestStartLinePos(
        action.payload,
        state.latestDeviceStatus
      );
      state.mapArguments.lat = pos.latitude;
      state.mapArguments.lon = pos.longitude;
    },

    updateRace: (state, action) => {
      state.currentRace = action.payload;
    },
    setView: (state, action) => {
      state.viewState = action.payload;
    },
    setMqttSettings: (state, action) => {
      state.mqttHost = action.payload.mqtt_host;
      state.mqttPort = action.payload.mqtt_port;
    },
    setLatestDeviceStatus: (state, action) => {
      const payload = action.payload;
      const hardwareID = payload.hardwareID;
      if (_isRelevantPacket(state, payload)) {
        const uuid = state.currentRace.uuid;
        if (state.relevantHardwareIDs[hardwareID] === "boat") {
          try {
            const boatLength = state.boatLengths[hardwareID];
            payload.distanceToLine = payload.line_distances[uuid];
            payload.boatLengthsFromLine = payload.distanceToLine / boatLength;
            payload.boatStatus = payload.status_in_race[uuid];
            payload.otle = payload.boatStatus === "otle";
            //console.log(`boatStatus: ${payload.boatStatus}  bl_from_line: ${payload.distanceToLine / boatLength}`);
          } catch (error) {
            //console.log(`Skipping bad packet ${hardwareID} on ${payload.nonce}`);
            return;
          }
        }
        state.latestDeviceStatus[hardwareID] = payload;
        updateMarker(
          state,
          registeredMaps,
          hardwareIDToMarker,
          raceLines,
          payload
        );
      }
    },
    setRaceView: (state, action) => {
      state.raceView = action.payload.raceView;
      if (action.payload.mapArguments) {
        state.mapArguments = action.payload.mapArguments;
        registeredMaps.forEach((map) => {
          map.setView([
            state.mapArguments.lat,
            state.mapArguments.lon,
          ], state.mapArguments.zoom);
        });
      }
    },
    setMapToStartingLine: (state, action) => {
      const pos = getLatestStartLinePos(
        state.currentRace,
        state.latestDeviceStatus
      );
      state.raceView = "map";
      state.mapArguments.lat = pos.latitude;
      state.mapArguments.lon = pos.longitude;
      state.mapArguments.zoom = action.payload.zoom;
      registeredMaps.forEach((map) => {
        map.setView([
          state.mapArguments.lat,
          state.mapArguments.lon,
          ], state.mapArguments.zoom);
      });
    },
    setMapToFinishLine: (state, action) => {
      const pos = getLatestFinishLinePos(
        state.currentRace,
        state.latestDeviceStatus
      );
      state.raceView = "map";
      state.mapArguments.lat = pos.latitude;
      state.mapArguments.lon = pos.longitude;
      state.mapArguments.zoom = action.payload.zoom;
      registeredMaps.forEach((map) => {
        map.setView([
          state.mapArguments.lat,
          state.mapArguments.lon,
          ], state.mapArguments.zoom);
      });
    },
  },
});

export const store = configureStore({
  reducer: {
    otle: OtleSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>getDefaultMiddleware({
    serializableCheck: false,
    immutableCheck: false,
    actionCreatorCheck: false
  }),
});
