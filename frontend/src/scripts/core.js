/**
 * Quickly define a bunch of commonly used functions.
 * @author William Fong
 */

import L from 'leaflet';

export const boatIcon = L.icon({
  iconUrl: "/static/boat.svg",
  iconSize: [16, 32],
  iconAnchor: [8, 16]
});

export const bouyIcon = L.icon({
  iconUrl: "/static/bouy.svg",
  iconSize: [8, 8],
  iconAnchor: [4, 4],
});

export const rcIcon = L.icon({
  iconUrl: "/static/rc.svg",
  iconSize: [8, 8],
  iconAnchor: [4, 4],
});

export const pinIcon = L.icon({
  iconUrl: "/static/pin.svg",
  iconSize: [8, 8],
  iconAnchor: [4, 4],
});

export const distanceToLine = (point, p1, p2) => {
  const lhs = (p2.easting - p1.easting) * (p1.northing - point.northing);
  const rhs = (p1.easting - point.easting) * (p2.northing - p1.northing);
  const nominator = Math.abs(lhs - rhs);

  const xDiff = Math.pow((p2.easting - p1.easting), 2);
  const yDiff = Math.pow((p2.northing - p1.northing), 2);
  const denominator = Math.sqrt(xDiff + yDiff);

  return nominator / denominator;
};

export const getStartLinePos = (race) => {
  const pos = race.course.starting_line.pin.static_position;
  return {
    lat: pos.latitude,
    lon: pos.longitude,
  };
};

export const getEndLinePos = (race) => {
  const pos = race.course.finish_line.pin.static_position;
  return {
    lat: pos.latitude,
    lon: pos.longitude,
  };
};


export const getLatestLinePos = (line, latestDeviceStatus) => {
  const pinDevice = line.pin_device.hardware_id;
  if (latestDeviceStatus[pinDevice] !== undefined) {
    return latestDeviceStatus[pinDevice];
  } else {
    return line.pin.static_position;
  }
};

export const getLatestStartLinePos = (race, latestDeviceStatus) => {
  return getLatestLinePos(race.course.starting_line, latestDeviceStatus);
};

export const getLatestFinishLinePos = (race, latestDeviceStatus) => {
  return getLatestLinePos(race.course.finish_line, latestDeviceStatus);
};

export const getIcon = (deviceType) => {
  switch (deviceType) {
    case "mark":
      return bouyIcon;
    case "boat":
      return boatIcon;
    case "rc":
      return rcIcon;
    case "pin":
      return pinIcon;
    default:
      return undefined;
  }
};
