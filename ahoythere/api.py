from django.http import JsonResponse
from django.conf import settings


def get_mqtt_setup(_) -> JsonResponse:
    return JsonResponse(
        {
            "host": settings.MQTT_HOST,
            "port": settings.MQTT_PORT,
        }
    )
