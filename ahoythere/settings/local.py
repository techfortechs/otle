import os
import sys

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = (
    "!!!SUPER SECRET KEY, DO NOT ADD TO GIT!!!"
)

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["*"]


# Application definition

# Database
# https://docs.djangoproject.com/en/4.0/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": os.environ.get("POSTGRES_DB", "ahoythere"),
        "USER": os.environ.get("POSTGRES_USER", "skipper"),
        "PASSWORD": os.environ.get("POSTGRES_PASSWORD", "seashanty"),
        "HOST": os.environ.get("POSTGRES_HOST", "localhost"),
        "PORT": os.environ.get("POSTGRES_PORT", 5432),
    }
}


MQTT_HOST = "192.168.4.1"
MQTT_PORT = 9001
MQTT_USERNAME = "t4t"
MQTT_PASSWORD = "GoBeavers5!"
RELAY_PUBLISH_CHANNEL = "status"
POSITION_CHANNEL = "+/position"

GLOBAL_SRID = 4326 # GPS, lat/lon

