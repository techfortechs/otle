from django.contrib import admin
from django.urls import include, path

from ahoythere import api as root_api
from races import api as race_api
from races import views as race_views

apipatterns = [
    *race_api.apipatterns,
    path("mqtt_settings/", root_api.get_mqtt_setup),
]

urlpatterns = [
    path("", race_views.ListRegattaView.as_view(), name="index"),
    path("debug/", race_views.debug, name="debug"),
    path(
        "regatta/<uuid:slug>/",
        race_views.DetailRegattaView.as_view(),
        name="regatta-detail",
    ),
    path("admin/", admin.site.urls),
    path("race/<uuid:slug>/", race_views.live_race, name="race-detail"),
    path("api/", include(apipatterns)),
]
